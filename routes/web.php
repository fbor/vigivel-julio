<?php

Route::get('/', 'HomeController@index')->name('home');

Route::get('/sobre-o-grupo.html', 'AboutController@index')->name('about');

Route::get('/nossos-servicos.html', 'ServicosController@index')->name('service');
Route::get('/nossos-servicos/{id}/{slug}.html', 'ServicosController@show')->name('service_show');

Route::get('/franquia.html', 'FranquiasController@index')->name('franquia');
Route::get('/franquia/{slug}.html', 'FranquiasController@show')->name('franquia_show');
Route::post('/franquia/sender', 'FranquiasController@sender');

Route::get('/orcamento/{id?}', 'OrcamentosController@index')->name('orcamento');
Route::post('/orcamento', 'OrcamentosController@sender');

/**
 * Route::get('/contato.html', 'ContactController@index')->name('contact');
 * Route::post('/contato.html', 'ContactController@sender');
 */

Route::get('/trabalhe-conosco.html','WorkController@index')->name('work');
Route::post('/trabalhe-conosco.html', 'WorkController@sender');

Route::get('/portal-do-cliente.html', 'CustomerController@index')->name('portal_cliente');
Route::post('/portal-do-cliente.html', function(){
    return response([ 'message'=> 'Solicitação Enviada! Em breve você receberá o acesso em seu e-mail!' ]);
});

Route::get('/dashboard/fornecedor.html', function(){
    return view('dashboard-fornecedor');
})->name('portal_fornecedor');

Route::get('/dashboard/portal.html', function(){
    return view('dashboard');
})->name('portal_franquia');

Route::get('/api/endereco', function(){
	$json = [
        [
            'lat'   => config('app.latitude'),
            'lon'   => config('app.longitude'),
            'label' => config('app.name'),
            'address'=> config('app.street_address') . '<br> ' . config('app.postal_code') . ', ' . config('app.locality') .' - ' . config('app.region'),
            'phone' => config('app.phone_number')
        ]
    ];

    return response($json);

})->name('address');

Route::post('/api/franquia', 'FranquiasController@showByState')->name('franquia_api');

Route::get('emails', function(){
    return view('emails.default');
});
Route::get('make-budget', function() {
    return view('budget');
});
Route::get('/answers', 'AnswerController@index');
Route::get('/answers/{id}', 'AnswerController@getNext');
Route::post('/products-related', 'AnswerController@getProducts');
Route::post('/submit-mail', 'AnswerController@submitMail');
