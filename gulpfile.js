const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');
require('laravel-elixir-minify-html');


elixir(mix => {
    mix
        .sass([
            './node_modules/normalize.css/normalize.css',
            'basic.scss',
        ], 'public/css/basic.css')
        .sass([
            'style.scss',
            './node_modules/slick-carousel/slick/slick.css'
        ], 'public/css/style.css')
        .scripts(
            [
                './node_modules/jquery/dist/jquery.min.js',
                './node_modules/jquery-lazyload/jquery.lazyload.js',
                './node_modules/slick-carousel/slick/slick.js',
                './node_modules/jquery-mask-plugin/src/jquery.mask.js',
                './node_modules/jquery-validation/dist/jquery.validate.js',
                './node_modules/jquery-validation/dist/localization/messages_pt_BR.js',
                './node_modules/jquery-form/dist/jquery.form.min.js',
                'main.js',
                'validate.js',
                'widget-map.js'
            ],
            'public/js/all.js'
        )
        .webpack('app.js')
        .browserSync({
            port: 8080,
            proxy: 'localhost:8000'
        });

    if (false && elixir.config.production) {

        mix.html('storage/framework/views/*', 'storage/framework/views/', {collapseWhitespace: true, removeAttributeQuotes: true, removeComments: true, minifyJS: true});

    }
});
