@extends('master')
@section('title', 'Dashboard do franquiado | ' . config('app.name'))
@section('schema', 'ContactPage')
@section('mainView', 'pg-dashboard')
@section('description', '[--meta description--]')

@section('content')
    <section class="wrapper container" role="main">

        <header class="heading text-center">
            <h1 class="title" itemprop="headline"><strong>Cadastre-se</strong></h1>
            <p class="caption">Agora ficou ainda mais facil acompanhar as novidades.</p>
            <h5 class="caption" itemprop="description"><small class="upp">Ao fazer seu cadastro você receberá cotações diretamente em seu e-mail!</small></h5>
        </header>

        <form class="form validate container" itemprop="mainEntityOfPage" action="{{ route('work') }}" method="post" enctype="multipart/form-data">

            <div class="row">
                <div class="field col s12 m6 l8">
                    <div class="form-group clearfix">
                        <label for="nome">Nome/Razão:</label>
                        <input class="form-control" id="nome" name="nome" type="text" required>
                    </div>
                </div>
                <div class="field col s12 m6 l4">
                    <div class="form-group clearfix">
                        <label for="nome_alternative">Nome Usual:</label>
                        <input class="form-control" id="nome_alternative" name="nome_alternative" type="text" required>
                    </div>
                </div>
                <div class="field col s12 m6 l5">
                    <div class="form-group clearfix">
                        <label for="cpf_cnpj">CPF/CNPJ:</label>
                        <input class="form-control" id="cpf_cnpj" name="cpf_cnpj" type="text" required>
                    </div>
                </div>
                <div class="field col s12 m6 l2">
                    <div class="form-group clearfix">
                        <label for="estado">Estado:</label>
                        <select class="form-control" id="estado" name="estado" required>
                            <option value="">UF:</option>
                            <option value="PR">PR</option>
                            <option value="RS">RS</option>
                            <option value="SC">SC</option>
                            <option value="SP">SP</option>
                            <option value="RJ">RJ</option>
                        </select>
                    </div>
                </div>
                <div class="field col s12 m6 l5">
                    <div class="form-group clearfix">
                        <label for="insc_municipal">Insc. Municipal:</label>
                        <input class="form-control" id="insc_municipal" name="insc_municipal" type="text" required>
                    </div>
                </div>

                <div class="col s12 clearfix">
                    <hr class="line">
                </div>

                <div class="field col s12 m6 l5">
                    <div class="form-group clearfix">
                        <label for="endereco">Endereço:</label>
                        <input class="form-control" id="endereco" name="endereco" type="text" required>
                    </div>
                </div>
                <div class="field col s12 m6 l1">
                    <div class="form-group clearfix">
                        <label for="numero">Nº</label>
                        <input class="form-control" id="numero" name="numero" type="text" required>
                    </div>
                </div>
                <div class="field col s12 m6 l3">
                    <div class="form-group clearfix">
                        <label for="bairro">Bairro:</label>
                        <input class="form-control" id="bairro" name="bairro" type="text" required>
                    </div>
                </div>
                <div class="field col s12 m6 l3">
                    <div class="form-group clearfix">
                        <label for="cep">CEP:</label>
                        <input class="form-control" id="cep" name="cep" type="text" required>
                    </div>
                </div>
                <div class="field col s12 m6 l5">
                    <div class="form-group clearfix">
                        <label for="cidade">Cidade:</label>
                        <input class="form-control" id="cidade" name="cidade" type="text" required>
                    </div>
                </div>
                <div class="field col s12 m6 l3">
                    <div class="form-group clearfix">
                        <label for="pais">Pais:</label>
                        <select class="form-control" id="estado" name="estado" required>
                            <option value="">Selecione:</option>
                            <option value="Brasil">Brasil</option>
                            <option value="Argentina">Argentina</option>
                            <option value="Paraguai">Paraguai</option>
                        </select>
                    </div>
                </div>

                <div class="col s12 clearfix">
                    <hr class="line">
                </div>

                <div class="field col s12 m6 l3">
                    <div class="form-group clearfix">
                        <label for="fone1">Telefone:</label>
                        <input class="form-control" id="fone1" name="fone1" placeholder="(xx) xxxx-xxxx" type="tel">
                    </div>
                </div>
                <div class="field col s12 m6 l3">
                    <div class="form-group clearfix">
                        <label for="fone2">Telefone:</label>
                        <input class="form-control" id="fone2" name="fone2" placeholder="(xx) xxxx-xxxx" type="tel">
                    </div>
                </div>
                <div class="field col s12 m6 l3">
                    <div class="form-group clearfix">
                        <label for="celular">Celular:</label>
                        <input class="form-control" id="celular" name="celular" placeholder="(xx) 9 xxxx-xxxx" type="tel">
                    </div>
                </div>
                <div class="field col s12 m6 l3">
                    <div class="form-group clearfix">
                        <label for="fax">Fax:</label>
                        <input class="form-control" id="fax" name="fax" placeholder="(xx) xxxx-xxxx" type="tel">
                    </div>
                </div>

                <div class="col s12 clearfix">
                    <hr class="line">
                </div>

                <div class="field col s12 m6 l3">
                    <div class="form-group clearfix">
                        <label for="atividade">Atividade:</label>
                        <select class="form-control" id="atividade" name="atividade" required>
                            <option value="">Selecione:</option>
                            <option value="Atacado">Atacado</option>
                            <option value="Comercio">Comercio</option>
                            <option value="Varejo">Varejo</option>
                            <option value="Lojista">Lojista</option>
                            <option value="Industria">Industria</option>
                        </select>
                    </div>
                </div>
                <div class="field col s12 m6 l3">
                    <div class="form-group clearfix">
                        <label for="email">E-mail:</label>
                        <input class="form-control" id="email" name="email" placeholder="seuemail@email.com" type="email" required>
                    </div>
                </div>
                <div class="field col s12 m6 l6">
                    <div class="form-group clearfix">
                        <label for="website">WebSite:</label>
                        <input class="form-control" id="website" name="website" placeholder="http://examplo.com.br" type="url">
                    </div>
                </div>

                <div class="col s12">
                    <button id="submit" type="submit" class="btn btn-primary waves-effect waves-light btn-block btn-lg text-uppercase">SALVAR DADOS</button>
                </div>
            </div><!-- end row -->

            <div class="form-msg alert">
                <p class="message"></p>
                <button class="closed"></button>
            </div>

    	</form>

    </section>


@stop
