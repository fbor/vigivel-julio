@extends('master')
@section('mainView', 'pg-service-show')
@section('title', $servico->label . ' | ' . config('app.name'))
@section('description', $servico->resumo)

@if($servico->image)
    @section('og-image', $servico->image->present()->render('large'))
@endif

@section('content')
    <div class="ui-breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
        <ol class="wrapper">
            <li class="breadcrumb-label"><span>Você está:</span></li>
            <li class="breadcrumb-item home" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="{{ route('home') }}"><span itemprop="name">Início</span></a></li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <a href="{{ route('service') }}" title="Nossos serviços">
                    <span itemprop="name">Serviços</span>
                </a>
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <span itemprop="name">{{ $servico->label }}</span>
            </li>
        </ol>
    </div><!-- .breadcrumb -->

    <section class="wrapper container" role="main">

        <header class="heading flex">
            <i class="icon">
                <img src="{{ $servico->icon->present()->render('thumb') }}" alt="">
            </i>
            <hgroup>
                <h1 class="title small" itemprop="headline"><strong>{{ $servico->label }}</strong></h1>
                <h5 class="caption" itemprop="description">{{ $servico->resumo }}</h5>
            </hgroup>
        </header>

        <div class="row">
            <div class="col s12 l7">
                <h6 class="resume">
                    {!! $servico->texto !!}
                </h6>
                    <br>
                    @if(!empty($videos))
                  <section>
                    <header class="heading">
                        <h4 class="title bold"><strong>Videos</strong></h4>
                    </header>

                    <ul id="slider-service-video" class="row feed-list">
                        @foreach( $videos as $video)

                        <?php
                        $val = unserialize($video->servicos);

                        for ($i=0; $i < count($val); $i++)
                        {

                            if(request()->id == $val[$i])
                            {

                                echo '

                                <li class="col s12 m6 l2 feed-item">

                                <div class="video">

                                    <h6 class="caption" itemprop="description"><span>'.$video->label.' </span></h6>
                                ';

                                echo '
                                    <iframe width="100%" height="100%" src="https://www.youtube.com/embed/'. $video->youtube.'"  frameborder="0" allowfullscreen></iframe>

                                               </div>
                            </li>
                                ' ;

                            }

                    }

            ?>






                        @endforeach
                    </ul>
                </section>
                    @endif
                <div class="widget bg-primary widget-orcamento">
                    <div class="box-title">
                        <h4 class="title bold"><strong>Ficou interessado</strong></h4>
                        <p class="label">ou quer saber mais, fale com a Vigivel!</p>
                    </div>
                    <div>
                        <a href="{{ route('orcamento', ['id'=>$servico->id]) }}" class="btn">SOLICITE UM ORÇAMENTO</a>
                        <small class="info">É sem compromisso.</small>
                    </div>
                </div>

                @if(!empty($servico->youtube))
                    <iframe width="724" height="407" src="https://www.youtube.com/embed/{{ $servico->youtube }}" frameborder="0" allowfullscreen></iframe>
                @endif

                @if(empty($servico->youtube) && $servico->image)
                    <figure class="picture">
                        <img src="{{ $servico->image->present()->render('large') }}" alt="{{ $servico->label }}">
                    </figure>
                @endif


               @if(isset($jaClientes->id))
                <article class="widget bg-primary">
                    <header class="widget-orcamento">
                        <div class="box-title">
                            <h4 class="title bold"><strong>{{ $jaClientes->titulo }}</strong></h4>
                        </div>
                        <div>
                            <a href="{{ $jaClientes->link }}" class="btn">{{ $jaClientes->label_botao }}</a>
                        </div>
                    </header>

                    <hr class="line space">
                    <p>
                       {!! $jaClientes->texto_opcional_1 !!}
                    </p>

                </article>
                @endif

            </div>



            <aside class="col s12 l4 offset-l1 sidebar">

                <div class="widget shared">
                    <p class="label upp"><small>Compartilhe com seus amigos:</small></p>
                    <div class="shared-toolbox addthis_inline_share_toolbox_clba"></div>
                </div>



                <section class="widget">
                    <header class="heading">
                        <h4 class="title bold"><strong>Mais Serviços</strong></h4>
                    </header>

                    <div class="feed-list nav">
                        @foreach($servicos as $servicoItem)
                            <a href="{{ route('service_show', ['id' => $servicoItem->id, 'slug' => $servicoItem->slug]) }}">{{ $servicoItem->label }}</a>
                        @endforeach
                    </div>
                </section>

                @foreach($sideBanners as $sideBanner)
                <section class="widget vigivel-app">
                    <div class="clearfix content">
                        <figure class="">
                            <a href="{{$sideBanner->link}}">
                                <img src="{{ $sideBanner->image->present()->render('thumb') }}" alt="">
                            </a>
                        </figure>
                    </div>
                </section>
                @endforeach
            </aside>
        </div>

    </section>
@stop

@section('scripts')
<script type="text/javascript">
    var addthis_share = {
        url_transforms : {
            shorten: {
                twitter: 'bitly'
            }
        },
        shorteners : {
            bitly : {}
        }
    };


</script>
<script async src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-51bf3d194679f698"></script>
@stop
