@extends('master')
@section('title', 'Solicite um orçamento | ' . config('app.name'))
@section('mainView', 'pg-orcamento')
@section('description', 'Ficou interessado, ou quer saber mais, solicite um orçamento sem compromisso!')

@section('content')
    <section class="wrapper" role="main">
    	@include('partials/heading', ['title'=>'Solicite um orçamento', 'subtitle'=>'Ficou interessado? Solicite um orçamento sem compromisso'])

    	<form class="form validate container" itemprop="mainEntityOfPage" action="{{ route('orcamento') }}" method="post">
            {{ csrf_field() }}
    		
			<fieldset class="row collapse">
                <div class="field col s12">
                    <label>Selecione os serviços do seu interesse:</label>
                    <ul class="feed-list row cycle">
                        @foreach($servicos as $servico)
                            <li class="col s12 m6 l3 item">
                                <label for="servico{{ $servico->id }}">
                                    <input id="servico{{ $servico->id }}" type="checkbox"
                                        {{ ($id == $servico->id) ? 'checked=checked' : '' }} name="servicos[]" value="{{ $servico->label }}">
                                    <span class="caption">{{ $servico->label }}</span>
                                </label>
                            </li>
                        @endforeach
                    </ul>
                </div>
                
				<div class="field col s12 m6 l4">
					<label for="name">Nome completo:</label>
					<input id="name" type="text" name="name" placeholder="Seu nome" minlength="3" required>
				</div>
				<div class="field col s12 m6 l4">
					<label for="email">E-mail:</label>
					<input id="email" type="email" name="email" placeholder="seu@email.com" required>
				</div>
                <div class="field col s12 m6 l4">
                    <label for="tel">Telefone:</label>
                    <input id="tel" type="tel" name="phone_number" placeholder="(xx) 9999-99999" required>
                </div>
                <div class="field col s12 m6">
                    <input type="text" name="cidade" placeholder="Cidade:" required>
                </div>
                <div class="field col s12 m6">
                    <input type="text" name="estado" placeholder="Estado:" required>
                </div>

				<div class="field col s12 l8">
                    <textarea name="observacao" rows="4" placeholder="Observações adicionais: (opcional)"></textarea>
                </div>

                <div class="field col s12 l4">
			        <div class="g-recaptcha"
			              data-sitekey="6LeqqBoUAAAAAJKTOUN_7MbVjvLapgQVcbXUPec_"
			              data-callback="onSubmit"
			              data-size="visible">
			        </div>
		        </div>
		        
                <div class="field col s12 l4">
                    <button type="submit" class="submit block">SOLICITAR ORÇAMENTO</button>
                </div>

            </fieldset>

            <div class="form-msg alert">
                <p class="message"></p>
                <button class="closed"></button>
            </div>

    	</form>
    </section>
@stop


@section('scripts')
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
@stop
