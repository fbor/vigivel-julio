<header class="heading wrap-picture">
    <i class="box-image"><img src="{{ asset('img/icon-protect-full.jpg') }}" alt="Protect Full"></i>
    <h6 class="caption">Franquia Vigivel</h6>
    <h1 class="title" itemprop="headline"><strong>Protect Full</strong></h1>
</header>


<article class="section row" itemprop="mainEntityOfPage">
    <div class="col s12 l8">
        <ul class="feed-list">
            <li>
            <h6 class="caption">O que é a Franquia Protect Full?</h6>
                <p>
                Este tipo de franquia deve ser formada por dois sócios, um ligado a área comercial e administrativa e outro com ligação na área técnica. Estes deverão passar pelo processo de treinamento WEB para credenciamento e assim possibilitar a comercialização do serviço de monitoramento de alarmes nas regiões em que possuem interesse.<br><br>
                </p>
            <h6 class="caption">Serviços Disponíveis</h6>
                <p>
                - Vigia: A função se destina precipuamente a resguardar a vida e o patrimônio das pessoas sem porte de armas;<br>
                - Controle de Acesso: A principal função do controlador de acesso é limitar a presença de pessoas, por isso, ele é bastante usado no setor público e no privado. O controlador de acesso cuida da segurança de prédios, estabelecimentos comerciais, indústrias, condomínios, entre outros lugares;<br>
                - Portaria: indivíduo que toma conta da portaria de edifício ou instituição, permite ou não a entrada de pessoas estranhas, recebe correspondência e distribui aos destinatários;<br>
                - Limpeza: Os serviços de conservação e limpeza pode ser considerado um fenômeno recente do segmento econômico de micro e pequenas empresas. Embora houvesse prestadores de serviço atuando há muitos anos no setor, a maioria das grandes empresas utilizava uma equipe própria de limpeza. O serviço era executado sem técnicas adequadas ou equipamentos e produtos profissionais, pois as atividades não representavam a principal função da organização e os clientes ainda não davam tanta importância para a higiene do recinto na aquisição de produtos e serviços.<br>
                - Jardinagem: O serviço de jardinagem conta com uma equipe selecionada para exercer as funções em jardins de empresas, condomínios e residências. Nosso objetivo é manter as áreas gramadas e jardins sempre com um aspecto bonito e saudável;<br>
                - Sistema de cameras: O Sitema é amplo e pensado para atender as principais necessidades do cliente, estes podem ser residenciais ou pessoas com lojas, escritórios e comércios. Para esse mercado oferecemos equimanetos com tecnologia de ponta;<br>
                - Monitoramento: Instalação de sistema de alarme que, mediante a uma pequena mensalidade, será monitorado dia e noite pela Vigivel. O monitoramento oferece relatórios sobre horários de ativamento e desativamento, entregando ao cliente as informações exatas sobre quem/quando abriu e fechou o patrimônio;<br>
                - Sharing cam: Também é um sistema de cameras onde o cliente pode gerenciar as imagens através de um aplicativo e armazenamento na nuvem fazendo com que o custo do serviço seja menor. Oferecemos o aplicativo e as cameras possuem formas de compra e comodato (empréstimo de equipamento);<br>
                - Rastreamento veicular: Indicado para aplicações em gestão de frota, segurança de veículos e passageiros, pode ser contratado conforme a necessidade do cliente. Simples de usar e fácil de implantar, o Rastreamento Veicular funciona através de autogestão, onde o cliente adquire o sistema, recebe o treinamento e gerencia sua frota via web ou através do aplicativo. A vigivel ainda presta regionalmente o serviço de assistência técnica, com atendimento ágil e de qualidade.<br>
                - Portaria Remota: Sistema de Controle de Acesso remoto onde todo controle é realizado através de nossa Central 24h.<br><br>
                </p>
            <h6 class="caption">Perfil do Cliente</h6>
                <p>
                RASTREADOR VEICULAR : Destacamos como clientes em potencial e que representam grande parcela do mercado a ser explorado empresas frotistas, essas empresas possuem uma necessidade muito grande de ter controle sob as atividades, rotas, forma de pilotagem, velocidade, controle de condutor dentre outros e são atraídas pela plataforma Web que disponibiliza relatórios diversos que permitem esse controle tão desejado, contudo não podemos esquecer das pessoas físicas, com necessidades distintas aos das pessoas jurídicas, esse tipo de cliente busca ter o controle sobre seu veiculo e geralmente é encantado pelo sistema de bloqueio;<br>
                MONITORAMENTO: Este serviço tem como foco empresas e pessoas físicas das classes A, B e C;<br>
                DEMAIS SERVIÇOS: Os demais serviços tem como principal objetivo atender empresas que buscam a tercerização de serviços basicos como limpeza e jardinagem, bem como construtoras, condominios entre outros tipos de negocios que necessitam de atendimento na area de serviços de manutenção e segurança.<br><br>
                </p>
            <h6 class="caption">Expectativa do Consumidor</h6>
                <p>
                - Atendimento rápido;<br>
                - Segurança;<br>
                - Confiança;<br>
                - Transparência;<br>
                - Comodidade;<br>
                - Assistência técnica especializada;<br>
                - Disponibilidade 24hs por parte da empresa.<br><br>
                </p>
            <h6 class="caption">Diferenciais do Franqueado Vigivel</h6>
                <p>
                - Nenhum investimento em Software: O franqueado receberá sem custo algum todos os sofwares que auxiliaram e possibilitarão toda a prestação de serviço, bem como todos os manuais de operação facilitando a retirada de duvidas e proporcionando ao franqueado autonomia operacional;<br>
                - Material de Propaganda e Publicidade: Material padronizado e gratuito aplicável a todas as unidades;<br>
                - Plataforma Gerencial: Proporcionará ao franqueado completo controle sobre sua empresa, auxiliando nas tomadas de decisão;<br>
                - Acesso web a Plataforma Gerencial de qualquer lugar do mundo;<br>
                - Web Site institucional: fornecido pela franqueadora, com contados da franquia local possibilitando acessibilidade e facilidade no contato entre cliente e franquia;<br>
                - Técnicas de atendimento: com base no Know How dos serviços e produtos oferecidos pelo Grupo Vigivel oferecemos técnicas que facilitam auxiliam na conquista e fidelização do cliente;<br>
                - Franquia, equipe e serviços padronizados.<br><br>
                </p>
            <h6 class="caption">Suporte</h6>
                <p>
                RH<br>
                Jurídico<br>
                Técnico<br>
                Financeiro<br>
                T.I<br>
                Corporativo<br>
                Comercial<br>
                Marketing<br>
                Operações<br><br>
                </p>
            <h6 class="caption">Plano de Negócios</h6>
                <p>
                Faturamento bruto pode ultrapassar facilmente o montante de R$15.000,00 logo no primeiro ano, proporcionando ao franqueado uma lucratividade liquida de mais de 30%, e um retorno em aproximadamente 15 meses. Chegando ao Faturamento Bruto mensal de mais de R$60.000,00 ao final do terceiro periodo, com uma lucratividade de mais de 35%, e uma rentabilidade de mais de 20%.<br><br>
                - Não é necessário investir em base de monitoramento;<br>
                - O grupo oferece estrutura de RH;<br>
                - O custo para montar uma base envolve no mínimo 4 profissionais em regime 12x36 representando um custo fixo e mensal de mais de aproximadamente R$12.000,00;<br>
                - Todas as atividades e procedimentos de acomapanhamento de cliente serão realizadas pela central do Grupo Vigivel, cabendo ao franqueado apenas a venda, instalação e atendimento de disparos;<br>
                - Todos os procedimentos serão passados ao franqueado atráves contrato de franquia após avaliação da COF (Carta de oferta de franquia).<br><br>
                Para ter acesso a projeção de resultados de sua região, solicite o Plano de Negócios a nossa central de atendimento.
                </p>
            </li>

        </ul>
    </div>
    <figure class="col s12 l4">
        <div class="image">
            <img class="lazy" src="{{ asset('img/protect-full.jpg') }}" data-original="{{ asset('img/protect-full.jpg') }}" alt="Protect Full">
        </div>
    </figure>
</article>


<div class="section row">
    <aside class="col s12 l4">
        <header class="heading" role="heading">
            <h2 class="label bold"><strong>VANTAGENS</strong> DO FRANQUEADO</h2>
        </header>
        <ul class="feed-list">
            <li><p>Baixo investimento para iniciar as atividades;</p></li>
            <li><p>Treinamento e apoio continuo as operações;</p></li>
            <li><p>Treinamento para capacitações;</p></li>
            <li><p>Treinamento e consultoria constante;</p></li>
        </ul>
    </aside>
    @include('partials/form-franquia')
</div>