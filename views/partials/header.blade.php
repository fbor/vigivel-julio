<div id="acesso-restrito" class="main-dash">
	@include('partials/acesso-restrito')
</div>
<header id="topo" class="main-header @yield('navbar', 'horizontal')">
	<section class="appbar" role="banner">

		<input type="checkbox" id="togglemenu" hidden>
		<label for="togglemenu" class="togglemenu close"></label>

		<label for="togglemenu" class="togglemenu open" aria-label="Menu de navegação">
            <i class="ico-nav" aria-hidden="true"></i>
            <span class="label">Menu</span>
        </label>

		@if(isset($errorView) || Request::route()->getName() == "home")
		<h1 class="app-title">
			<span class="logo">
				<img src="{{ asset('img/vigivel-seguranca.png') }}" alt="{{ config('app.name') }}" title="{{ config('app.name') }}">
			</span>
		</h1>
		@else
		<h2 class="app-title">
			<a class="logo" href="{{ route('home') }}">
				<img src="{{ asset('img/vigivel-seguranca.png') }}" alt="{{ config('app.name') }}" title="{{ config('app.name') }}">
			</a>
		</h2>
		@endif


		<nav class="main-navigation">
			<ul class="navbar" itemscope itemtype="http://www.schema.org/SiteNavigationElement">
				<li>
					<a itemprop="url" class="nav-item home hidden" href="{{ route('home') }}">
						<span itemprop="name">Início</span>
					</a>
				</li>
				<li>
					<a itemprop="url" class="nav-item about" href="{{ route('about') }}" title="Conheça nossa história">
						<span itemprop="name">Sobre nós</span>
					</a>
				</li>
				<li>
					<a itemprop="url" class="nav-item service" href="{{ route('service') }}" title="Confira nossos serviços">
						<span itemprop="name">Serviços</span>
					</a>
				</li>
				<li>
					<a itemprop="url" class="nav-item franquia" href="{{ route('franquia') }}" title="Vantagens de ser uma franquia">
						<span itemprop="name">Franquia Vigivel</span>
					</a>
				</li>
				<li>
					<a itemprop="url" class="nav-item orcamento" href="{{ route('orcamento') }}" title="Solicite um orçamento">
						<span itemprop="name">Orçamento</span>
					</a>
				</li>

				<li>
					<a itemprop="url" class="nav-item work" href="{{ route('work') }}" title="Trabalhe conosco">
						<span itemprop="name">Trabalhe conosco</span>
					</a>
				</li>
			</ul>
			<div class="toolbar">
				<button id="toggleRestrito" class="toggle restrict" title="Acesso Restrito">
					<i class="icon ico-user"></i> Acesso Restrito
				</button>
			</div>
		</nav>

	</section>


	@if(isset($errorView) || Request::route()->getName() == "home")
	<aside class="central-de-atendimento">
		<p class="caption">CENTRAL DE ATENDIMENTO</p>
		<h3 class="label large">0800 400 8008</h3>
	</aside>
	@endif

</header>