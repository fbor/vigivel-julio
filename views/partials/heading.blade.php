<header class="heading">
	<h1 class="title" itemprop="headline">{{ $title }}</h1>
	@if ( isset($subtitle) )
	<h4 class="caption" itemprop="description">{{ $subtitle }}</h4>
	@endif
</header>