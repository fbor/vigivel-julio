<form class="form validate col s12 l8" itemprop="mainEntityOfPage" action="/franquia/sender" method="post">
    {{ csrf_field() }}
    <header class="heading">
        <h4 class="title"><strong>Seja uma franquia</strong></h4>
        <p class="caption">Deixe seu dados abaixo entramos em contato com você para mais informações!</p>
    </header>
    <fieldset class="row collapse">
        <div class="field col s12 m6">
            <label for="name">Nome completo:</label>
            <input id="name" type="text" name="name" placeholder="Seu nome" minlength="3" required>
        </div>
        <div class="field col s12 m6">
            <label for="email">E-mail:</label>
            <input id="email" type="email" name="email" placeholder="seu@email.com" required>
        </div>
        <div class="field col s12 m6">
            <label for="tel">Telefone:</label>
            <input id="tel" type="tel" name="phone_number" placeholder="(xx) 9999-99999" required>
        </div>
        <div class="field col s12 m6">
            <label for="cidade">Sua cidade:</label>
            <input id="cidade" type="text" name="city" placeholder="Onde reside" required>
        </div>
            <input id="servico" type="hidden" name="servico" value="{{$slug}}" required>


        <div class="field col s12">
            <button type="submit" class="submit block">ENVIAR</button>
        </div>

    </fieldset>

    <div class="form-msg alert">
        <p class="message"></p>
        <button class="closed"></button>
    </div>

</form>