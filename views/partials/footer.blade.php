
<footer class="main-footer container">

    <div class="wrapper" role="contentinfo" itemscope itemtype="http://schema.org/LocalBusiness">

        <meta itemprop="url" content="{{ asset('/') }}">
        <meta itemprop="name" content="{{ config('app.name') }}">
        <meta itemprop="description" content="{{ config('app.slogan') }}">
        <meta itemprop="image" content="{{ asset('img/vigivel-seguranca.png') }}">
        <meta itemprop="telephone" content="{{ config('app.phone_number') }}">

        <div class="row collapse footer-content">

            <div class="col s12 l3">
                <div class="heading" role="heading">
                    <h5 class="title bold">ONDE ESTAMOS</h5>
                </div>

                <ul class="widget-address">
                    <li class="address-item" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                        <span itemprop="streetAddress">{{ config('app.street_address') }}</span><br>
                        <span itemprop="postalCode">{{ config('app.postal_code') }}</span>,
                        <span itemprop="addressLocality">{{ config('app.locality') }}</span> - <span itemprop="addressRegion">{{ config('app.region') }}</span>.
                    </li>
                    <li class="address-item central">
                        <p class="info">CENTRAL DE ATENDIMENTO:</p>
                        <h5 class="tel">{{ config('app.phone_number') }}</h5>
                        <h5 class="tel">(45) 3037-6079</h5>
                    </li>
                </ul>

            </div>


            <div class="col s12 m6 l5 offset-l1">
                <div class="heading" role="heading">
                    <h5 class="title bold">ONDE ENCONTRAR</h5>
                </div>

                <h6>Confira onde encontrar uma <strong>franquia Vigivel</strong></h6>
                <a href="{{ route('franquia') }}#onde-encontrar" class="btn upp">Franquia Vigivel</a>
            </div>


            <aside class="col s12 m6 l3 box-social">
                <div class="heading" role="heading">
                    <h5 class="title bold">SIGA-NOS</h5>
                </div>
                <ul class="ui-social">
                    <li>
                        <a itemprop="sameAs" target="_blank" href="{{ config('app.social_facebook') }}" class="social facebook">
                            <span class="caption">Facebook</span>
                        </a>
                    </li>
                    <li>
                        <a itemprop="sameAs" target="_blank" href="{{ config('app.social_instagram') }}" class="social instagram">
                            <span class="caption">Instagram</span>
                        </a>
                    </li>
                    <li>
                        <a itemprop="sameAs" target="_blank" href="https://www.youtube.com/channel/UCh_XQ_4OBPEyke2WbVXgu-A" class="social youtube">
                            <span class="caption">Youtube</span>
                        </a>
                    </li>
                </ul>
            </aside>

            <div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                <meta itemprop="ratingValue" content="4.5">
                <meta itemprop="reviewCount" content="250">
            </div>
            <div itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates">
                <meta itemprop="latitude" content="{{ config('app.latitude') }}">
                <meta itemprop="longitude" content="{{ config('app.longitude') }}">
            </div>

        </div>
    </div><!-- .wrapper -->


    <div class="footer-down">
        <ol class="wrapper">
            <li class="copyright">
                <p>� {{ config('app.name') }}  2017.</p>
            </li>
            <li class="goutnix">
                <a href="http://goutnix.com.br/" target="_blank" title="Desenvolvido por Goutnix Ag�ncia Digital">goutnix.com.br</a>
            </li>
        </ol>
    </div>


</footer>