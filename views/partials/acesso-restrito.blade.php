<aside class="widget-portal">
    <a href="http://intranet.vigivel.com.br" class="portal">
        <i class="icon ico-company"></i>
        <h6 class="info">Portal do</h6>
        <h4 class="large"><strong>Franqueado</strong></h4>
    </a>
    <a href="{{ route('portal_cliente') }}" class="portal">
        <i class="icon ico-user"></i>
        <h6 class="info">Portal do</h6>
        <h4 class="large"><strong>Cliente</strong></h4>
    </a>
    <a href="http://intranet.vigivel.com.br" class="portal">
        <i class="icon ico-team"></i>
        <h6 class="info">Portal do</h6>
        <h4 class="large"><strong>Colaborador</strong></h4>
    </a>
    <a href="{{ route('portal_fornecedor') }}" class="portal">
        <i class="icon ico-service"></i>
        <h6 class="info">Portal do</h6>
        <h4 class="large"><strong>Fornecedor</strong></h4>
    </a>
</aside><!-- .dash -->
