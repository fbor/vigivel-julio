<h4 class="title bold">{{ $state->sigla }}</h4>

@if($representantes->count() > 0)
<table>
    <thead>
        <tr>
            <th>Representante</th>
            <th>Contato</th>
            <th>Cidade</th>
        </tr>
    </thead>
    <tbody>
        @foreach($representantes as $representante)
            <tr>
                <td><strong>{{ $representante->label }}</strong></td>
                <td>{{ $representante->telefone }}<br>{{ $representante->email }}</td>
                <td>{{ $representante->city->label }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
@else
<p>Nenhum representante para esse estado.</p>
@endif