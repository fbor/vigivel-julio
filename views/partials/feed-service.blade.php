<figure class="picture">
    <i class="icon">
        @if ( isset($smallTitle) )
        <img class="lazy" src="{{ asset('img/loading.svg') }}" data-original="{{ $categoria->image->present()->render('thumb')}}" itemprop="image">
        @else
        <img src="{{ asset('img/loading.svg') }}" data-lazy="{{ $categoria->image->present()->render('thumb')}}" itemprop="image">
        @endif
    </i>
</figure>
<hgroup class="heading">
    @if ( isset($smallTitle) )
    <h5 class="title bold" itemprop="name">{{ $categoria->categoria }}</h5>
    @else
    <h4 class="title bold" itemprop="name">{{ $categoria->categoria }}</h4>
    @endif
    <div class="wrap-list">


    @foreach( $servicos as $servico)

            <?php
            $val = unserialize($servico->categorias);
                    if (isset($val) )
                    {
                        for ($i=0; $i < count($val); $i++)
                        {
                            if($categoria->id == $val[$i])
                            {

                                echo '
                                    <a href="https://vigivel.com.br/nossos-servicos/'.$servico->id.'/'.str_slug($servico->label, '-').'.html">

                                        <i class="icon">
                                            <img class="lazy" data-original="'. $servico->icon->present()->render('thumb') .'">
                                        </i>'. $servico->label .
                                    '</a>' ;

                            }
                        }
                    }

            ?>

        </a>
    @endforeach

    </div>
    <meta itemprop="position" content="">
</hgroup>
