@extends('master')
@section('mainView', 'pg-about')
@section('title', 'Sobre Nós | ' . config('app.name'))
@section('schema', 'AboutPage')
@section('description', '[--meta description--]')

@section('content')
    <section class="wrapper container about" role="main">
        
        <header class="heading hidden">
            <h1 class="title" itemprop="headline">Sobre Nós</h1>
        </header>

        <div class="section row">
            <figure class="col s12 l8">
                <iframe width="100%" height="460" src="https://www.youtube.com/embed/mBczd3zI5ic" frameborder="0" allowfullscreen></iframe>
            </figure>
            <div class="col s12 l4 text-center">
                <div class="heading">
                    <h4 class="title bold">Inovando através da tecnologia</h4>
                </div>
                <a href="{{ route('service') }}" class="btn">NOSSOS SERVIÇOS</a>
            </div>
        </div>

        <article class="section row" itemprop="mainEntityOfPage">
            <header class="col s12 m6 l7">

                <div class="heading" role="heading">
                    <h2 class="title large">Quem somos</h2>
                </div>
                <p>
                    O Grupo Vigivel surgiu em meados do ano de 2008, com a proposta de trazer para seus clientes o que havia de mais moderno em termos de segurança, seja em material, mas principalmente em recursos humanos.<br><br>
                    Inicialmente formada com o objetivo principal de atuar no ramo de segurança para eventos, com o passar dos anos foi mudando seu foco, embora ainda hoje seja uma das empresas mais requisitadas para atuar nesse tipo de serviço, com personalidade forte a empresa teve um crescimento lento até o ano de 2011, quando efetivamente começou a passar por transformações internas, como setorização e troca da administração da empresa, com um ex-oficial do exército a frente da administração a empresa teve um crescimento expressivo nos dois primeiros anos com aumento de mais de 1000% de faturamento bruto. <br><br>
                    No ano de 2014 a empresa decidiu ampliar seus serviços para o ramo de segurança eletrônica e passa a oferecer produtos como câmeras de segurança, sistemas de alarmes monitorado e cerca elétrica, bem como serviços paralelos como instalação de interfones, portão eletrônicos entre outros.<br><br>
                    O alto custo inicial para implantação de tal serviço representou um grande investimento por parte da empresa principalmente em pessoal especializado no ramo de segurança eletrônica, contudo proporcionou o aprimoramento do sistema de gestão bem como evolução jurídica da empresa. <br><br>
                    Atualmente a empresa possui uma estrutura financeira própria, não dependendo de capital de terceiros para sua subsistência, conta atualmente com uma frota destinada para os serviços de escolta armada, vigilância, serviço técnico de instalação de sistemas de segurança, jardinagem e outros.
                </p>

            </header>
            <figure class="col s12 m6 l4 offset-l1 picture">
                <div class="image">
                    <img class="lazy" src="{{ asset('img/about-min.jpg') }}" data-original="{{ asset('img/about.jpg') }}" alt="Quem somos">
                </div>
            </figure>
        </article>

        <aside class="section row text-center">
            <article class="col s12 m4">
                <h4 class="title bold"><strong>MISSÃO</strong></h4>
                <p>Prestar serviços de segurança com foco na eficiência, eficácia e transparência, contribuindo para o crescimento do seguimento de segurança no país, constituindo-se na primeira franqueadora do segmento junto a ABF (Associação Brasileira de Franquias) e impactando de forma positiva na percepção de segurança de toda a população, principalmente dos nossos clientes.</p>
            </article>
            <article class="col s12 m4">
                <h4 class="title bold"><strong>VISÃO</strong></h4>
                <p>Ser o maior grupo de empresas do ramo de segurança do estado do Paraná até 2021, sendo reconhecida pelo seu alto padrão de qualidade em todas as suas franquias.</p>
            </article>
            <article class="col s12 m4">
                <h4 class="title bold"><strong>VALORES</strong></h4>
                <p>Ética e Transparência: É nosso princípio e deve nortear todas as nossas ações, devemos ser conhecidos por nossos clientes, fornecedores e colaboradores pela nossa atitude clara e bem definida, com foco na moralidade e legalidade.<br>Inovação: Procurar o aprimoramento contínuo de nossos colaboradores e procedimentos, inovando através da tecnologia de forma a tornar o serviço palpável a nossos clientes.</p>
            </article>
        </aside>
        
    </section>

    <section class="client wrapper container collapse-top section">
        <header class="heading">
            <h3 class="title bold large">EMPRESAS DO <strong>GRUPO</strong></h3>
        </header>

        <ul id="slider-clients" class="row feed-list">

            @foreach($empresas as $empresa)
            <li class="col s12 m6 l3 feed-item">
                <div class="logo">
                    @if($empresa->link)
                        <a href="{{ $empresa->link }}" target="{{ $empresa->target }}">
                            <img src="{{ asset('img/loading.svg') }}" data-lazy="{{ $empresa->image->present()->render('thumb') }}" alt="{{ $empresa->titulo }}">
                        </a>
                    @else
                        <img src="{{ asset('img/loading.svg') }}" data-lazy="{{ $empresa->image->present()->render('thumb') }}" alt="{{ $empresa->titulo }}">
                    @endif
                </div>
            </li>
            @endforeach

        </ul>
    </section>

    <!-- <figure class="about-gallery single-slider">
        <ul class="slick-slider">
            <li><img src="{{ asset('img/1.jpg') }}" alt="{{ config('app.title') }}"></li>
            <li><img src="{{ asset('img/2.jpg') }}" alt="{{ config('app.title') }}"></li>
            <li><img src="{{ asset('img/3.jpg') }}" alt="{{ config('app.title') }}"></li>
            <li><img src="{{ asset('img/4.jpg') }}" alt="{{ config('app.title') }}"></li>
            <li><img src="{{ asset('img/5.jpg') }}" alt="{{ config('app.title') }}"></li>
        </ul>
    </figure> -->
@stop