@extends('master')
@section('title', 'Dashboard do franquiado | ' . config('app.name'))
@section('schema', 'ContactPage')
@section('mainView', 'pg-dashboard')
@section('description', '[--meta description--]')

@section('content')
    <section class="wrapper container" role="main">
        <header class="heading">
            <h4 class="caption" itemprop="description"><small class="upp">Portal do</small></h4>
            <h1 class="title" itemprop="headline"><strong>Franqueado</strong></h1>
        </header>

    	<div class="row">
            <form class="col s12 l5 form validate">
            
                <fieldset class="row collapse">
                    <div class="field col s12">
                        <label for="user">Usuário/E-mail:</label>
                        <input id="user" type="text" name="user" placeholder="Login" required>
                    </div>
                    <div class="field col s12">
                        <label for="pass">Senha:</label>
                        <input id="pass" type="password" name="pa55" placeholder="Senha de acesso" required>
                    </div>

                    <div class="field col s12">
                        <button type="submit" class="submit block">ENTRAR</button>
                    </div>

                </fieldset>

                <div class="form-msg alert">
                    <p class="message"></p>
                    <button class="closed"></button>
                </div>

            </form>

            <aside class="col s12 l5 offset-l2 sidebar pg-franquia">
                <div class="widget">
                    <header class="heading" role="heading">
                        <h2 class="label bold"><strong>VANTAGENS</strong> DO FRANQUEADO</h2>
                    </header>
                    <ul class="feed-list">
                        <li><p>Baixo investimento para iniciar as atividades;</p></li>
                        <li><p>Treinamento e apoio continuo as operações;</p></li>
                        <li><p>Treinamento para capacitações;</p></li>
                        <li><p>Treinamento e consultoria constante;</p></li>
                    </ul>
                </div>
            </aside>
        </div>
    </section>

    
@stop