<?php $errorView = true; ?>
<!DOCTYPE html>
<html>
<head>
    <title>Serviço Temporariamente Indisponível.</title>
    
    <link rel="stylesheet" href="{{ asset('css/basic.css') }}">
     <link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet"> 

    <style>
        .error-page { font-family: 'Lato', sans-serif; }
    </style>
</head>
<body class="@yield('mainView', 'error')">

    @include('../partials/header')

    <main class="wrapper text-center">
        <div class="error-page content">
            <div class="code">503</div>
            <p class="title">Serviço Temporariamente Indisponível</p>

            <p class="resume">O servidor no momento não pode processar a solicitação gerada pela aplicação.</p>
            <p class="info">Tente novamente... Se o problema persistir, contate o suporte técnico da organização!</p>
        </div>
    </main>

</body>
</html>