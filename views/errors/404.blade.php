<?php $errorView = true; ?>
<!DOCTYPE html>
<html>
<head>
    <title>Página não encontrada.</title>
    
    <link rel="stylesheet" href="{{ asset('css/basic.css') }}">
     <link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet"> 

    <style>
        .error-page { font-family: 'Lato', sans-serif; }
    </style>
</head>
<body class="@yield('mainView', 'error')">

    @include('../partials/header')

    <main class="wrapper text-center">
        <div class="error-page content">
            <div class="code">404</div>
            <p class="title"><b>Ooops!</b> Página não encontrada.</p>

            <p class="resume">
                Não sabemos como você chegou até aqui,<br> 
                mais fique tranquilo e nem tudo está perdido!<br>
                <span class="upp">Continue navegando...</span>
            </p>
            <a href="{{ route('home') }}" class="btn">Pagina Inícial</a>
        </div>
    </main>

</body>
</html>
