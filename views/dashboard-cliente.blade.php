@extends('master')
@section('title', 'Portal do Cliente | ' . config('app.name'))
@section('mainView', 'pg-dashboard')
@section('description', 'Para facilitar sua vida e ganhar tempo, foi criado o Portal do Cliente. Nele você acessa a sua conta e tem acesso a todos os recursos de forma facil e rápida!')


@section('content')
    <section class="wrapper container" role="main">
        <header class="heading text-center">
            <h4 class="caption" itemprop="description"><small class="upp">Portal do</small></h4>
            <h1 class="title" itemprop="headline"><strong>Cliente</strong></h1>
            <h6>Para facilitar sua vida e ganhar tempo, foi criado o Portal do Cliente. Nele você acessa a sua conta para:</h6>
        </header>

        <style>
        .grid-container{
            display:grid;
            grid-template-columns: 50% 50%
        }
        </style>

        <article class="grid-container">

            <section>

            <h2>Softwares</h2>

            <ul class="row cycle feedlist dashboard">
            @if($software)
                @foreach ($software as $item)
                    <li class="col s10">
                        <a class="feeditem" href="{{ $item['link'] }}" target="_blank">{{ $item['titulo'] }} </a>
                    </li>
                @endforeach
            @endif
            </ul>

            </section>

            <section>

            <h2>Gerencial</h2>

            <ul class="row cycle feedlist dashboard">
            @if($gerencial)
                @foreach ($gerencial as $item)
                    <li class="col s10">
                        <a class="feeditem" href="{{ $item['link'] }}" target="_blank">{{ $item['titulo'] }}</a>
                    </li>
                @endforeach
            @endif
            </ul>
            </section>
        </article>

        <div class="solicitar-acesso container-toggle">
            <hr class="line space">
            <button class="btn btn-toggle" target="#form-toggle">Solicitar Acesso</button>
            <form id="form-toggle" class="form validate container hidden" action={{ route('portal_cliente') }} method="post">
                {{ csrf_field() }}

    			<fieldset class="row collapse">
    				<div class="field col s12 m6 l4">
    					<label for="name">Nome:</label>
    					<input id="name" type="text" name="name" placeholder="Nome Completo:" minlength="3" required>
    				</div>
    				<div class="field col s12 m6 l4">
    					<label for="email">E-mail:</label>
    					<input id="email" type="email" name="email" placeholder="seu@email.com" required>
    				</div>
                    <div class="field col s12 m6 l4">
                        <label for="tel">Telefone:</label>
                        <input id="tel" type="tel" name="phone_number" placeholder="(xx) 9999-99999" required>
                    </div>

    				<div class="field col s12 l8">
                        <ul class="row collapse checked-service">
                            <?php $count = 0; ?>
                            @foreach ($software as $item)
                            <?php $count++; ?>
                            <li class="col s12 m6">
                                <label for="checkbox{{ $count }}" class="checkbox">
                                    <input id="checkbox{{ $count }}" type="checkbox" name="servico" value="{{ $item['label'] }}" required>
                                    <span class="caption">{{ $item['label'] }}</span>
                                </label>
                            </li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="field col s12 l4">
                        <button type="submit" class="submit block">SOLICITAR ACESSO</button>
                    </div>

                </fieldset>

                <div class="form-msg alert">
                    <p class="message"></p>
                    <button class="closed"></button>
                </div>
            </form>
        </div>
    </section>
@stop

@section('scripts')
<script src="{{ asset('js/app.js') }}"></script>
@stop
