@extends('master')
@section('title', 'Fale conosco | ' . config('app.name'))
@section('schema', 'ContactPage')
@section('mainView', 'pg-contact')
@section('description', '[--meta description--]')

@section('content')
    <section class="wrapper" role="main">
        
        @include('partials/heading', ['title'=>'Fale conosco', 'subtitle'=>'Envie sua mensagem preenchendo o formulário abaixo'])

    	<form class="form validate container" itemprop="mainEntityOfPage" action="{{ route('contact') }}" method="post">
            {{ csrf_field() }}
    		
			<fieldset class="row collapse">
				<div class="field col s12 m6 l4">
					<label for="name">Nome completo:</label>
					<input id="name" type="text" name="name" placeholder="Seu nome" minlength="3" required>
				</div>
				<div class="field col s12 m6 l4">
					<label for="email">E-mail:</label>
					<input id="email" type="email" name="email" placeholder="seu@email.com" required>
				</div>
                <div class="field col s12 m6 l4">
                    <label for="tel">Telefone:</label>
                    <input id="tel" type="tel" name="phone_number" placeholder="(xx) 9999-99999" required>
                </div>

				<div class="field col s12 l8">
                    <textarea name="observacao" rows="5" placeholder="Sua mensagem:"></textarea>
                </div>


                <div class="field col s12 l4">
                    <div class="g-recaptcha"
                          data-sitekey="6LeqqBoUAAAAAJKTOUN_7MbVjvLapgQVcbXUPec_"
                          data-callback="onSubmit"
                          data-size="visible">
                    </div>
                </div>

                <div class="field col s12 l4">
                    <button type="submit" class="submit block">ENVIAR MENSAGEM</button>
                </div>

            </fieldset>

            <div class="form-msg alert">
                <p class="message"></p>
                <button class="closed"></button>
            </div>

    	</form>
    </section>

    <aside id="onde-estamos" class="map-canvas">

        <div class="wrapper collapse">
            <form action="javascript: void(0);" class="form como-chegar">
                <fieldset class="row collapse">
                    <div class="col s12 l4">
                        <h4 class="title upp bold">Como chegar</h4>
                    </div>
                    <div class="col s12 m6 l4 shared-location">
                        <button id="origin-button" class="btn primary" type="button">SUA LOCALIZAÇÃO</button>
                    </div>
                    <!-- <div class="col s12 m6 l4">
                        <input id="origin-input" type="text" name="origin" placeholder="Seu endereço:" required>
                    </div> -->
                </fieldset>
            </form>            
        </div>

        <div id="map_canvas"></div>
    </aside>
@stop


@section('scripts')
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
@stop
