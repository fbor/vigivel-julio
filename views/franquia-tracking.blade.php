<header class="heading wrap-picture">
    <i class="box-image"><img src="{{ asset('img/franquia-tracking.jpg') }}" alt="TRACKING"></i>
    <h6 class="caption">Franquia Vigivel</h6>
    <h1 class="title" itemprop="headline"><strong>TRACKING</strong></h1>
</header>


<article class="section row" itemprop="mainEntityOfPage">
    <div class="col s12 l8">
        <ul class="feed-list">
            <li>
            <h6 class="caption">Rastreamento Veicular</h6>
                <p>
                O mercado de rastreamento veicular mantém um ritmo de crescimento maior que os demais mercados. Estimativas apontam um crescimento médio de 15% ao ano entre 2016 e 2020. Agora é o momento certo para iniciar um negócio nessa área!<br><br>
                </p>
            <h6 class="caption">O que é a Franquia Tracking do Grupo Vigivel?</h6>
                <p>
                Este modelo de franquia é destinado a empresários, que já possuem estrutura física, como oficinas, auto-elétricas, lojas de som automotivo que queiram agregar mais um serviço com rendimento fixo mensal.<br><br>
                </p>
                <h6 class="caption">Perfil do Cliente</h6>
                <p>
                Destacamos como clientes em potencial e que representam grande parcela do mercado a ser explorado empresas frotistas, essas empresas possuem uma necessidade muito grande de ter controle sob as atividades, rotas, forma de pilotagem, velocidade, controle de condutor dentre outros e são atraidas pela plataforma Web que disponibiliza relatórios diversos que permitem esse controle tão desejado, contudo não podemos esquecer das pessoas físicas, com necessidades distintas aos das pessoas jurídicas, esse tipo de cliente busca ter o controle sobre seu veiculo e geralmente é encantado pelo sistema de bloqueio via APP.<br><br>
                </p>
                <h6 class="caption">Expectativa do Consumidor</h6>
                <p>
                Pessoa Física<br>
                - Controle de seu veículo quando em poder de outras pessoas;<br>
                - Bloqueio via APP evitando furto ou facilitando a recuperação do veículo em caso de roubo.<br><br>
                Pessoa Jurídica<br>
                - Controle sobre sua frota e motoristas;<br>
                - Redução de ônus com multas de transito;<br>
                - Controle de Abertura e Fechamento de compartimentos de carga;<br>
                - Controle de Velocidade;<br>
                - Controle de perímetro ou rota por veículo;<br>
                - Relatório de desgaste em caso de acionamento excessivo de embreagem;<br>
                - Redução de consumo com a visualização como giro de motor;<br>
                - Redução de manutenção com relatório de freadas bruscas;<br>
                - Controle de visitas realizadas por colaboradores a clientes;<br>
                - Acompanhamento em tempo real dos deslocamentos feitos pela equipe de trabalho;<br>
                - Controle de Horas de condução feita pelos motoristas da empresa e consequentemente redução de ônus trabalhista;<br>
                - Telemetria por condutor.<br><br>
                </p>
                <h6 class="caption">Diferenciais do Franqueado Vigivel</h6>
                <p>
                - Nenhum investimento em Software: O franqueado receberá sem custo algum todos os sofwares que auxiliaram e possibilitarão toda a prestação de serviço, bem como todos os manuais de operação facilitando a retirada de duvidas e proporcionando ao franqueado autonomia operacional;<br>
                - Material de Propaganda e Publicidade: Material padronizado e gratuito aplicável a todas as unidades;<br>
                - Plataforma Gerencial: Proporcionará ao franqueado completo controle sobre sua empresa, auxiliando nas tomadas de decisão;<br>
                - Acesso web a Plataforma Gerencial de qualquer lugar do mundo;<br>
                - Web Site institucional: fornecido pela franqueadora, com contados da franquia local possibilitando acessibilidade e facilidade no contato entre cliente e franquia;<br>
                - Técnicas de atendimento: com base no Know How dos serviços e produtos oferecidos pelo Grupo Vigivel oferecemos técnicas que facilitam auxiliam na conquista e fidelização do cliente;<br>
                - Franquia, equipe e serviços padronizados.<br><br>
                </p>
                <h6 class="caption">Suporte</h6>
                <p>
                Jurídico<br>
                Técnico<br>
                Financeiro<br>
                T.I<br>
                Corporativo<br>
                Comercial<br>
                Marketing<br><br>
                </p>
                <h6 class="caption">Plano de Negócios</h6>
                <p>
                Já pensou em um investimento que traga uma renda mensal, sem custo com a aquisição de material, isso mesmo o Grupo Vigivel não vende equipamentos, vende solução, o franqueado recebe o aparelho de rastreamento sem custo algum, proporcionando competitividade, o franqueado se preocupar em gerenciar estoque, pois nos te mandamos o equipamento totalmente pronto o trabalho do franqueado é realizar a venda e fazer a instalação.<br><br>
                - Chips GPRS totalmente gratuito: nos enviamos os chips configurados os aparelhos habilitados na sua plataforma de rastreamento, basta fazer a instalação e o cliente sai de sua loja com o veiculo rastreado.<br>
                - O rastreamento pode ser feito via GPRS ou via Satelital, sendo possível o rastreamento em qualquer lugar do mundo utilizando nosso sistema.<br>
                - Com a franquia você adquire a licença para utilizar nosso aplicativo, possibilitando ao seu cliente a auto gestão, o próprio cliente em caso de sinistro conseguirá fazer o bloqueamento de seu veiculo.<br>
                - Tutorial de Utilização simplificado facilitando a orientação ao cliente com relação a utilização do sistema, proporcionando ganha de tempo e credibilidade junto ao seu cliente.<br>
                - Adesivo para identificação de veiculos rastreados em modelo padrão, de acordo com sua franquia e personalizado.<br><br>
                Rentabilidade ao final do terceiro ano de mais de 60% com um faturamento bruto aproximado de R$11.000 mantendo uma média de 4 equipamentos por mês com uma receita liquida de mais de R$8.000 por mês ao final do terceiro ano. Faturamento bruto mensal a partir de R$4000,00 ao final do primeiro ano com retorno sobre o investimento no prazo de 10 meses.<br><br> Para ter acesso a projeção de resultados de sua região, solicite o Plano de Negócios a nossa central de atendimento.<br><br>
                </p>
            </li>

        </ul>
    </div>
    <figure class="col s12 l4">
        <div class="image">
            <img class="lazy" src="{{ asset('img/tracking.jpg') }}" data-original="{{ asset('img/tracking.jpg') }}" alt="Tracking">
        </div>
    </figure>
</article>


<div class="section row">
    <aside class="col s12 l4">
        <header class="heading" role="heading">
            <h2 class="label bold"><strong>VANTAGENS</strong> DO FRANQUEADO</h2>
        </header>
        <ul class="feed-list">
            <li><p>Baixo investimento para iniciar as atividades;</p></li>
            <li><p>Treinamento e apoio continuo as operações;</p></li>
            <li><p>Treinamento para capacitações;</p></li>
            <li><p>Treinamento e consultoria constante.</p></li>
        </ul>
    </aside>
    @include('partials/form-franquia')
</div>