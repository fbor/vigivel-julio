<!DOCTYPE html>
<html lang="pt-br">
<head itemscope itemtype="http://schema.org/WebSite">
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="canonical" href="{{ url()->full() }}">
    <title itemprop="name">@yield('title', config('app.title'))</title>
    <meta name="description" content="@yield('description', config('app.description'))">
    <meta name="base_url" itemprop="url" content="{{ asset('/') }}">
    <meta name="rating" content="general">
    <meta name="robots" content="index, follow">

    <meta property="og:locale" content="pt_BR">
    <meta property="og:site_name" content="{{ config('app.name') }}">

    <meta property="og:type" content="website">
    <meta property="og:title" content="@yield('title', config('app.title') )">
    <meta property="og:description" content="@yield('description', config('app.description') )">
    <meta property="og:image" content="@yield('og-image', asset('img/vigivel-og.jpg') )">

    <meta name="fb:page_id" content="{{ config('app.page_id') }}">

    <meta name="og:email" content="{{ config('app.email') }}">
	<meta name="og:phone_number" content="{{ config('app.phone_number') }}">
	<meta name="og:latitude" content="{{ config('app.latitude') }}">
	<meta name="og:longitude" content="{{ config('app.longitude') }}">
	<meta name="og:street-address" content="{{ config('app.street_address') }}">
	<meta name="og:locality" content="{{ config('app.locality') }}">
	<meta name="og:region" content="{{ config('app.region') }}">
	<meta name="og:postal-code" content="{{ config('app.postal_code') }}">
	<meta name="og:country-name" content="BR">

	<meta name="google-site-verification" content="">
	<meta name="copyright" content="©2016 Goutnix Agência Digital">

    <!-- Disable tap highlight on IE -->
    <meta name="msapplication-tap-highlight" content="no">

    <!-- Web Application Manifest -->
    <link rel="manifest" href="{{ asset('manifest.json') }}">
	<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicons/apple-touch-icon.png') }}">
	<link rel="icon" type="image/png" href="{{ asset('favicons/favicon-32x32.png') }}" sizes="32x32">
	<link rel="icon" type="image/png" href="{{ asset('favicons/favicon-16x16.png') }}" sizes="16x16">
	<link rel="mask-icon" href="{{ asset('favicons/safari-pinned-tab.svg') }}" color="#2b5797">
	<meta name="theme-color" content="#3f5498">
	<meta name="msapplication-config" content="{{ asset('browserconfig.xml') }}">

	<link rel="stylesheet" href="{{ asset('css/basic.css') }}">


        <script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="980689a0-1484-4d84-93f4-9a7ae7a3a4b0";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>

</head>
<body class="body @yield('mainView', 'pg-main')" itemscope itemtype="http://schema.org/@yield('schema', 'WebPage')">

	@include('partials/header')
	<main class="main-view">
		@yield('content')
	</main>
	@include('partials/footer')


	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/styles_q.css') }}">
	@yield('styles')

	<!-- scripts -->
	<script src="{{ asset('js/all.js') }}"></script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-77832258-1', 'auto');
	  ga('send', 'pageview');

	</script>
	@yield('scripts')

    <script type="text/javascript" async src="https://d335luupugsy2.cloudfront.net/js/loader-scripts/dadfe024-cea6-459c-ac65-3960b303f0b7-loader.js" ></script>
	<script async src="{{ asset('js/bundle.js')}}"></script>


</body>
</html>