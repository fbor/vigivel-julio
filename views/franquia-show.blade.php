<?php

    $data = [
        'title' => 'Franquia'
    ];

    switch ($slug) {
        case 'tracking':
            $data['title'] = 'TRACKING';
            break;
        case 'protect-basic':
            $data['title'] = 'PROTECT BASIC';
            break;
        case 'protect-plus':
            $data['title'] = 'PROTECT PLUS';
            break;
        case 'protect-full':
            $data['title'] = 'PROTECT FULL';
            break;
    }

?>
@extends('master')
@section('mainView', 'pg-franquia-show')
@section('title', $data['title'] . ' | ' . config('app.name'))

@section('content')
    <div class="ui-breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
        <ol class="wrapper">
            <li class="breadcrumb-label"><span>Você está:</span></li>
            <li class="breadcrumb-item home" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="{{ route('home') }}"><span itemprop="name">Início</span></a></li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <a href="{{ route('franquia') }}" title="Franquia Vigivel">
                    <span itemprop="name">Franquia Vigivel</span>
                </a>
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <span itemprop="name">{{ $data['title'] }}</span>
            </li>
        </ol>
    </div><!-- .breadcrumb -->

    <section class="wrapper container about" role="main">
        @include('franquia-' . $slug)
    </section>
@stop