@extends('master')
@section('mainView', 'pg-service')
@section('title', 'Serviços | ' . config('app.name'))
@section('description', 'Grupo Vigivel tudo em segurança eletrônica e física, Escolta Armada, Segurança Para Eventos, Controle de Acesso e Portaria 24hs, Rastreamento Monitoramento')

@section('content')
    <section class="wrapper" role="main">
        
        <header class="heading text-center">
            <h1 class="title" itemprop="headline">Nossos Serviços</h1>
            <h6 class="caption upp" itemprop="description"><small>Grupo Vigivel tudo em segurança eletrônica e física, Escolta Armada, Segurança Para Eventos, Controle de Acesso e Portaria 24hs, Rastreamento Monitoramento</small></h6>
        </header>


        <div class="row cycle feed-list service" itemscope itemtype="http://schema.org/ItemList">

            <link itemprop="url" href="{{ url()->full() }}">
            <meta itemprop="numberOfItems" content="{{ $categorias->count() }}">

            @foreach($categorias as $categoria)
            <article class="feed-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/Product">
                @include('partials/feed-service', ['smallTitle' => true])
            </article>
            @endforeach

        </div>
        
    </section>
@stop