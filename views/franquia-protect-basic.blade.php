<header class="heading wrap-picture">
    <i class="box-image"><img src="{{ asset('img/icon-protect-basic.jpg') }}" alt="Protect Basic"></i>
    <h6 class="caption">Franquia Vigivel</h6>
    <h1 class="title" itemprop="headline"><strong>Protect Basic</strong></h1>
</header>


<article class="section row" itemprop="mainEntityOfPage">
    <div class="col s12 l8">
        <ul class="feed-list">
            <li>
            <h6 class="caption">Monitoramento e Venda de Alarmes</h6>
                <p>
                    O mercado formal de segurança eletrônica no Brasil vêm crescendo constantemente, segundo uma pesquisa divulgada pela SIA (Security Industry Association) recentemente. De acordo com a análise, o mercado formal de segurança eletrônica brasileiro teve um volume de negócios de R$ 1,2 bilhão em 2011. Em 2017 espera-se um crescimento de 20,6%, atingindo R$ 3,7 bilhões.<br><br>
                </p>
            <h6 class="caption">O que é a Franquia Protect Basic?</h6>
                <p>
                    Este tipo de franquia deve ser formada por dois sócios, um ligado a área comercial e administrativa e outro com ligação na área técnica. Estes deverão passar pelo processo de treinamento WEB para credenciamento e assim possibilitar a comercialização do serviço de monitoramento de alarmes nas regiões em que possuem interesse.<br><br>
                </p>
            <h6 class="caption">Serviços Disponíveis</h6>
                <p>
                    - Sistema de câmeras: O Sitema é amplo e pensado para atender as principais necessidades do cliente, estes podem ser residenciais ou pessoas com lojas, escritórios e comércios. Para esse mercado oferecemos equimanetos com tecnologia de ponta.<br>
                    - Monitoramento: Instalação de sistema de alarme que, mediante a uma pequena mensalidade, será monitorado dia e noite pela Vigivel. O monitoramento oferece relatórios sobre horários de ativamento e desativamento, entregando ao cliente as informações exatas sobre quem/quando abriu e fechou o patrimônio.<br>
                    - Sharing cam: Também é um sistema de cameras onde o cliente pode gerenciar as imagens através de um aplicativo e armazenamento na nuvem fazendo com que o custo do serviço seja menor. Oferecemos o aplicativo e as câmeras possuem formas de compra e comodato (empréstimo de equipamento)<br>
                    - Portaria Remota: Sistema de Controle de Acesso remoto onde todo controle é realizado através de nossa Central 24h.<br><br>
                </p>
            <h6 class="caption">Perfil do Cliente</h6>
                <p>
                    O serviço de monitoramento tem como foco empresas e pessoas físicas das classes A, B e C.<br><br>
                </p>
            <h6 class="caption">Expectativa do Consumidor</h6>
                <p>
                    - Atendimento rápido;<br>
                    - Segurança;<br>
                    - Confiança;<br>
                    - Transparência;<br>
                    - Comodidade;<br>
                    - Assistência técnica especializada;<br>
                    - Disponibilidade 24hs por parte da empresa.<br><br>
                </p>
            <h6 class="caption">Diferenciais do Franqueado Vigivel</h6>
                <p>
                    - Nenhum investimento em Software: O franqueado receberá sem custo algum todos os sofwares que auxiliaram e possibilitarão toda a prestação de serviço, bem como todos os manuais de operação facilitando a retirada de duvidas e proporcionando ao franqueado autonomia operacional;<br>
                    - Material de Propaganda e Publicidade: Material padronizado e gratuito aplicável a todas as unidades;<br>
                    - Plataforma Gerencial: Proporcionará ao franqueado completo controle sobre sua empresa, auxiliando nas tomadas de decisão;<br>
                    - Acesso web a Plataforma Gerencial de qualquer lugar do mundo;<br>
                    - Web Site institucional: fornecido pela franqueadora, com contados da franquia local possibilitando acessibilidade e facilidade no contato entre cliente e franquia;<br>
                    - Técnicas de atendimento: com base no Know How dos serviços e produtos oferecidos pelo Grupo Vigivel oferecemos técnicas que facilitam auxiliam na conquista e fidelização do cliente;<br>
                    - Franquia,equipe e serviços padronizados.<br><br>
                </p>
            <h6 class="caption">Suporte</h6>
                <p>
                    RH<br>
                    Jurídico<br>
                    Técnico<br>
                    Financeiro<br>
                    T.I<br>
                    Corporativo<br>
                    Comercial<br>
                    Marketing<br>
                    Operações<br><br>
                </p>
                <h6 class="caption">Plano de Negócios</h6>
                <p>
                    Com dedicação em 3 anos o franqueado pode ter lucros líquidos acima de R$14.000 por mês e uma lucratividade de mais de 50%. Ao final do 3 ano uma rentabilidade de mais de 25%, com a compra da franquia serão enviados 60 kits sem custo nenhum para o franqueado Para que o mesmo possa abrir uma cartela de clientes de imediato, na venda através do sistema de comodato pra iniciar as atividades com uma renda bruta de mais de R$7.000,00 desde que consiga vender os 60 kits que sem custo para o cliente representam um grande diferencial competitivo quando na negociação e abertura do mercado local.<br><br>
                    - Não é necessário investir em base de monitoramento;<br>
                    - O custo para montar uma base envolve no mínimo 4 profissionais em regime 12x36 representando um custo fixo e mensal de mais de aproximadamente R$12.000,00;<br>
                    - Todas as atividades e procedimentos de acomapanhamento de cliente serão realizadas pela central do Grupo Vigivel, cabendo ao franqueado apenas a venda, instalação e atendimento de disparos;<br>
                    - Todos os procedimentos serão passados ao franqueado atráves contrato de franquia após avaliação da COF (Carta de oferta de franquia).<br><br>
                    <strong>Para ter acesso a projeção de resultados de sua região, solicite o Plano de Negócios a nossa central de atendimento.</strong>
                </p>
            </li>

        </ul>
    </div>
    <figure class="col s12 l4">
        <div class="image">
            <img class="lazy" src="{{ asset('img/protect-basic.jpg') }}" data-original="{{ asset('img/protect-basic.jpg') }}" alt="Protect Basic">
        </div>
    </figure>
</article>


<div class="section row">
    <aside class="col s12 l4">
        <header class="heading" role="heading">
            <h2 class="label bold"><strong>VANTAGENS</strong> DO FRANQUEADO</h2>
        </header>
        <ul class="feed-list">
            <li><p>Baixo investimento para iniciar as atividades;</p></li>
            <li><p>Treinamento e apoio continuo as operações;</p></li>
            <li><p>Treinamento para capacitações;</p></li>
            <li><p>Treinamento e consultoria constante.</p></li>
        </ul>
    </aside>
    @include('partials/form-franquia')
</div>