@extends('master')
@section('navbar', 'vertical view')
@section('mainView', 'pg-home')

@section('content')

	<div class="main-dash">

        <section class="dash service view" itemscope itemtype="http://schema.org/ItemList">
            <header class="heading main">
                <h6 class="caption">NOSSOS</h6>
                <h2 class="title"><span class="light" itemprop="name">SERVIÇOS</span></h2>
            </header>

            <link itemprop="url" href="{{ url()->full() }}">
            <meta itemprop="numberOfItems" content="{{ $servicos->count() }}">

            <div id="slider-service" class="content feed-list">

                @foreach($servicos as $servico)
                <article class="feed-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/Product">
                    <a href="{{ route('service_show', ['id' => $servico->id, 'slug' => $servico->slug]) }}" itemprop="url">
                        <figure class="picture">
                            <i class="icon">
                                <img class="lazy loading" src="{{ asset('img/loading.svg') }}" data-original="{{ $servico->icon->present()->render('thumb') }}" alt="{{ $servico->label }}" itemprop="image">
                            </i>
                        </figure>
                        <div class="heading">
                            <h6 class="title" itemprop="name"><small>{{ $servico->label }}</small></h6>
                            <meta itemprop="description" content="{{ $servico->resumo }}">
                            <meta itemprop="position" content="{{ $servico->ordem }}">
                        </div>
                    </a>
                </article>
                @endforeach

            </div>
            <a href="{{ route('service') }}" class="btn primary-light">MAIS SERVIÇOS</a>
        </section><!-- .service -->

        <div class="dash seja-uma-franquia view-50">
            <section>
                <header class="heading">
                    <h6 class="caption">Seja uma</h6>
                    <h2 class="title">Franquia</h2>
                    <h5 class="label"><span class="light">do Grupo Vigivel</span></h5>
                </header>

                <p class="info">SEJA UM EMPRESÁRIO<br>DO RAMO DE SEGURANÇA</p>
                <a href="{{ route('franquia') }}" class="btn primary-light">CONHEÇA AS VANTAGENS</a>
            </section>
        </div><!-- .dash -->

        <figure id="banner" class="dash widget-banner view-50">
            <ul class="slick-slider">
                @foreach($banners as $banner)
                    <li>

                        <img src="{{ $banner->image->present()->render('thumb') }}" alt="">{{ isset($banner->titulo) ? $banner->titulo : '' }}

                    </li>
                @endForeach
            </ul>
        </figure>

    </div><!-- .main-dash -->


    <div class="wrapper container about" role="main">
    	<section class="row" itemprop="mainEntityOfPage">
    		<header class="col s12 l7">

                <div class="heading" role="heading">
                    <h2 class="title large">Quem somos</h2>
                </div>
                <p class="resume">O <strong>Grupo Vigivel</strong> surgiu em meados do ano de 2008, com a proposta de trazer para seus clientes o que havia de mais moderno em termos de segurança, seja em material, mas principalmente em recursos humanos.</p>

                <a href="{{ route('about') }}" class="btn more">Conheça mais</a>

            </header>
            <figure class="col s12 l4 offset-l1 picture">
                <div class="image">
                    <img class="lazy" src="{{ asset('img/about-min.jpg') }}" data-original="{{ asset('img/about.jpg') }}" alt="Quem somos">
                </div>
            </figure>
    	</section>
    </div>


    <section class="client wrapper container collapse-top">
        <header class="heading">
            <h3 class="title bold large"><strong>CLIENTES</strong> VIGIVEL</h3>
            <h6 class="caption upp">Eles confiam em nosso trabalho!</h6>
        </header>

        <ul id="slider-clients" class="row feed-list">

            @foreach($clientes as $cliente)
            <li class="col s12 m6 l3 feed-item">
                <div class="logo">
                    @if($cliente->link)
                        <a href="{{ $cliente->link }}" target="{{ $cliente->target }}">
                            <img src="{{ asset('img/loading.svg') }}" data-lazy="{{ $cliente->image->present()->render('thumb') }}" alt="{{ $cliente->titulo }}">
                        </a>
                    @else
                        <img src="{{ asset('img/loading.svg') }}" data-lazy="{{ $cliente->image->present()->render('thumb') }}" alt="{{ $cliente->titulo }}">
                    @endif
                </div>
            </li>
            @endforeach

        </ul>
    </section>
@stop
