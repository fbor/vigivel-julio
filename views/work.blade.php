@extends('master')
@section('title', 'Trabalhe conosco | ' . config('app.name'))
@section('mainView', 'pg-work')
@section('description', '[--meta description--]')

@section('content')
    <section class="wrapper" role="main">
    	@include('partials/heading', ['title'=>'Trabalhe conosco', 'subtitle'=>'Preencha o formulário e candidate-se a oportunidades de emprego!'])

    	<form class="form validate container" itemprop="mainEntityOfPage" action="{{ route('work') }}" method="post" enctype="multipart/form-data">
            
    		
			<fieldset class="row collapse">
				<div class="field col s12 m6 l4">
					<label for="name">Nome completo:</label>
					<input id="name" type="text" name="nome" placeholder="Seu nome" minlength="3" required>
				</div>
                <div class="field col s12 m6 l3">
                    <label for="cpf">CPF:</label>
                    <input id="cpf" type="text" name="cpf" placeholder="Apenas números" minlength="3" required>
                </div>
                <div class="field col s12 m6 l3">
                    <label for="rg">R.G:</label>
                    <input id="rg" type="text" name="rg" placeholder="Apenas números" minlength="3" required>
                </div>
                <div class="field col s12 m6 l2">
                    <label for="sexo">Sexo:</label>
                    <select name="sexo" id="sexo">
                        <option value="">Selecione:</option>
                        <option value="male">Masculino</option>
                        <option value="female">Feminino</option>
                    </select>
                </div>
                <div class="field col s12 m4 l3">
                    <label for="escolaridade">Escolaridade:</label>
                    <select name="escolaridade" id="escolaridade">
                        <option value="">Selecione:</option>
                        <option value="Analfabeto">Analfabeto</option>
                        <option value="Ensino fundamental">Ensino fundamental</option>
                        <option value="Ensino médio">Ensino médio</option>
                        <option value="Superior incompleto">Superior incompleto</option>
                        <option value="Superior em andamento">Superior em andamento</option>
                        <option value="Superior completo">Superior completo</option>
                        <option value="Pós-graduado">Pós-graduado</option>
                        <option value="Mestrado">Mestrado</option>
                        <option value="Doutorado">Doutorado</option>
                    </select>
                </div>
                <div class="field col s12 m4 l3">
                    <label for="cargo">Cargo pretendido:</label>
                    <select name="cargo" id="cargo">
                        <option value="">Selecione:</option>
                        <option value="Opção 1">Auxiliar Financeiro</option>
                        <option value="Opção 2">Auxiliar RH</option>
                        <option value="Opção 3">Controlador de Acesso</option>
                        <option value="Opção 4">Faturista</option>
                        <option value="Opção 6">Vendedor</option>
                        <option value="Opção 7">Gestão Comercial</option>
                        <option value="Opção 8">Jardineiro</option>
                        <option value="Opção 9">Monitor Externo (Tático Móvel)</option>
                        <option value="Opção 10">Monitor Iinterno (Operador de Sistemas)</option>
                        <option value="Opção 11">Supervisor Operacional</option>
                        <option value="Opção 12">Porteiro</option>
                        <option value="Opção 13">Profissional de Limpeza</option>
                        <option value="Opção 14">Recepcionista</option>
                        <option value="Opção 15">Tratorista</option>
                        <option value="Opção 16">Vigia (sem curso vigilante)</option>
                        <option value="Opção 17">Vigilante de Escolta Armada</option>
                        <option value="Opção 18">Vigilante para Eventos</option>
                        <option value="Opção 19">Vigilante Patrimonial</option>
                    </select>
                </div>
                <div class="field col s12 m4 l3">
                    <label for="tel">Telefone:</label>
                    <input id="tel" type="tel" name="telefone" placeholder="(xx) 9999-99999" required>
                </div>
				<div class="field col s12 m4 l3">
					<label for="email">E-mail:</label>
					<input id="email" type="email" name="email" placeholder="seu@email.com" required>
				</div>
                <div class="field col s12 m6 l3">
                    <label for="cep">CEP:</label>
                    <input id="cep" class="cep" type="text" name="cep" placeholder="CEP:" required>
                </div>
                <div class="field col s12 m6 l5">
                    <label for="logradouro">Endereço:</label>
                    <input id="logradouro" type="text" name="endereco" placeholder="Endereço:" required>
                </div>
                <div class="field col s12 m6 l4">
                    <label for="bairro">Bairro:</label>
                    <input id="bairro" type="text" name="bairro" placeholder="Bairro:" required>
                </div>
                <div class="field col s12 m6 l4">
                    <label for="cidade">Cidade:</label>
                    <input id="cidade" type="text" name="cidade" placeholder="Cidade:" required>
                </div>
                <div class="field col s12 m6 l2">
                    <label for="estado">Estado:</label>
                    <input id="estado" type="text" name="estado" placeholder="Estado:" required>
                </div>

                {{ csrf_field() }}

                <div class="field col s12 m6 l6">
                    <label for="curriculo">Currículo:</label>
                    <label class="fake-file" for="file" title="Anexar currículo">
                        <input id="file" class="file" type="file" name="curriculo" value="anexar" required>
                        <span class="fake-input filename" title="">Anexar currículo:</span>
                        <div class="fake-button"><i class="icon icon-checked"></i> <span class="legend">Anexar</span></div>
                    </label>
                </div>


                <div class="field col s12 l4">
                    <div class="g-recaptcha"
                          data-sitekey="6LeqqBoUAAAAAJKTOUN_7MbVjvLapgQVcbXUPec_"
                          data-callback="onSubmit"
                          data-size="visible">
                    </div>
                </div>

                <div class="field col s12 l4">
                    <button type="submit" class="submit block">ENVIAR CURRÍCULO</button>
                </div>

            </fieldset>

            <div class="form-msg alert">
                <p class="message"></p>
                <button class="closed"></button>
            </div>

    	</form>
    </section>
@stop


@section('scripts')
<script src="{{ asset('js/app.js') }}"></script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
@stop
