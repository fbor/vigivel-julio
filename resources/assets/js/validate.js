;(function ($, window, document, undefined){

	var formValidate = $( 'form.validate' ),
		formTarget;


	var app = (function(){

		var initialize = function (){

			if ( ! $.fn.validate ){
		        setTimeout(app.init, 500);
		        return false;
		    };


		    // Apply validate plugin
		    applyValidate( formValidate );


		    // Aplly plugin mask
		    app.masker();


		    $( document )
		    	.on('applyValidate', 'form', createValidate)
		    	.on('change', 'input.cep-api', searchAddress);

		},

		searchAddress = function (event){

			event.preventDefault();

			var el = $( this ),
				target = el.closest( 'form' ),
				cep = String( el.val() ).replace('-', ''),
				reg_cep = /^[0-9]{8}$/;

			if ( cep.length < 1 || !reg_cep.test( cep ) )
				return false;

			el.addClass( 'loading' );

			$.ajax({
				url: 'http://apps.widenet.com.br/busca-cep/api/cep/' + cep + '.json',
				type: 'GET'
			})
			.done(function (response) {
				console.log( response, response.status );

				if ( ! response.status ) return;

				target.find( 'select.uf' ).val( response.state );
				target.find( 'input.city' ).val( response.city );

				var _address = response.address;

				if ( _address.length && response.district.length )
					_address = _address + ', ' + response.district;
				else if ( response.district.length )
					_address = response.district;

				target.find( 'input.address' ).val( _address );

			})
			.always(function() {
				setTimeout(function (){
					el.removeClass( 'loading' );
				}, 1200);
			});


		},

		createValidate = function (event, target){

			var el = target || $(event.target);
			applyValidate.apply(this, [el]);

		};

		applyValidate = function (target){

			$.each(target, function(ind, obj) {

				var objForm = $( obj );

				objForm.validate({
					submitHandler: function(form) {

						formTarget = $( form );
						formTarget
							.trigger( 'submitHandler' )
							.trigger('showAlert', ['info', '<b>Aguarde,</b> conectando!'])
							.find( 'input[type="submit"], button' ).prop('disabled', true);

						return false;

					},
					invalidHandler: function (event, validator){

						$( event.target ).trigger('showAlert', ['info', '<b>Por favor,</b> preencha todos os campos!', 4500]);

					}
				});

				/* Events forms */

				objForm.on('submitSuccess', function (event, data){

					if ( data.status === 'error' ){

						$( event.target ).trigger('showAlert', ['error', data.message || '<b>Ops! tente novamente,</b> Algo inesperado aconteceu!']);

					} else if ( data.status ){

						objForm.find( 'input,select,textarea' ).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected').trigger('change');

						if ( data.redirect ) window.location = data.redirect;

					}

				}).on('submitFail', function (event, data){

					// console.log( data );

				});

			});

		},

		apllyMask = function (){

			try {
				
				var input = $( 'input' );
				input.filter( "[type='tel']" ).mask( '(00) 0 0000-0000' );
				input.filter( "[name='cpf']" ).mask( '999.999.999-99' );
				input.filter( "[name='postal-code']" ).mask( '99999-999' );

			} catch (err){
				console.warn( 'Not defined plugin mask!', err );
			};

		};



		return {

			init: initialize,
			masker: apllyMask

		};

	} ());


	app.init();





}( jQuery, window, document ));