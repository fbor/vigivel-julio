
;(function ($, window, document, undefined){
    // 'use strict';

    // Definitions
    var _window = $( window ),
        _document = $( 'html, body' ),
    	_body = $( '.body' ),
        _header = $( '.main-header' ),
        toggleNavbar = $( 'input#togglemenu' );


    toggleNavbar.prop('checked', false).on("change", function (event){

        _body.toggleClass( 'is-navbar' );

    });


    ;(function (){

        $( '.lazy' ).lazyload({
            effect : 'fadeIn',
            load : function (element, el_left, settings){
                $( this ).removeClass( 'loading load-circle' );
            }
        });

    } ());


    ;(function (){

        var _dashboard = $( '#acesso-restrito' );
        $( '#toggleRestrito' ).on('click', function (event) {
            event.preventDefault();

            var _height = _dashboard.find( '.widget-portal' ).height();

            if ( !_body.hasClass( 'dashboard-open' ) ){

                _dashboard.addClass( 'enter' );
                _document.animate({ scrollTop: 0 }, 410);

                if ( _body.hasClass( 'pg-home' ) ){
                    _header.animate({ 'top':_height }, 410);
                }

            } else {
                _header.animate({ 'top':0 }, 410);
            }


            _dashboard.slideToggle( 410, function() {
                _body.toggleClass( 'dashboard-open' );
                _dashboard.removeClass( 'enter' ).removeAttr( 'style' );
            });

            if ( _body.hasClass( 'is-navbar' ) ){
                toggleNavbar.prop('checked', false).trigger( 'change' );
            };

        });

    } ());



    /*
     * Slider Banner
     */
    ;(function (){

        var el = $( '#banner' );


        if ( !el.length )
            return;


        el.find( '.slick-slider' ).slick({
            infinite: true,
            autoplay: true,
            autoplaySpeed: 5000,
            speed: 400
        });


    } ());


    /*
     * Slider servicos
     */
    ;(function (){

        var el = $( '#slider-service' ),
            opts = {
                arrows: false,
                dots: true,
                lazyLoad: 'ondemand',
                mobileFirst: true,
                responsive: [
                    {
                        breakpoint: 0,
                        settings: "unslick" // destroys slick
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                        }
                    },
                    {
                        breakpoint: 1200,
                        settings: "unslick" // destroys slick
                    },
                ]
            };


        if ( !el.length )
            return;

        function changeSlider (event, slick, currentSlide, nextSlide){
            $( '.lazy.loading' ).trigger('appear');
        }


        el
            .on('afterChange', changeSlider)
            .slick( opts );


    } ());



    /*
     * Slider basic
     */
    ;(function (){

        var el = $( '.basic-slider' );


        if ( !el.length )
            return;


        el.slick();


    } ());




    /*
     * Slider products
     */
    ;(function (){

        var el = $( '#slider-clients' );


        if ( !el.length )
            return;


        el.slick({
            lazyLoad: 'ondemand',
            mobileFirst: true,
            responsive: [
                {
                    breakpoint: 520,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                },
                {
                    breakpoint: 720,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    }
                },
                {
                    breakpoint: 1020,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4,
                    }
                },
            ]
        });


    } ());


    /*
 * Slider products
 */
    ; (function () {

        var el = $('#slider-service-video');


        if (!el.length)
            return;


        el.slick({
            lazyLoad: 'ondemand',
            mobileFirst: true,
            responsive: [
                {
                    breakpoint: 520,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                },
                {
                    breakpoint: 720,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                },
                {
                    breakpoint: 1020,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                },
            ]
        });


    }());















		/*
     * Request All Form
     */
    var formAlert = '.form-msg',
        status      = {
            'success'   : 'success',
            'error'     : 'error',
            'warning'   : 'warning',
            'info'      : 'info'
        };


    var form = (function (){
        var formPost = function (target, serialize){

            $.ajax({
                url     : target.attr( 'action' ) || 'ajax_fakes/form.php',
                type    : target.attr( 'method' ) || 'POST',
                dataType: 'json',
                data    : serialize || target.serialize(),
            })
            .done(function (res, textStatus, jqXHR){
                setTimeout(function(){

                    target
                        .trigger('submitSuccess', [res, textStatus, jqXHR])
                        .trigger('showAlert', [res.status || 'success', res.message, 5500]);

                }, 1200);
            })
            .fail(function (jqXHR, textStatus, errorThrown){
                target.trigger('submitFail', [jqXHR, textStatus, errorThrown]);
                target.trigger('showAlert', ['error', '<b>ERRO</b> ao conectar ao servidor!', 4500]);
            })
            .always(function (){
                setTimeout(function (){
                    target.find( 'input[type="submit"], button' ).prop('disabled', false);
                }, 1300);
            });

        },

        clearAlert = function (target){
            target.removeClass('success error warning info show');
        },

        showAlert = function (target, status, message){

            var obj = target.find( formAlert );

            if ( !obj.length ) return false;
            clearAlert( obj );
            obj.addClass('show ' + status).find( '.message' ).html( message );

        },

        checkFields = function (el){

            var response = true,
                getAllField = el.find('input,textarea,select').filter('[required]:visible');

            for (var i = 0, _lgt = getAllField.length; i < _lgt; i++){
                var _val = getAllField[i].value;
                if ( _val.length < 4 ){
                    response = false;
                    $( getAllField[i] ).addClass( 'error' );
                };
            };

            return response;

        },

        basicValidate = function (event){

            var deferred    = $.Deferred(),
                target      = $(event.target);

            if ( checkFields(target) )
                deferred.resolve();
            else
                deferred.reject();

            return deferred.promise();

        };


        return {

            post: formPost,
            notify: showAlert,
            basicValidate: basicValidate,
            clearAlert: clearAlert

        };


    } ());


    /* All event form */
    $( document ).on('submitHandler', 'form', function (event){

        form.post( $(event.target) );
        return false;

    }).on('showAlert', function (event, state, message, time){

        form.notify($( event.target ), status[state], message);
        if ( time ){
            clearTimeout( timer );
            var timer = setTimeout(function (){
                form.clearAlert( $( event.target ).find(formAlert) );
            }, time);
        };

    }).on('change', 'input[type="file"]', function (event){

        var el = $(this).parent();

        el.removeClass('valid error').find( '.filename' ).text( this.value );

        if ( $( this ).hasClass('valid') ){
            el.addClass( 'valid' );
        } else if ( $(this).hasClass('error') ){
            el.addClass( 'error' );
        }

    });

    /*
     * Close alert
     */
    $( formAlert ).on('click', '.closed', function (event){

        $( formAlert ).slideUp(150, function (){
            form.clearAlert( $(this) );
            $(this).removeAttr('style');
        });
        return false;

    });










} (jQuery, window, document));



