

const el = $( 'input.cep' );
let logradouro, bairro, localidade, uf;


if ( el.length )
	init();

function init (){

	el.on('change', onsearch);

	const form = el.closest('form');

	logradouro = form.find('input[name="logradouro"]');
	bairro = form.find('input[name="bairro"]');
	localidade = form.find('input[name="cidade"]');
	uf = form.find('input[name="estado"]');

}


function onsearch (event){

	el.addClass( 'loading' );

	// init
	getSearchData( $(this).val() )
		.then( renderView, (err) => {
			console.log('error:', err);
		})
		.then( () => {
			el.removeClass( 'loading' );
		});

}

function renderView (response){

	console.log( response );

	logradouro.val( response.logradouro || '' );
	bairro.val( response.bairro || '' );
	localidade.val( response.localidade || '' );
	uf.val( response.uf || '' );

	return;

}

function getSearchData (cep){

	return new Promise((resolve, reject) => {

		const settings = {
			url: `https://viacep.com.br/ws/${cep}/json/`,
			method: 'GET',
			error: reject,
			success: resolve,
		}

		$.ajax( settings );

	});

}
