const boxWrap = '.container-toggle'
const btn = $('.btn-toggle')

btn.on('click', switchBox)

function switchBox (event) {
    console.log(event)
    event.preventDefault()
    const target = $(this).attr('target')
    const current = $(this).closest(boxWrap)

    current.toggleClass('is-active')
    if ($(target).hasClass('hidden')){
        $(target).removeClass('hidden').hide()
    }
    $(target).slideToggle(300)
}
