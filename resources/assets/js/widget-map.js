

;(function($, window, document, undefined) {

    var locations = [],
        base_url = $( 'meta[name="base_url"]' ).attr( 'content' ),
        _start = false,
        origin_input = document.getElementById('origin-input'),
        origin_button = document.getElementById('origin-button'),
        address_origin, address_destination;

    var map_canvas = document.getElementById('map_canvas');
    var objMap = $( map_canvas );
    var map, directionsDisplay, directionsService;
    var image = 'img/marker.png';
    var infowindow;
    var marker, i;
    var markers = new Array();

    var clusterStyles = [
        {
            url: 'img/marker-group.png',
            textColor: '#fff',
            textSize: 14,
            width: 42,
            height: 52,
        }
    ];
    var _optsMarker = {
        'gridSize': 72,
        styles: clusterStyles
    };


    var createMarker = function (){

        var _tpl, _render = '';

        // Instantiate a directions service.
        directionsService = new google.maps.DirectionsService;


        map = new google.maps.Map(map_canvas, {
            zoom: 14,
            scrollwheel: !1,
            center: new google.maps.LatLng(-25.7733423, -53.5354413),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: [{"elementType": "geometry", "stylers": [{"color": "#242f3e"} ] }, {"elementType": "labels.text.fill", "stylers": [{"color": "#746855"} ] }, {"elementType": "labels.text.stroke", "stylers": [{"color": "#242f3e"} ] }, {"featureType": "administrative.locality", "elementType": "labels.text.fill", "stylers": [{"color": "#d59563"} ] }, {"featureType": "poi", "elementType": "labels.text.fill", "stylers": [{"color": "#d59563"} ] }, {"featureType": "poi.business", "stylers": [{"visibility": "off"} ] }, {"featureType": "poi.park", "elementType": "geometry", "stylers": [{"color": "#263c3f"} ] }, {"featureType": "poi.park", "elementType": "labels.text", "stylers": [{"visibility": "off"} ] }, {"featureType": "poi.park", "elementType": "labels.text.fill", "stylers": [{"color": "#6b9a76"} ] }, {"featureType": "road", "elementType": "geometry", "stylers": [{"color": "#38414e"} ] }, {"featureType": "road", "elementType": "geometry.stroke", "stylers": [{"color": "#212a37"} ] }, {"featureType": "road", "elementType": "labels.text.fill", "stylers": [{"color": "#9ca5b3"} ] }, {"featureType": "road.highway", "elementType": "geometry", "stylers": [{"color": "#746855"} ] }, {"featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{"color": "#1f2835"} ] }, {"featureType": "road.highway", "elementType": "labels.text.fill", "stylers": [{"color": "#f3d19c"} ] }, {"featureType": "transit", "elementType": "geometry", "stylers": [{"color": "#2f3948"} ] }, {"featureType": "transit.station", "elementType": "labels.text.fill", "stylers": [{"color": "#d59563"} ] }, {"featureType": "water", "elementType": "geometry", "stylers": [{"color": "#17263c"} ] }, {"featureType": "water", "elementType": "labels.text.fill", "stylers": [{"color": "#515c6d"} ] }, {"featureType": "water", "elementType": "labels.text.stroke", "stylers": [{"color": "#17263c"} ] } ] });

        // Create a renderer for directions and bind it to the map.
        directionsDisplay = new google.maps.DirectionsRenderer({map: map});



        infowindow = new google.maps.InfoWindow();

        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i]['lat'], locations[i]['lon']),
                map: map,
                icon: image,
                animation: google.maps.Animation.DROP,
                zIndex: 999
            });
            markers.push(marker);

            // Address company
            address_destination = String(locations[i]['lat'] + ', '  +locations[i]['lon']);

            _tpl = mapInfoTpl(locations[i]);
            
            google.maps.event.addListener(marker, 'click', (function(marker, i, _tpl) {

                return function() {
                    infowindow.setContent( _tpl );
                    infowindow.open(map, marker);
                    // map.setZoom(12);
                    map.setCenter(marker.getPosition());
                }

            })(marker, i, _tpl));
        }

        if ( window.MarkerClusterer ){
            var markerCluster = new MarkerClusterer(map, markers, _optsMarker);
        }

        if ( markers.length >= 2 ){
            autoCenter();
        } else {
            map.setCenter(marker.getPosition());
            map.setZoom(16);
        }


        
        var destination_autocomplete = new google.maps.places.Autocomplete(origin_input);
        destination_autocomplete.bindTo('bounds', map);

        destination_autocomplete.addListener('place_changed', function() {
            var place = destination_autocomplete.getPlace();

            if (!place.geometry) {
                window.alert("Autocomplete's returned place contains no geometry");
                return;
            }

            address_origin = place.formatted_address;

            calcRoute();
        });




    },

    calcRoute = function () {

        if ( !address_origin )
            return;

        var request = {
            origin: address_origin,
            destination: address_destination,
            travelMode: google.maps.TravelMode.DRIVING
        };

        directionsService.route(request, function(response, status) {

            // Route the directions and pass the response to a function to create
            if (status === google.maps.DirectionsStatus.OK) {

                // markerArray[i].setMap(null);

                console.log( response );

                directionsDisplay.setDirections(response);
            } else {
                window.alert('Directions request failed due to ' + status);
            }

        });

    },

    mapInfoTpl = function (json){

        var _tpl = '',
            _content;

        if ( json.image ){
            _tpl = '<figure class="image"><img src="' + json.image + '"></figure>';
        }

        if ( json.address ){
            _content = '<p>' + json.address + '<br>' + json.phone + '</p>';
        } else if ( json.content ) {
            _content = '<p>' + json.content.join(',<br> ') + '</p>';
        }

        return _tpl + '<h4 class="title">' + json.label + '</h4>' + _content;

    },

    autoCenter = function (){

        //  Create a new viewpoint bound
        var bounds = new google.maps.LatLngBounds();
        //  Go through each...
        $.each(markers, function(index, marker) {
            bounds.extend(marker.position);
        });
        //  Fit these bounds to the map
        map.fitBounds(bounds);

    },

    initialize = function (){

        var _route = (base_url.length) ? base_url + 'api/endereco' : '/api/endereco';

        if ( objMap.data( 'sync' ) )
            _route = objMap.data( 'sync' );

        $.ajax({
            url     : _route,
            type    : 'GET',
            dataType: 'json',
        })
        .done(function (response, status, xhr) {
            
            if ( xhr.status === 200 ){
                locations = response;
                createMarker();
            }

        });

        if ( !navigator.geolocation ){
            $( '.shared-location' ).addClass('hidden');
        }

        $( origin_button ).on('click', originAddress);

    },

    initMap = function (){

        loadScript('https://maps.googleapis.com/maps/api/js?key=AIzaSyBBPUG13eoAA0fDWIr2Y0SHas-I9wHsuLE&libraries=places&language=pt-BR', initialize);

    }, 

    mapIsVisible = function (){
        
        if ( ! objMap.length )
            return;


        var _window = $( window ),
            posY = _window.scrollTop() || 0;

        _window.on('scroll', function(event) {
            
            posY = objMap.offset().top - _window.height();
            if ( !_start && _window.scrollTop() >= posY ){
                _start = true;
                initMap();
            }

        }).trigger('scroll');


    } ();


    function originAddress (){

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
            
            navigator.geolocation.getCurrentPosition(function(position) {
            
                address_origin = String(position.coords.latitude + ', '  + position.coords.longitude);
                // Render route
                calcRoute();

            });
        }

    }

    
    function loadScript(src, callback){
  
        var script = document.createElement("script");
        script.type = "text/javascript";
        if (callback) script.onload = callback;
        document.getElementsByTagName("head")[0].appendChild(script);
        script.src = src;

    }


} (jQuery, window, document));
