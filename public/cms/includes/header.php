<?php

    if( has_permission($_SESSION['user']['nivel_permissao'], ['admin']) )
    {
        // Novos contatos
        $stmt = $pdo->prepare('select count(*) from tbl_contato where visualizado = 0');
        $stmt->execute();

        $novos_contatos_total = $stmt->fetchColumn();
    }

?>

<!-- START HEADER -->
<div id="header">

	<!-- logo -->
	<div class="logo"><a href="<?php echo $_pages['dashboard']; ?>"><img src="img/goutnix.png" width="130" alt="logo"/></a>	</div>
    
    
    <!-- notifications -->
    <?php if( has_permission($_SESSION['user']['nivel_permissao'], ['admin']) ): ?>
        <div id="notifications">
            <a href="<?php echo $_pages['dashboard']; ?>" class="qbutton-left tips" title="Dashboard"><img src="img/icons/header/dashboard.png" width="16" height="15" alt="" /></a>
            <a href="<?php echo $_pages['contatos']; ?>" class="qbutton-right tips" title="Novos contatos"><img src="img/icons/header/message.png" width="19" height="13" alt="" /> <?php if($novos_contatos_total > 0): ?><span class="qballon"><?php echo $novos_contatos_total; ?></span><?php endif; ?> </a>
            <div class="clear"></div>
        </div>
    <?php endif; ?>
    
    
    <!-- profile box -->
    <div id="profilebox">
    	<a href="#" class="display">
        	<img src="<?php echo $_SESSION['user']['thumb']; ?>" width="33" height="33" alt="profile"/><b>Bem-vindo(a)</b><span><?php echo limitar_texto($_SESSION['user']['username'], 11, false); ?></span>
        </a>
        
        <div class="profilemenu">
        	<ul>
            	<li><a href="<?php echo $_pages['profile']; ?>">Perfil</a></li>
            	<li><a href="<?php echo $_pages['logout']; ?>">Sair</a></li>
            </ul>
        </div>
        
    </div>
    
    
    <div class="clear"></div>
</div>
<!-- END HEADER -->