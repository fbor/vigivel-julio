<!-- START SIDEBAR -->
<div id="sidebar">
    
    <!-- start sidemenu -->
    <div id="sidemenu">
        <ul>

            <?php
            
                foreach ($_menu_sidebar as $menu) {
                    if( is_null($menu['permissions']) || has_permission($_SESSION['user']['nivel_permissao'], $menu['permissions']) )
                    {
                        echo '
                            <li ' . ($menu_active == $menu['menu_slug'] ? 'class="active"' : '') . '>
                                <a href="' . $menu['module_link'] . '">
                                    <img src="' . $menu['icon']['src'] . '" width="' . $menu['icon']['width'] . '" height="' . $menu['icon']['height'] . '" alt="icon">' . $menu['label'] . '
                                </a>
                            </li>';
                    }
                }
            
            ?>

        </ul>
    </div>
    <!-- end sidemenu -->
    
</div>
<!-- END SIDEBAR -->