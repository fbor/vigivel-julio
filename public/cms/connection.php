<?php

// Conexão com MySQL
$pdo = new PDO(
	'mysql:host=' . $dbhost . ':41890;dbname=' . $dbname, $dbuser, $dbpass, [
		PDO::ATTR_PERSISTENT 		 => true,
		PDO::ATTR_EMULATE_PREPARES   => false,
		PDO::ATTR_ERRMODE 			 => PDO::ERRMODE_EXCEPTION,
		PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'",
	]
);