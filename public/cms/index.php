<?php

    require 'bootstrap.php';
    require 'connection.php';

    if( ! isset($_SESSION['logged']) || $_SESSION['logged'] !== true ){
        redirect(sprintf('%s/login.php', $_vars['url_base']));
    }

    $menu_active = 'dashboard';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $_vars['title']; ?></title>
	
    <!-- Reset -->
    <link rel="stylesheet" type="text/css" href="style/reset.css" /> 
    <!-- Main Style File -->
    <link rel="stylesheet" type="text/css" href="style/root.css" /> 
    <!-- Grid Styles -->
    <link rel="stylesheet" type="text/css" href="style/grid.css" /> 
    <!-- Typography Elements -->
    <link rel="stylesheet" type="text/css" href="style/typography.css" /> 
    <!-- Jquery UI -->
    <link rel="stylesheet" type="text/css" href="style/jquery-ui.css" />
    <!-- Jquery Plugin Css Files Base -->
    <link rel="stylesheet" type="text/css" href="style/jquery-plugin-base.css" />
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
    
    <!--[if IE 7]>	  <link rel="stylesheet" type="text/css" href="style/ie7-style.css" />	<![endif]-->
    
    <!-- jquery base -->
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.11.custom.min.js"></script>
    <!-- jquery plugins settings -->
	<script type="text/javascript" src="js/jquery-settings.js"></script>
    <!-- toggle -->
	<script type="text/javascript" src="js/toogle.js"></script>
    <!-- tipsy -->
	<script type="text/javascript" src="js/jquery.tipsy.js"></script>
    <!-- uniform -->
	<script type="text/javascript" src="js/jquery.uniform.min.js"></script>
    <!-- Jwysiwyg editor -->
	<script type="text/javascript" src="js/jquery.wysiwyg.js"></script>
    <!-- table shorter -->
	<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
    <!-- raphael base and raphael charts -->
	<script type="text/javascript" src="js/raphael.js"></script>
	<script type="text/javascript" src="js/analytics.js"></script>
	<script type="text/javascript" src="js/popup.js"></script>
    <!-- fullcalendar -->
	<script type="text/javascript" src="js/fullcalendar.min.js"></script>
    <!-- prettyPhoto -->
	<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
    <!-- Jquery.UI Base -->
	<script type="text/javascript" src="js/jquery.ui.core.js"></script>
	<script type="text/javascript" src="js/jquery.ui.mouse.js"></script>
	<script type="text/javascript" src="js/jquery.ui.widget.js"></script>
    <!-- Slider -->
	<script type="text/javascript" src="js/jquery.ui.slider.js"></script>
    <!-- Date Picker -->
	<script type="text/javascript" src="js/jquery.ui.datepicker.js"></script>
    <!-- Tabs -->
	<script type="text/javascript" src="js/jquery.ui.tabs.js"></script>
    <!-- Accordion -->
	<script type="text/javascript" src="js/jquery.ui.accordion.js"></script>
    <!-- Google Js Api / Chart and others -->
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <!-- Date Tables -->
	<script type="text/javascript" src="js/jquery.dataTables.js"></script>
	
        
    
</head>
<body>
<div class="wrapper">

	<?php include 'includes/header.php'; ?>
    
    <!-- START MAIN -->
    <div id="main">

        <?php include 'includes/sidebar.php'; ?>
                
        <!-- START PAGE -->
        <div id="page">
            	
            <!-- start page title -->
            <div class="page-title">
            	<div class="in">
            		<div class="titlebar">	<h2>DASHBOARD</h2>	<p>Atalhos e dados gerais</p></div>
                    
                    <div class="shortcuts-icons">
                    	<a class="shortcut tips" href="<?php echo $_vars['pagina_atual']; ?>" title="Atualizar página"><img src="img/icons/shortcut/refresh.png" width="25" height="25" alt="" /></a>
                    </div>
                    
                    <div class="clear"></div>
                </div>
            </div>
            <!-- end page title -->
                
            <!-- START CONTENT -->
            <div class="content">

                <!-- alerta -->
                <?php

                    if(isset($novos_contatos_total) && $novos_contatos_total > 0):
                        $mensagem = ($novos_contatos_total == 1) 
                            ? 'existe <b>%d</b> novo contato vindo do site. <a href="%s">visualize-o</a>'
                            : 'existem <b>%d</b> novos contatos vindos do site. <a href="%s">visualize-os</a>';

                ?>
                    <div class="albox dialogbox">
                        <div class="icon"><img src="img/icons/16x16/note.png" width="16" height="16" alt="icon"/></div>
                        <b>Olá</b>, <?php echo sprintf($mensagem, $novos_contatos_total, $_pages['contatos']); ?>
                    </div>
                    <div class="clear"></div>
                <?php endif; ?>

                <!-- atalhos -->
                <div class="grid740">

                    <?php if( has_permission($_SESSION['user']['nivel_permissao'], ['admin']) ): ?>
                        <a href="<?php echo $_pages['clientes']; ?>" class="dashbutton"> 
                            <img src="img/icons/dashbutton/group.png" width="55" height="32" alt="icon" />  
                            <b>Clientes</b>
                        </a>
                    <?php endif; ?>

                    <?php if( has_permission($_SESSION['user']['nivel_permissao'], ['admin']) ): ?>
                        <a href="<?php echo $_pages['servicos']; ?>" class="dashbutton"> 
                            <img src="img/icons/dashbutton/shopping-cart.png" width="32" height="32" alt="icon" />  
                            <b>Serviços</b>
                        </a>
                    <?php endif; ?>

                    <?php if( has_permission($_SESSION['user']['nivel_permissao'], ['admin']) ): ?>
                        <a href="<?php echo $_pages['orcamentos']; ?>" class="dashbutton"> 
                            <img src="img/icons/dashbutton/mail.png" width="49" height="32" alt="icon" />  
                            <b>Solicitações</b>de orçamento
                        </a>
                    <?php endif; ?>

                    <?php if( has_permission($_SESSION['user']['nivel_permissao'], ['admin']) ): ?>
                        <a href="<?php echo $_pages['franquias']; ?>" class="dashbutton"> 
                            <img src="img/icons/dashbutton/mail.png" width="49" height="32" alt="icon" />  
                            <b>Solicitações</b>de franquia
                        </a>
                    <?php endif; ?>
                
                    <?php if( has_permission($_SESSION['user']['nivel_permissao'], ['admin']) ): ?>
                        <a href="<?php echo $_pages['contatos']; ?>" class="dashbutton"> 
                            <img src="img/icons/dashbutton/mail.png" width="49" height="32" alt="icon" />  
                            <b>Contatos</b> Enviados pelo site
                        </a>
                    <?php endif; ?>

                    <?php if( has_permission($_SESSION['user']['nivel_permissao'], ['admin']) ): ?>
                        <a href="<?php echo $_pages['usuarios']; ?>" class="dashbutton"> 
                            <img src="img/icons/dashbutton/users.png" width="41" height="32" alt="icon" />  
                            <b>Usuários</b> Usuários do sistema
                        </a>
                    <?php endif; ?>

                    <a href="http://www.goutnix.com.br" class="dashbutton" target="_blank"> 
                        <img src="img/icons/dashbutton/bubbles.png" width="44" height="32" alt="icon" />    
                        <b>Suporte</b> Fale com a Goutnix
                    </a>
                    
                </div>

                <div class="clear"></div>

            </div>
            <!-- END CONTENT -->
            
        </div>
        <!-- END PAGE -->

        <div class="clear"></div>

    </div>
    <!-- END MAIN -->
   
	<?php include 'includes/footer.php'; ?>

</div>
</body>
</html>