/* Brazilian initialisation for the jQuery UI date picker plugin. */
/* Written by Leonildo Costa Silva (leocsilva@gmail.com). */
jQuery(function($){
	$.timepicker.regional['pt-BR'] = {
		currentText: 'Agora',
		closeText: 'Fechar',
		ampm: false,
		amNames: ['AM', 'A'],
		pmNames: ['PM', 'P'],
		timeFormat: 'hh:mm tt',
		timeSuffix: '',
		timeOnlyTitle: 'Escolha o tempo',
		timeText: 'Tempo',
		hourText: 'Hora',
		minuteText: 'Minuto',
		secondText: 'Segundo',
		millisecText: 'Milissegundo',
		timezoneText: 'Timezone'
	};

	$.timepicker.setDefaults($.timepicker.regional['pt-BR']);
});