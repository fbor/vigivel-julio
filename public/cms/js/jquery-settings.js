$(function(){ 
	/* Tipsy */
	if( $('.tips').length ){
		$('.tips').tipsy({gravity: 's',html: true}); 
	}

	if( $('.tips-right').length ){
		$('.tips-right').tipsy({gravity: 'w',html: true});
	}

	if( $('.tips-left').length ){
		$('.tips-left').tipsy({gravity: 'e',html: true});
	}

	if( $('.tips-bottom').length ){
		$('.tips-bottom').tipsy({gravity: 'n',html: true});
	}

	/* Form Style */
	if( $('.st-forminput').length ){
		$('.st-forminput')
			.focus(function(){
				$(this).removeClass('st-forminput').addClass('st-forminput-active');
			})
			.blur(function(){
				$(this).removeClass('st-forminput-active').addClass('st-forminput');
			});
	}
});