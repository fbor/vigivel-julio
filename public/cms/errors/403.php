<?php

    require __DIR__ . '/../bootstrap.php';

    $error_number  = 403;
    $error_type    = 'Forbidden';
    $error_message = 'Desculpe, mas você não tem permissão de acesso para este recurso.';

?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $error_number; ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $_vars['url_base']; ?>/errors/css/errors.css">

        <!-- JQUERY LIBRARY LINKS -->
        <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    </head>

    <body>
        <div class="container">
            <div class="col-md-6 col-sm-6 imgSec">
                <div class="icon">
                    <div class="victor"></div>
                    <div class="animation"></div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 content">
                <h2 class="heading"><?php echo $error_number; ?></h2>
                <p><?php echo $error_type; ?></p>
                <p><small><?php echo $error_message; ?></small></p>
                <a href="javascript:history.back();" class="button"> Voltar</a>
            </div>
        </div>
    </body>
</html> 