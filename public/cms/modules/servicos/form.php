<?php

    require __DIR__ . '/config.php';

    check_logged();
    check_permissions($_SESSION['user']['nivel_permissao'], $_module['permissoes']['geral']);

    $menu_active = $_module['menu_slug'];

    /**
     * Actions
     */
    $actions = array('new', 'edit');
    $action  = isset($_REQUEST['action']) && in_array($_REQUEST['action'], $actions) ? $_REQUEST['action'] : 'new';

    /**
     * Edit
     */
    $id = isset($_REQUEST['id']) && is_numeric($_REQUEST['id']) ? $_REQUEST['id'] : null;

    if( $action == 'edit' && is_null($id) ){
        redirect($_module['url_base']);
    }

    $stmt = $pdo->prepare('select id, categoria from tbl_categorias where ativo = 1');
    $stmt->execute();

    $categories = $stmt->fetchAll(PDO::FETCH_ASSOC);


    if( ! is_null($id) ){
        $stmt = $pdo->prepare('select * from tbl_servicos where id = :id');
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $data = $stmt->fetch(PDO::FETCH_ASSOC);

        if( ! $data )
        {
            redirect($_module['url_base']);
        }

        $_edit_label  = $data['label'];
        $_edit_id_categoria = unserialize($data['categorias']);
        $_edit_slug   = $data['slug'];
        $_edit_resumo = $data['resumo'];
        $_edit_youtube = $data['youtube'];
        $_edit_texto  = $data['texto'];
        $_edit_ativo  = $data['ativo'];

        // Ícone
        $stmt_icon = $pdo->prepare('select id, title, filename from tbl_files where type = "icon" and module = :module and relationship = :id');
        $stmt_icon->bindValue(':id', $id, PDO::PARAM_INT);
        $stmt_icon->bindValue(':module', $_module['module_slug'], PDO::PARAM_STR);
        $stmt_icon->execute();

        $_edit_icon = $stmt_icon->fetch(PDO::FETCH_ASSOC);

        // Foto fixa
        $stmt_foto_fixa = $pdo->prepare('select id, title, filename from tbl_files where type = "photo_fixed" and module = :module and relationship = :id');
        $stmt_foto_fixa->bindValue(':id', $id, PDO::PARAM_INT);
        $stmt_foto_fixa->bindValue(':module', $_module['module_slug'], PDO::PARAM_STR);
        $stmt_foto_fixa->execute();

        $_edit_photo_fixed = $stmt_foto_fixa->fetch(PDO::FETCH_ASSOC);
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <base href="<?php echo $_vars['url_base']; ?>/" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo $_vars['title']; ?></title>
	
    <link rel="stylesheet" type="text/css" href="style/reset.css" /> 
    <link rel="stylesheet" type="text/css" href="style/root.css" /> 
    <link rel="stylesheet" type="text/css" href="style/grid.css" /> 
    <link rel="stylesheet" type="text/css" href="style/typography.css" /> 
    <link rel="stylesheet" type="text/css" href="style/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="style/jquery-plugin-base.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $_module['url_base'] ?>/css/multiple-select.css" />
    <link rel="stylesheet" type="text/css" href="plugins/sweetalert/dist/sweetalert.css" />
    <link rel="stylesheet" type="text/css" href="plugins/jQuery.filer/css/jquery.filer.css" />
    <link rel="stylesheet" type="text/css" href="plugins/jQuery.filer/css/themes/jquery.filer-dragdropbox-theme.css" />
    
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.tipsy.js"></script>
	<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/jquery.maskMoney.js"></script>
    <script type="text/javascript" src="plugins/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="plugins/jQuery.filer/js/jquery.filer.min.js?v=1.0.5"></script>
    <script type="text/javascript" src="plugins/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="js/jquery-settings.js"></script>
    <script type="text/javascript" src="<?php echo $_module['url_base'] ?>/js/module.js"></script>
    
    
</head>
<body>
<div class="wrapper">

	<?php include sprintf('%s/includes/header.php', $_vars['path_admin']); ?>
    
    <!-- START MAIN -->
    <div id="main">

        <?php include sprintf('%s/includes/sidebar.php', $_vars['path_admin']); ?>
                
        <!-- START PAGE -->
        <div id="page">
            	
            <?php include __DIR__ . '/includes/header.php'; ?>
                
            <!-- START CONTENT -->
            <div class="content">

                <!-- START SIMPLE FORM -->
                <div class="simplebox grid740">
                    <div class="titleh">
                        <h3>Cadastro/Edição</h3>
                        <div class="shortcuts-icons description-text">
                            Campos com <span class="required-red">*</span> são obrigatórios
                        </div>
                    </div>
                    <div class="body">
                    
                      <form id="form" name="form" method="post" action="<?php echo $_module['url_base'] ?>/action.php" enctype="multipart/form-data">

                        <div class="st-form-line">  
                            <span class="st-labeltext"><span class="required-red">*</span> Nome do serviço</span> 
                            <input name="label" id="label" type="text" class="st-forminput" style="width:510px" value="<?php echo isset($_edit_label) ? htmlentities($_edit_label, ENT_QUOTES, 'utf-8') : ''; ?>" /> 
                            <div class="clear"></div>
                        </div>

                        <div class="st-form-line">  
                            <span class="st-labeltext"><span class="required-red">*</span> Categorias</span>
                            <select name="categoria[]" class="uniform select" id="selectCategories" style="width:510px" multiple="multiple" >
                                <?php
                                
                                for ($i=0; $i < count($categories); $i++)
                                { 
                                    for ($j=0; $j < count($_edit_id_categoria); $j++) { 
                                        if (isset($_edit_id_categoria) && ($_edit_id_categoria[$j] == $categories[$i]['id'])) 
                                        {
                                            
                                            printf('<option selected="selected" value="%s" >%s</option>',$categories[$i]['id'],$categories[$i]['categoria']);
                                         break;   
                                        }
                                    }
                                    if ($_edit_id_categoria[$j] != $categories[$i]['id']) {
                                         printf('<option value="%s" >%s</option>', $categories[$i]['id'], $categories[$i]['categoria']);
                                    }
                                }
                                ?>
                            </select>
                            <div class="clear"></div> 
                        </div>

                        <div class="st-form-line">  
                            <span class="st-labeltext"><span class="required-red">*</span> Resumo</span> 
                            <input name="resumo" id="resumo" type="text" class="st-forminput" style="width:510px" value="<?php echo isset($_edit_resumo) ? $_edit_resumo : ''; ?>" /> 
                            <div class="clear"></div>
                        </div>

                        <div class="st-form-line">  
                            <span class="st-labeltext">ID Youtube</span> 
                            <input name="youtube" id="youtube" type="text" class="st-forminput" style="width:510px" value="<?php echo isset($_edit_youtube) ? $_edit_youtube : ''; ?>" /> 
                            <div class="clear"></div>
                        </div>

                        <?php /* ?><div class="st-form-line">  
                            <span class="st-labeltext"><span class="required-red">*</span> Slug <em>(URL)</em></span> 
                            <input name="slug" id="slug" type="text" class="st-forminput" style="width:510px" value="<?php echo isset($_edit_slug) ? $_edit_slug : ''; ?>" /> 
                            <div class="clear"></div>
                        </div><?php */ ?>

                        <div class="st-form-line">  
                            <div class="titleh">
                                <h3>Descrição completa</h3>
                            </div>
                            <div class="body">
                                <textarea name="texto" class="ckeditor" style="width:98%; height: 100px"><?php echo isset($_edit_texto) ? htmlentities($_edit_texto, ENT_QUOTES, 'utf-8') : ''; ?></textarea>
                            </div>
                        </div>

                        <div class="st-form-line">  
                            <span class="st-labeltext">Ícone</span>

                            <?php 

                                if( isset($_edit_icon) && $_edit_icon ):

                                    $thumb = sprintf('%s/%d/thumb_%s', $_module['url_files'], $id, $_edit_icon['filename']);

                            ?>

                            <div class="get-photo">
                                <div class="buttons">   
                                    <a href="<?php echo $_module['url_base'] ?>/action.php?action=destroy-image&id=<?php echo $_edit_icon['id']; ?>" class="mini-delete">Delete</a>
                                    <div class="clear"></div>
                                </div>
                                <img src="<?php echo $thumb; ?>" width="100%" alt="" />
                            </div>

                            <?php else: ?>

                            <span class="ml0 st-form-error">(Formato aceito: png)</span>
                            <input type="file" name="icon" id="icon">
                            <div class="clear"></div>

                            <?php endif; ?>

                            <div class="clear"></div>
                        </div>

                        <div class="st-form-line">  
                            <span class="st-labeltext">Imagem</span>

                            <?php 

                                if( isset($_edit_photo_fixed) && $_edit_photo_fixed ):

                                    $thumb = sprintf('%s/%d/thumb_%s', $_module['url_files'], $id, $_edit_photo_fixed['filename']);
                                    $large = sprintf('%s/%d/large_%s', $_module['url_files'], $id, $_edit_photo_fixed['filename']);

                            ?>

                            <div class="get-photo">
                                <div class="buttons">   
                                    <a href="<?php echo $_module['url_base'] ?>/action.php?action=destroy-image&id=<?php echo $_edit_photo_fixed['id']; ?>" class="mini-delete">Delete</a>
                                    <div class="clear"></div>
                                </div>
                                <a href="<?php echo $large; ?>" rel="prettyPhoto"><img src="<?php echo $thumb; ?>" width="100%" alt="" /></a>
                            </div>

                            <?php else: ?>

                            <span class="ml0 st-form-error">(Formato aceito: jpg/jpge, png, gif)</span>
                            <input type="file" name="imagem_fixa" id="imagem_fixa">
                            <div class="clear"></div>

                            <?php endif; ?>

                            <div class="clear"></div>
                        </div>           
                        
                        <div class="st-form-line">  
                            <span class="st-labeltext"><span class="required-red">*</span> Ativo</span>    
                            <label class="margin-right10"><input type="radio" name="ativo" class="uniform" value="1" <?php echo ($action == 'new' || (isset($_edit_ativo) && $_edit_ativo == 1)) ? 'checked="checked"' : ''; ?> /> Sim</label> 
                            <label class="margin-right10"><input type="radio" name="ativo" class="uniform" value="0" <?php echo (isset($_edit_ativo) && $_edit_ativo == 0) ? 'checked="checked"' : ''; ?> /> Não</label>
                            <div class="clear"></div> 
                        </div>

                        <div class="simple-tips" id="errors-box" style="display:none;">
                            <h2><img src="img/icons/error/error.png" width="13" alt="" /> Atenção!</h2>
                            <ul></ul>
                        </div>

                        <div class="simple-tips loading">
                            <img src="img/loading/5.gif" alt="icon">
                        </div>
                        
                        <div class="button-box">
                            <input type="submit" name="button" value="Salvar" class="st-button"/>
                            <a class="button-gray" href="<?php echo $_module['url_base']; ?>">Cancelar</a>
                        </div>

                        <input type="hidden" name="action" value="<?php echo $action; ?>" />

                        <?php if($action == 'edit'): ?>
                            <input type="hidden" name="id" value="<?php echo $id; ?>" />
                        <?php endif; ?>
                        
                      </form>
                      
                    </div>
                </div>
                <!-- END SIMPLE FORM -->
                
                <div class="clear"></div>

            </div>
            <!-- END CONTENT -->
            
        </div>
        <!-- END PAGE -->

        <div class="clear"></div>

    </div>
    <!-- END MAIN -->
    <script type="text/javascript" src="<?php echo $_module['url_base'] ?>/js/multiple-select.js"></script>

    <script type="text/javascript">
        $('#selectCategories').multipleSelect({
            selectAll: false,
            placeholder: "Selecione uma ou mais categoria(s)",
            allSelected: 'Todas categorias foram selecionadas'
        });
    </script>
   
	<?php include sprintf('%s/includes/footer.php', $_vars['path_admin']); ?>

</div>
</body>
</html>