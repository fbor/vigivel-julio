<?php

	require_once __DIR__ . '/config.php';
	require_once $_vars['path_admin'] . '/plugins/jQuery.filer/php/class.uploader.php';

	$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : null;

	switch ($action) {
		case 'new':
		case 'edit':

			usleep(600000);

			/**
			 * Values
			 */
			$id 		= isset($_REQUEST['id']) && is_numeric($_REQUEST['id']) ? $_REQUEST['id'] : null;
			$label 		= isset($_REQUEST['label']) ? trim($_REQUEST['label']) : null;
			$categorias 		= isset($_REQUEST['categoria']) ? ($_REQUEST['categoria']) : null;

			// $slug 		= isset($_REQUEST['slug']) ? trim($_REQUEST['slug']) : null;
			$resumo 	= isset($_REQUEST['resumo']) ? trim($_REQUEST['resumo']) : null;
			$youtube 	= isset($_REQUEST['youtube']) ? trim($_REQUEST['youtube']) : null;
			$texto 		= isset($_REQUEST['texto']) ? trim($_REQUEST['texto']) : null;
			// $ordem 		= isset($_REQUEST['ordem']) && is_numeric($_REQUEST['ordem']) ? (int)$_REQUEST['ordem'] : 0;
			$ativo 		= isset($_REQUEST['ativo']) ? (int)$_REQUEST['ativo'] : 0;

			/**
			 * Save data
			 */
			$sql = ! is_null($id)
				? 'update tbl_servicos set label = :label, categorias = :categorias, resumo = :resumo, youtube = :youtube, texto = :texto, ativo = :ativo where id = :id'
				: 'insert into tbl_servicos (label, categorias, resumo, youtube, texto, ativo, created_at) values(:label, :categorias, :resumo, :youtube, :texto, :ativo, :created_at)';

			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(':label', $label, PDO::PARAM_STR);
			$stmt->bindValue(':categorias', serialize($categorias), PDO::PARAM_STR);
			$stmt->bindValue(':resumo', $resumo, PDO::PARAM_STR);
			$stmt->bindValue(':youtube', $youtube, PDO::PARAM_STR);
			$stmt->bindValue(':texto', $texto, PDO::PARAM_STR);
			$stmt->bindValue(':ativo', $ativo, PDO::PARAM_INT);

			if( $id ){
				$stmt->bindValue(':id', $id, PDO::PARAM_INT);
			} else {
				$stmt->bindValue(':created_at', date('Y-m-d H:i:s'), PDO::PARAM_STR);
			}
            
            $stmt->execute();

            if( ! $id ){
				$id = $pdo->lastInsertId();
			}


			/**
			 * Verifica e cria diretório
			 */
			$path = sprintf('%s/%d', $_module['path_files'], $id);
			
			if( ! is_dir($path) )
			{
				mkdir($path, 0755, true);
			}

			/**
			 * Salva o ícone
			 */
			if(isset($_FILES['icon']) && $_FILES['icon']['error'] == 0 && $_FILES['icon']['size'] > 0)
			{
				$icon = $_FILES['icon'];	

				$name     = pathinfo($icon['name'], PATHINFO_FILENAME);
				$filename = get_slug_filename($icon['name']);
				$thumb    = sprintf('thumb_%s', $filename);

				move_uploaded_file($icon['tmp_name'], sprintf('%s/%s', $path, $thumb));

				$stmt = $pdo->prepare('insert into tbl_files (filename, title, type, module, relationship, created_at) values (:filename, :title, :type, :module, :relationship, :created_at)');
				$stmt->bindValue(':filename', $filename, PDO::PARAM_STR);
				$stmt->bindValue(':title', $name, PDO::PARAM_STR);
				$stmt->bindValue(':type', 'icon', PDO::PARAM_STR);
				$stmt->bindValue(':module', $_module['module_slug'], PDO::PARAM_STR);
				$stmt->bindValue(':relationship', $id, PDO::PARAM_INT);
				$stmt->bindValue(':created_at', date('Y-m-d H:i:s'), PDO::PARAM_STR);
				$stmt->execute();
			}

			/**
			 * Salva a imagem fixa
			 */
			if(isset($_FILES['imagem_fixa']) && $_FILES['imagem_fixa']['error'] == 0 && $_FILES['imagem_fixa']['size'] > 0)
			{
				$imagem_fixa = $_FILES['imagem_fixa'];	

				$name     = pathinfo($imagem_fixa['name'], PATHINFO_FILENAME);
				$filename = get_slug_filename($imagem_fixa['name']);
				$thumb    = sprintf('thumb_%s', $filename);
				$large    = sprintf('large_%s', $filename);

				$img = Canvas\Canvas::Instance();
				$img->carrega($imagem_fixa['tmp_name'])
					->redimensiona( 200, '100%', 'proporcional' )
					->grava(sprintf('%s/%s', $path, $thumb), 70);

				$img = Canvas\Canvas::Instance();
				$img->carrega($imagem_fixa['tmp_name'])
					->redimensiona( 724, '100%', 'proporcional' )
					->grava(sprintf('%s/%s', $path, $large), 70);

				$stmt = $pdo->prepare('insert into tbl_files (filename, title, type, module, relationship, created_at) values (:filename, :title, :type, :module, :relationship, :created_at)');
				$stmt->bindValue(':filename', $filename, PDO::PARAM_STR);
				$stmt->bindValue(':title', $name, PDO::PARAM_STR);
				$stmt->bindValue(':type', 'photo_fixed', PDO::PARAM_STR);
				$stmt->bindValue(':module', $_module['module_slug'], PDO::PARAM_STR);
				$stmt->bindValue(':relationship', $id, PDO::PARAM_INT);
				$stmt->bindValue(':created_at', date('Y-m-d H:i:s'), PDO::PARAM_STR);
				$stmt->execute();
			}

			set_flash_message('message', ($action == 'new') ? 'Novo registro adicionado.' : 'Registro alterado.');
            redirect($_module['url_base']);

		break;

		case 'destroy':

			/**
			 * Values
			 */
			$id = isset($_REQUEST['id']) && is_numeric($_REQUEST['id']) ? $_REQUEST['id'] : null;

			/**
			 * Destroy register
			 */
			$stmt = $pdo->prepare('delete from tbl_servicos where id = :id');
			$stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();

            set_flash_message('message', 'Registro removido.');
            redirect($_module['url_base']);

		break;

		case 'destroy-image':

			/**
			 * Values
			 */
			$id = isset($_REQUEST['id']) && is_numeric($_REQUEST['id']) ? $_REQUEST['id'] : null;

			/**
			 * Destroy images
			 */
			$stmt = $pdo->prepare('select relationship, filename from tbl_files where id = :id');
			$stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();

            $fetch = $stmt->fetch(PDO::FETCH_ASSOC);

            if($fetch)
            {
				$path  		= sprintf('%s/%d', $_module['path_files'], $fetch['relationship']);
				$thumb 		= sprintf('%s/thumb_%s', $path, $fetch['filename']);
				$large 		= sprintf('%s/large_%s', $path, $fetch['filename']);

				if( is_file($thumb) ){
					unlink($thumb);
				}

				if( is_file($large) ){
					unlink($large);
				}

				/**
				 * Destroy register
				 */
				$stmt = $pdo->prepare('delete from tbl_files where id = :id');
				$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	            $stmt->execute();
            }

            redirect($_SERVER['HTTP_REFERER']);

		break;

		case 'reorder':

			/**
			 * Values
			 */
			$reorder = isset($_REQUEST['reorder']) && is_array($_REQUEST['reorder']) ? $_REQUEST['reorder'] : [];

			/**
			 * Reorder
			 */
			$stmt = $pdo->prepare('update tbl_servicos set ordem = :ordem where id = :id');
			
			foreach ($reorder as $key => $value) 
			{
				$stmt->bindValue(':ordem', $key+1, PDO::PARAM_INT);
				$stmt->bindValue(':id', $value, PDO::PARAM_INT);
            	$stmt->execute();
			}

            set_flash_message('message', 'Registros reordenados.');
            redirect($_module['url_base']);

		break;
		
		default:

			exit;

		break;
	}