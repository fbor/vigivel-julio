<?php

	require_once __DIR__ . '/../../bootstrap.php';

	$module_name = 'servicos';

	$_module = array(
		'menu_slug'     => $module_name,
		'module_slug'   => $module_name,
		'url_base' 		=> sprintf('%s/modules/%s', $_vars['url_base'], $module_name),
		'path_files'	=> sprintf('%s/%s', $_vars['path_files'], $module_name),
		'url_files'		=> sprintf('%s/%s', $_vars['url_files'], $module_name),
		'max_images'    => 1,
		'permissoes'    => [
			'geral' => [
				'admin',
			],
		],
		'title'    => 'SERVIÇOS',
		'subtitle' => 'Gerenciamento de serviços',
	);