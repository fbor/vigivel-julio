<?php

    require __DIR__ . '/config.php';

    check_logged();
    check_permissions($_SESSION['user']['nivel_permissao'], $_module['permissoes']['geral']);

    $menu_active = $_module['menu_slug'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <base href="<?php echo $_vars['url_base']; ?>/" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo $_vars['title']; ?></title>
	
    <link rel="stylesheet" type="text/css" href="style/reset.css" /> 
    <link rel="stylesheet" type="text/css" href="style/root.css" /> 
    <link rel="stylesheet" type="text/css" href="style/grid.css" /> 
    <link rel="stylesheet" type="text/css" href="style/typography.css" /> 
    <link rel="stylesheet" type="text/css" href="style/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="style/jquery-plugin-base.css" />
    <link rel="stylesheet" type="text/css" href="plugins/sweetalert/dist/sweetalert.css" />
    <link rel="stylesheet" type="text/css" href="plugins/dragula/dist/dragula.min.css" />

    <style type="text/css">
        .btn-titleh { padding: 5px 10px; cursor: pointer; }
        #box-order { display: none; }
        #box-order table tr td.first-td { padding: 0 !important; }
        #box-order table tr td.first-td img { margin: 10px; }
    </style>

	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.tipsy.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="plugins/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="plugins/dragula/dist/dragula.min.js"></script>
    <script type="text/javascript" src="js/jquery-settings.js"></script>
    <script type="text/javascript" src="<?php echo $_module['url_base'] ?>/js/module.js"></script>

</head>
<body>
<div class="wrapper">

	<?php include sprintf('%s/includes/header.php', $_vars['path_admin']); ?>
    
    <!-- START MAIN -->
    <div id="main">

        <?php include sprintf('%s/includes/sidebar.php', $_vars['path_admin']); ?>
                
        <!-- START PAGE -->
        <div id="page">
            	
            <?php include __DIR__ . '/includes/header.php'; ?>
                
            <!-- START CONTENT -->
            <div class="content">

                <!-- START TABLE -->
                <div class="simplebox grid740">

                    <?php 
                        $flash_message = get_flash_message('message');
                        if( $flash_message ):
                    ?>
                        <div class="albox succesbox">
                            <b>Sucesso!</b> <?php echo $flash_message; ?>
                        </div>
                    <?php endif; ?>

                    <div id="box-list-rows">
                        <div class="titleh">
                            <h3>Lista de registros</h3>
                            <div class="shortcuts-icons">
                                <a class="shortcut tips" href="<?php echo $_module['url_base']; ?>/form.php" title="Adicionar novo"><img src="img/icons/shortcut/plus.png" width="25" height="25" alt="icon" /></a>
                                <button type="button" class="button-green btn-titleh" id="btn-reordenar" href="#">REORDENAR</button>
                            </div>
                        </div>

                        <table cellpadding="0" cellspacing="0" border="0" class="display data-table" id="list-rows">
                        
                            <thead>
                                <tr>
                                    <th width="1">ID</th>
                                    <th>Serviço</th>
                                    <th width="1">Ordem</th>
                                    <th width="1">Ativo</th>
                                    <th width="1">Ações</th>
                                </tr>
                            </thead>
                            
                            <tbody>

                                <?php 

                                    $stmt = $pdo->prepare('select id, label, ordem, ativo from tbl_servicos order by ordem');
                                    $stmt->execute();

                                    foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $data):

                                ?>
                            
                                    <tr class="gradeA">
                                        <td class="center"><?php echo $data['id']; ?></td>
                                        <td><?php echo $data['label']; ?></td>
                                        <td align="center"><?php echo $data['ordem']; ?></td>
                                        <td align="center">
                                            <?php echo (bool)$data['ativo'] ? '<strong class="hg-green">Sim</strong>' : '<strong class="hg-red">Não</strong>'; ?>
                                        </td>
                                        <td class="center"> 
                                            <a href="<?php echo $_module['url_base']; ?>/form.php?action=edit&id=<?php echo $data['id']; ?>" class="tips" title="Editar"><img src="img/icons/sidemenu/file_edit.png"></a>
                                            <a href="<?php echo $_module['url_base']; ?>/action.php?action=destroy&id=<?php echo $data['id']; ?>" class="delete-row tips" title="Excluir"><img src="img/icons/sidemenu/trash.png"></a>
                                        </td>
                                    </tr>

                                <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                    <!-- end #box-list-rows -->

                    <div id="box-order">
                        <form action="<?php echo $_module['url_base'] ?>/action.php" method="post">
                            <input type="hidden" name="action" value="reorder" />

                            <div class="titleh">
                                <h3>Lista de registros</h3>
                                <div class="shortcuts-icons">
                                    <button type="submit" class="button-green btn-titleh" href="#">SALVAR ORDENAÇÃO</button>
                                    <a href="<?php echo $_module['url_base']; ?>" class="button-gray btn-titleh">VOLTAR</a>
                                </div>
                            </div>
                            <table cellpadding="0" cellspacing="0" border="0" class="display data-table tablesorter">
                            
                                <thead>
                                    <tr>
                                        <th width="1"><img src="<?php echo $_vars['url_base']; ?>/img/drag.png" alt="" width="35"></th>
                                        <th width="1">ID</th>
                                        <th>Serviço</th>
                                        <th width="1">Ativo</th>
                                    </tr>
                                </thead>
                                
                                <tbody id="list-order">

                                    <?php 

                                        $stmt = $pdo->prepare('select id, label, ordem, ativo 
                                                                from tbl_servicos
                                                                order by ordem asc, id desc');
                                        $stmt->execute();

                                        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $data):

                                    ?>
                                
                                        <tr class="gradeA">
                                            <td class="center first-td"><img src="<?php echo $_vars['url_base']; ?>/img/drag2.png" alt="" width="20" class="handle"></td>
                                            <td class="center">
                                                <?php echo $data['id']; ?>
                                                <input type="hidden" name="reorder[]" value="<?php echo $data['id']; ?>" />
                                            </td>
                                            <td><?php echo $data['label']; ?></td>
                                            <td align="center">
                                                <?php echo (bool)$data['ativo'] ? '<strong class="hg-green">Sim</strong>' : '<strong class="hg-red">Não</strong>'; ?>
                                            </td>
                                        </tr>

                                    <?php endforeach; ?>

                                </tbody>
                            </table>
                        </form>
                    </div>
                    <!-- end #box-order -->
                    
                </div>
                <!-- END TABLE -->
                
                <div class="clear"></div>

            </div>
            <!-- END CONTENT -->
            
        </div>
        <!-- END PAGE -->

        <div class="clear"></div>

    </div>
    <!-- END MAIN -->
   
	<?php include sprintf('%s/includes/footer.php', $_vars['path_admin']); ?>

</div>
</body>
</html>