<?php

	require_once __DIR__ . '/config.php';
	require_once $_vars['path_admin'] . '/plugins/jQuery.filer/php/class.uploader.php';

	$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : null;

	switch ($action) {
		case 'new':
		case 'edit':

			/**
			 * Values
			 */
			$id 					= isset($_REQUEST['id']) && is_numeric($_REQUEST['id']) ? $_REQUEST['id'] : null;
			$id_servico 	= isset($_REQUEST['servico']) && ($_REQUEST['servico']) ? $_REQUEST['servico'] : null;
			$titulo 			= isset($_REQUEST['titulo']) ? trim($_REQUEST['titulo']) : null;
			$tag 			    = isset($_REQUEST['tag']) ? trim($_REQUEST['tag']) : null;
			$link   			= isset($_REQUEST['link']) ? trim($_REQUEST['link']) : null;
			$target 			= isset($_REQUEST['target']) ? trim($_REQUEST['target']) : null;
			$label_botao 		= isset($_REQUEST['label_botao']) ? trim($_REQUEST['label_botao']) : null;
			$ordem  			= isset($_REQUEST['ordem']) ? trim($_REQUEST['ordem']) : null;
			$data 				= isset($_REQUEST['data']) ? date_format(date_create_from_format('d/m/Y H:i', $_REQUEST['data']), 'Y-m-d H:i') : null;
			$texto_opcional_1  	= isset($_REQUEST['texto_opcional_1']) ? trim($_REQUEST['texto_opcional_1']) : null;
			$texto_opcional_2  	= isset($_REQUEST['texto_opcional_2']) ? trim($_REQUEST['texto_opcional_2']) : null;
			$ativo 				= isset($_REQUEST['ativo']) ? trim($_REQUEST['ativo']) : null;


			/**
			 * Save data
			 */
			$sql = $id
				? 'update tbl_side_banners set id_servico = :id_servico, titulo = :titulo, tag = :tag, link = :link, target = :target, label_botao = :label_botao, ordem = :ordem, data = :data, texto_opcional_1 = :texto_opcional_1, texto_opcional_2 = :texto_opcional_2, ativo = :ativo where id = :id'
				: 'insert into tbl_side_banners (id_servico, titulo, tag, link, target, label_botao, ordem, data, texto_opcional_1, texto_opcional_2, ativo, created_at) values(:id_servico, :titulo, :tag, :link, :target, :label_botao, :ordem, :data, :texto_opcional_1, :texto_opcional_2, :ativo, :created_at)';

			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(':id_servico', $id_servico, PDO::PARAM_STR);
			$stmt->bindValue(':titulo', $titulo, PDO::PARAM_STR);
			$stmt->bindValue(':tag', $tag, PDO::PARAM_STR);
			$stmt->bindValue(':link', $link, PDO::PARAM_STR);
			$stmt->bindValue(':target', $target, PDO::PARAM_STR);
			$stmt->bindValue(':label_botao', $label_botao, PDO::PARAM_STR);
			$stmt->bindValue(':ordem', $ordem, PDO::PARAM_STR);
			$stmt->bindValue(':data', $data, PDO::PARAM_STR);
			$stmt->bindValue(':texto_opcional_1', $texto_opcional_1, PDO::PARAM_STR);
			$stmt->bindValue(':texto_opcional_2', $texto_opcional_2, PDO::PARAM_STR);
			$stmt->bindValue(':ativo', $ativo, PDO::PARAM_STR);

			if( $id ){
				$stmt->bindValue(':id', $id, PDO::PARAM_INT);
			} else {
				$stmt->bindValue(':created_at', date('Y-m-d H:i:s'), PDO::PARAM_STR);
			}

            $stmt->execute();

            if( ! $id ){
				$id = $pdo->lastInsertId();
			}


			/**
			 * Verifica e cria diretório
			 */
			$path = sprintf('%s/%d', $_module['path_files'], $id);

			if( ! is_dir($path) )
			{
				mkdir($path, 0755, true);
			}

			/**
			 * Ordena arquivos e itera sobre eles para salvar no diretório e no banco
			 */
			if( isset($_FILES['imagens']) )
			{
				$imagens = re_array_files($_FILES['imagens']);

				foreach($imagens as $imagem)
				{
					if($imagem['error'] > 0 || $imagem['size'] == 0)
					{
						continue;
					}

					$name     = pathinfo($imagem['name'], PATHINFO_FILENAME);
					$filename = get_slug_filename($imagem['name']);
					$thumb    = sprintf('thumb_%s', $filename);
					$media    = sprintf('media_%s', $filename);
					$large    = sprintf('large_%s', $filename);

					$img = Canvas\Canvas::Instance();
					$img->carrega($imagem['tmp_name'])
						->redimensiona( 680, 454, 'crop' )
						->grava(sprintf('%s/%s', $path, $thumb), 70);

					$img = Canvas\Canvas::Instance();
					$img->carrega($imagem['tmp_name'])
						->redimensiona( 880, 587, 'crop' )
						->grava(sprintf('%s/%s', $path, $media), 70);

					$img = Canvas\Canvas::Instance();
					$img->carrega($imagem['tmp_name'])
						->redimensiona( 1080, 720, 'crop' )
						->grava(sprintf('%s/%s', $path, $large), 70);

					$stmt = $pdo->prepare('insert into tbl_files (filename, title, type, module, relationship, created_at) values (:filename, :title, :type, :module, :relationship, :created_at)');
					$stmt->bindValue(':filename', $filename, PDO::PARAM_STR);
					$stmt->bindValue(':title', $name, PDO::PARAM_STR);
					$stmt->bindValue(':type', 'photos', PDO::PARAM_STR);
					$stmt->bindValue(':module', $_module['module_slug'], PDO::PARAM_STR);
					$stmt->bindValue(':relationship', $id, PDO::PARAM_INT);
					$stmt->bindValue(':created_at', date('Y-m-d H:i:s'), PDO::PARAM_STR);
					$stmt->execute();
				}
			}

            redirect($_module['url_base']);

		break;

		case 'destroy':

			/**
			 * Values
			 */
			$id = isset($_REQUEST['id']) && is_numeric($_REQUEST['id']) ? $_REQUEST['id'] : null;

			/**
			 * Destroy register
			 */
			$stmt = $pdo->prepare('delete from tbl_side_banners where id = :id');
			$stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();

            redirect($_module['url_base']);

		break;

		case 'destroy-image':

			/**
			 * Values
			 */
			$id = isset($_REQUEST['id']) && is_numeric($_REQUEST['id']) ? $_REQUEST['id'] : null;

			/**
			 * Destroy images
			 */
			$stmt = $pdo->prepare('select relationship, filename from tbl_files where id = :id');
			$stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();

            $fetch = $stmt->fetch(PDO::FETCH_ASSOC);

            if($fetch)
            {
				$path  = sprintf('%s/%d', $_module['path_files'], $fetch['relationship']);
				$thumb = sprintf('%s/thumb_%s', $path, $fetch['filename']);
				$media = sprintf('%s/media_%s', $path, $fetch['filename']);
				$large = sprintf('%s/large_%s', $path, $fetch['filename']);

				if( is_file($thumb) ){
					unlink($thumb);
				}

				if( is_file($media) ){
					unlink($media);
				}

				if( is_file($large) ){
					unlink($large);
				}

				/**
				 * Destroy register
				 */
				$stmt = $pdo->prepare('delete from tbl_files where id = :id');
				$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	            $stmt->execute();
            }

            redirect($_SERVER['HTTP_REFERER']);

		break;

		default:

			redirect($_module['url_base']);

		break;
	}
