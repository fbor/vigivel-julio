<?php

	require_once __DIR__ . '/../../bootstrap.php';

	$_module = array(
		'menu_slug'   	=> 'side_banners',
		'module_slug'   => 'side_banners',
		'path_files'	=> sprintf('%s/side_banners', $_vars['path_files']),
		'url_files'		=> sprintf('%s/side_banners', $_vars['url_files']),
		'url_base' 		=> sprintf('%s/modules/side_banners', $_vars['url_base']),
		'max_images'    => 1,
	);