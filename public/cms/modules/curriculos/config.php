<?php

	require_once __DIR__ . '/../../bootstrap.php';

	$_module = array(
		'menu_slug'     => 'curriculos',
		'module_slug'   => 'curriculos',
		'url_base' 		=> sprintf('%s/modules/curriculos', $_vars['url_base']),
		'path_files'	=> sprintf('%s/curriculos', $_vars['path_files']),
		'url_files'		=> sprintf('%s/curriculos', $_vars['url_files']),
		'permissoes'    => [
			'geral' => [
				'admin',
				'rh'
			],
		],
	);