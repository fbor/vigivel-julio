<?php

	require_once __DIR__ . '/../../bootstrap.php';

	$_module = array(
		'menu_slug'   	=> 'cadastro_franquias',
		'module_slug'   => 'cadastro_franquias',
		'path_files'	=> sprintf('%s/cadastro_franquias', $_vars['path_files']),
		'url_files'		=> sprintf('%s/cadastro_franquias', $_vars['url_files']),
		'url_base' 		=> sprintf('%s/modules/cadastro_franquias', $_vars['url_base']),
		'max_images'    => 1,
	);

	$imageManager = new Intervention\Image\ImageManager(['driver' => 'imagick']);