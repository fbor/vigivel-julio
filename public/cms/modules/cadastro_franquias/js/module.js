	$('.telefone').mask('(00) 00000-0000');

jQuery(function($){



	var form = $('#form');

	if( form.length ){


		/**
		 * Uploader
		 */
		$('#imagens').filer({
			limit: 1,
			maxSize: 5,
        	extensions: ['jpg', 'jpeg', 'png', 'gif'],
			showThumbs: true,
			//changeInput: true,
			//addMore: true,
			captions: {
	            button: "Selecionar",
	            feedback: "Selecione a imagem",
	            feedback2: "arquivo(s) selecionado(s)",
	            removeConfirmation: "Deseja remover este arquivo da lista de upload?",
	            errors: {
	                filesLimit: "Máximo {{fi-limit}} arquivo(s) é permitido.",
	                filesType: "Somente imagens são aceitas para upload.",
	                filesSize: "{{fi-name}} é muito grande! O máximo é {{fi-maxSize}} MB.",
	                filesSizeAll: "Arquivos selecionados são muito grandes! Faça upload de arquivos até {{fi-maxSize}} MB."
	            }
	        }
	    });

	    /**
	     * Imagens
	     */
	    $("a[rel^='prettyPhoto']").prettyPhoto({
	    	animation_speed: 'fast', 
	    	theme:'light_square',
	    	show_title: true,
	    	keyboard_shortcuts: false
	    });

	    $('.mini-delete').on('click', function(){
			var _self = $(this);

	        swal({   
	            title: "Atenção!",   
	            text: "Deseja realmente excluir esta imagem?",   
	            type: "warning",   
	            showCancelButton: true,   
	            confirmButtonColor: "#DD6B55",   
	            confirmButtonText: "Sim, quero excluir!",   
	            cancelButtonText: "Cancelar",   
	            closeOnConfirm: false
	        }, function(){   
	            location.href = _self.attr('href');
	        });

	        return false;
		});
	
	}


	/**
	 * Trash
	 */
	$('.delete-row').on('click', function(){
		var _self = $(this);

        swal({   
            title: "Atenção!",   
            text: "Deseja realmente excluir este registro?",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Sim, quero excluir!",   
            cancelButtonText: "Cancelar",   
            closeOnConfirm: false
        }, function(){   
            location.href = _self.attr('href');
        });

        return false;
	});

	/**
	 * Change city
	 */

	$("#estado").change(function(){

		let id = $("#estado").val();

		$.ajax({
			url: "modules/cadastro_franquias/ajax.php",
			dataType: 'text',
            type: 'post',
            contentType: 'application/x-www-form-urlencoded',
            data: {id: id},
            beforeSend: function() {
		        $('#cidade').html('<option>Carregando...</option>');
		    },
			success: function(j){
				let cid = JSON.parse(j)
				
        	  var options;
	          for (var i = 0; i < cid.length; i++) {
	            options += '<option value="' + cid[i].id + '">' + cid[i].nome + '</option>';
	          }

	          $('#cidade').html(options).show();
	 
        	}
        });
		
	});



});