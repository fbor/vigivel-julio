<?php

	require_once __DIR__ . '/config.php';
		
	$id 	= isset($_REQUEST['id']) && is_numeric($_REQUEST['id']) ? $_REQUEST['id'] : null;

	/**
     * Cidades
     */
    $stmt = $pdo->prepare('select id, nome from gv_cidade where estado = :id  order by id');
    $stmt->bindValue(':id', $id, PDO::PARAM_STR);
    $stmt->execute();

    $cidades = $stmt->fetchAll(PDO::FETCH_ASSOC);



	echo json_encode($cidades);