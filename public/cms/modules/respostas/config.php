<?php

	require_once __DIR__ . '/../../bootstrap.php';

	$_module = array(
		'menu_slug'   	=> 'respostas',
		'module_slug'   => 'respostas',
		'path_files'	=> sprintf('%s/respostas', $_vars['path_files']),
		'url_files'		=> sprintf('%s/respostas', $_vars['url_files']),
		'url_base' 		=> sprintf('%s/modules/respostas', $_vars['url_base']),
		'max_images'    => 1,
	);