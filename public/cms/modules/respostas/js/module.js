jQuery(function($){

	var form = $('#form');

	var campos_max = 4;   //max de 10 campos

	var x = 0;
	$('#add_field').click(function (e) {
		e.preventDefault();     //prevenir novos clicks
		if (x < campos_max) {
			$('#inner').append(
				'<div style="border-bottom: 1px solid #ddd; padding: 10px 0">'
				+ document.getElementById("lista").innerHTML +
                '<a href="#" class="remover_campo">Remover</a><br></div>');
			x++;
		}
	});
	// Remover o div anterior
	$('#inner').on("click", ".remover_campo", function (e) {
		e.preventDefault();
		$(this).parent('div').remove();
		x--;
	});

	if( form.length ){

		/**
		 * Mask
		 */
		form.find('input[name="data"]').mask("99/99/9999 99:99");


		/**
		 * Datepicker
		 */
		form.find('input[name="data"]').datetimepicker();

	}




	/**
	 * Trash
	 */
	$('.delete-row').on('click', function(){
		var _self = $(this);

        swal({
            title: "Atenção!",
            text: "Deseja realmente excluir este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim, quero excluir!",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false
        }, function(){
            location.href = _self.attr('href');
        });

        return false;
	});

});