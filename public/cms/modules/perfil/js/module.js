jQuery(function($){

	var form            = $('#form'),
        photo           = $("a[rel^='prettyPhoto']");

	if( form.length ){

        /**
         * Uploader
         */
        form.find('input[name="imagem"]').filer({
            limit: 1,
            maxSize: 5,
            extensions: ['jpg', 'jpeg', 'png', 'gif'],
            showThumbs: true,
            captions: {
                button: "Selecionar",
                feedback: "Selecione uma foto para o perfil",
                feedback2: "arquivo(s) selecionado(s)",
                removeConfirmation: "Deseja remover este arquivo da lista de upload?",
                errors: {
                    filesLimit: "Máximo {{fi-limit}} arquivo(s) é permitido.",
                    filesType: "Somente imagens são aceitas para upload.",
                    filesSize: "{{fi-name}} é muito grande! O máximo é {{fi-maxSize}} MB.",
                    filesSizeAll: "Arquivos selecionados são muito grandes! Faça upload de arquivos até {{fi-maxSize}} MB."
                }
            }
        });

        /**
         * Imagens
         */
        $('.mini-delete').on('click', function(){
            var _self = $(this);

            swal({   
                title: "Atenção!",   
                text: "Deseja realmente excluir esta imagem?",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Sim, quero excluir!",   
                cancelButtonText: "Cancelar",   
                closeOnConfirm: false
            }, function(){   
                location.href = _self.attr('href');
            });

            return false;
        });

        /**
         * Validate
         */
        form.validate({
            rules: {
                nome: { required: true },
                email: { required: true, email: true },
                username: { 
                    required: true, 
                    remote: {
                        url: form.attr('action'),
                        type: 'post',
                        data: { 
                            action: 'check-username-exists',
                            id: form.find('input[name="id"]').val(),
                        }
                    } 
                },
                csenha: { equalTo: '#senha' },
            },
            messages: {
                nome: { required: 'Insira seu nome completo' },
                email: { required: 'Insira seu e-mail', email: 'Insira um endereço de e-mail válido' },
                username: { required: 'Insira seu username', remote: "Username já consta no sistema" },
                csenha: { equalTo: 'As senhas devem ser iguais' },
            },
            submitHandler: function(form) {
                $(form).find('input[type="submit"]').prop('disabled', true);
                $(form).find('.loading').slideDown('fast');

                form.submit();
            },
            errorPlacement: function(error, element) {},
            errorContainer: form.find('#errors-box'),
            errorLabelContainer: form.find('#errors-box ul'),
            wrapper: 'li',
            errorClass: 'st-error-input',
        });
		
	};

    if( photo.length ){
        photo.prettyPhoto({
            animation_speed: 'fast', 
            theme:'light_square',
            show_title: true,
            keyboard_shortcuts: false
        });
    };

});