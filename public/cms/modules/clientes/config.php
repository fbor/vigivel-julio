<?php

	require_once __DIR__ . '/../../bootstrap.php';

	$_module = array(
		'menu_slug'   	=> 'clientes',
		'module_slug'   => 'clientes',
		'path_files'	=> sprintf('%s/clientes', $_vars['path_files']),
		'url_files'		=> sprintf('%s/clientes', $_vars['url_files']),
		'url_base' 		=> sprintf('%s/modules/clientes', $_vars['url_base']),
		'max_images'    => 1,
	);

	$imageManager = new Intervention\Image\ImageManager(['driver' => 'imagick']);