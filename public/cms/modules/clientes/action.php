<?php

	require_once __DIR__ . '/config.php';
	require_once $_vars['path_admin'] . '/plugins/jQuery.filer/php/class.uploader.php';

	$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : null;

	switch ($action) {
		case 'new':
		case 'edit':

			/**
			 * Values
			 */
			$id 	= isset($_REQUEST['id']) && is_numeric($_REQUEST['id']) ? $_REQUEST['id'] : null;
			$titulo = isset($_REQUEST['titulo']) ? trim($_REQUEST['titulo']) : null;
			$link   = isset($_REQUEST['link']) ? trim($_REQUEST['link']) : null;
			$target = isset($_REQUEST['target']) ? trim($_REQUEST['target']) : null;
			$ordem  = isset($_REQUEST['ordem']) ? trim($_REQUEST['ordem']) : null;
			$segmentos 	= isset($_REQUEST['segmentos']) && is_array($_REQUEST['segmentos']) ? $_REQUEST['segmentos'] : array();
			$ativo 	= isset($_REQUEST['ativo']) ? trim($_REQUEST['ativo']) : null;

		
			/**
			 * Save data
			 */
			$sql = $id
				? 'update tbl_clientes set titulo = :titulo, link = :link, target = :target, ordem = :ordem, segmentos = :segmentos, ativo = :ativo where id = :id' 
				: 'insert into tbl_clientes (titulo, link, target, ordem, segmentos, ativo, created_at) values(:titulo, :link, :target, :ordem, :segmentos, :ativo, :created_at)';

			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(':titulo', $titulo, PDO::PARAM_STR);
			$stmt->bindValue(':link', $link, PDO::PARAM_STR);
			$stmt->bindValue(':target', $target, PDO::PARAM_STR);
			$stmt->bindValue(':ordem', $ordem, PDO::PARAM_STR);
			$stmt->bindValue(':segmentos', implode(',', $segmentos), PDO::PARAM_STR);
			$stmt->bindValue(':ativo', $ativo, PDO::PARAM_STR);

			if( $id ){
				$stmt->bindValue(':id', $id, PDO::PARAM_INT);
			} else {
				$stmt->bindValue(':created_at', date('Y-m-d H:i:s'), PDO::PARAM_STR);
			}
            
            $stmt->execute();

            if( ! $id ){
				$id = $pdo->lastInsertId();
			}


			/**
			 * Verifica e cria diretório
			 */
			$path = sprintf('%s/%d', $_module['path_files'], $id);
			
			if( ! is_dir($path) )
			{
				mkdir($path, 0755, true);
			}

			/**
			 * Ordena arquivos e itera sobre eles para salvar no diretório e no banco
			 */
			if( isset($_FILES['imagens']) )
			{
				$imagens = re_array_files($_FILES['imagens']);
				
				foreach($imagens as $imagem)
				{
					if($imagem['error'] > 0 || $imagem['size'] == 0)
					{
						continue;
					}

					$name     = pathinfo($imagem['name'], PATHINFO_FILENAME);
					$filename = get_slug_filename($imagem['name']);
					$min      = sprintf('min_%s', $filename);
					$thumb    = sprintf('thumb_%s', $filename);
					$large    = sprintf('large_%s', $filename);

					$imageManager
						->make($imagem['tmp_name'])
						->resize(48, 27, function ($constraint) {
						    $constraint->aspectRatio();
						    $constraint->upsize();
						})
						->resizeCanvas(48, 27, 'center', false, 'ffffff')
						->save(sprintf('%s/%s', $path, $min), 20);

					$imageManager
						->make($imagem['tmp_name'])
						->resize(178, 110, function ($constraint) {
						    $constraint->aspectRatio();
						    $constraint->upsize();
						})
						->resizeCanvas(250, 140, 'center', false, 'ffffff')
						->save(sprintf('%s/%s', $path, $thumb), 70);

					$imageManager
						->make($imagem['tmp_name'])
						->resize(300, 300, function ($constraint) {
						    $constraint->aspectRatio();
						    $constraint->upsize();
						})
						->save(sprintf('%s/%s', $path, $large), 70);

					$stmt = $pdo->prepare('insert into tbl_files (filename, title, type, module, relationship, created_at) values (:filename, :title, :type, :module, :relationship, :created_at)');
					$stmt->bindValue(':filename', $filename, PDO::PARAM_STR);
					$stmt->bindValue(':title', $name, PDO::PARAM_STR);
					$stmt->bindValue(':type', 'photos', PDO::PARAM_STR);
					$stmt->bindValue(':module', $_module['module_slug'], PDO::PARAM_STR);
					$stmt->bindValue(':relationship', $id, PDO::PARAM_INT);
					$stmt->bindValue(':created_at', date('Y-m-d H:i:s'), PDO::PARAM_STR);
					$stmt->execute();
				}
			}

            redirect($_module['url_base']);

		break;

		case 'destroy':

			/**
			 * Values
			 */
			$id = isset($_REQUEST['id']) && is_numeric($_REQUEST['id']) ? $_REQUEST['id'] : null;

			/**
			 * Destroy register
			 */
			$stmt = $pdo->prepare('delete from tbl_clientes where id = :id');
			$stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();

            redirect($_module['url_base']);

		break;

		case 'destroy-image':

			/**
			 * Values
			 */
			$id = isset($_REQUEST['id']) && is_numeric($_REQUEST['id']) ? $_REQUEST['id'] : null;

			/**
			 * Destroy images
			 */
			$stmt = $pdo->prepare('select relationship, filename from tbl_files where id = :id');
			$stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();

            $fetch = $stmt->fetch(PDO::FETCH_ASSOC);

            if($fetch)
            {
				$path  = sprintf('%s/%d', $_module['path_files'], $fetch['relationship']);
				$min   = sprintf('%s/min_%s', $path, $fetch['filename']);
				$thumb = sprintf('%s/thumb_%s', $path, $fetch['filename']);
				$large = sprintf('%s/large_%s', $path, $fetch['filename']);

				if( is_file($min) ){
					unlink($min);
				}

				if( is_file($thumb) ){
					unlink($thumb);
				}

				if( is_file($large) ){
					unlink($large);
				}

				/**
				 * Destroy register
				 */
				$stmt = $pdo->prepare('delete from tbl_files where id = :id');
				$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	            $stmt->execute();
            }

            redirect($_SERVER['HTTP_REFERER']);

		break;
		
		default:

			redirect($_module['url_base']);

		break;
	}