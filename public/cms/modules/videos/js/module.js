jQuery(function($){

	var form            = $('#form'),
        list_rows       = $('#list-rows');

	if( form.length ){

        /**
         * Validate
         */
        form.validate({
            rules: {
                label: { required: true },
                servico: { required: true },
                slug: { required: true },
                ordem: { required: true },
                imagem_fixa: { required: true },
            },
            messages: {
                label: { required: 'Insira o nome do video' },
                servico: { required: 'Insira serviços' },
                slug: { required: 'Insira o slug do serviço' },
                ordem: { required: 'Insira a ordem do serviço' },
                imagem_fixa: { required: 'Insira uma imagem' },
            },
            submitHandler: function(form) {
                $(form).find('input[type="submit"]').prop('disabled', true);
                $(form).find('.loading').slideDown('fast');

                form.submit();
            },
            errorPlacement: function(error, element) {},
            errorContainer: form.find('#errors-box'),
            errorLabelContainer: form.find('#errors-box ul'),
            wrapper: 'li',
            errorClass: 'st-error-input',
        });

        /**
         * Uploader
         */
        $('#imagem_fixa').filer({
            limit: 1,
            extensions: ['jpg', 'jpeg', 'png', 'gif'],
            showThumbs: true,
            captions: {
                button: "Selecionar",
                feedback: "Selecione uma imagem",
                feedback2: "arquivo(s) selecionado(s)",
                removeConfirmation: "Deseja remover este arquivo da lista de upload?",
                errors: {
                    filesLimit: "Máximo {{fi-limit}} arquivo(s) é permitido.",
                    filesType: "Somente imagens são aceitas para upload.",
                    filesSize: "{{fi-name}} é muito grande! O máximo é {{fi-maxSize}} MB.",
                    filesSizeAll: "Arquivos selecionados são muito grandes! Faça upload de arquivos até {{fi-maxSize}} MB."
                }
            }
        });

        $('#icon').filer({
            limit: 1,
            extensions: ['png'],
            showThumbs: true,
            captions: {
                button: "Selecionar",
                feedback: "Selecione um ícone",
                feedback2: "arquivo(s) selecionado(s)",
                removeConfirmation: "Deseja remover este arquivo da lista de upload?",
                errors: {
                    filesLimit: "Máximo {{fi-limit}} arquivo(s) é permitido.",
                    filesType: "Somente imagens PNG são aceitas para upload.",
                    filesSize: "{{fi-name}} é muito grande! O máximo é {{fi-maxSize}} MB.",
                    filesSizeAll: "Arquivos selecionados são muito grandes! Faça upload de arquivos até {{fi-maxSize}} MB."
                }
            }
        });

        /**
         * Imagens
         */
        $("a[rel^='prettyPhoto']").prettyPhoto({
            animation_speed: 'fast',
            theme:'light_square',
            show_title: true,
            keyboard_shortcuts: false
        });

        $('.mini-delete').on('click', function(){
            var _self = $(this);

            swal({
                title: "Atenção!",
                text: "Deseja realmente excluir esta imagem?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, quero excluir!",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false
            }, function(){
                location.href = _self.attr('href');
            });

            return false;
        });

	};


    if( list_rows.length ){

        /**
         * List rows
         */
        list_rows.dataTable({
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "aaSorting": [[ 2, "asc" ]]
        });

        /**
         * Order
         */
        dragula([document.getElementById('list-order')], {
            moves: function (el, container, handle) {
                return handle.classList.contains('handle');
            }
        });

        $('#btn-reordenar').on('click', function(e){
            e.preventDefault();

            $('#box-list-rows').hide();
            $('#box-order').show();
        });

        /**
         * Trash
         */
        $('.delete-row').on('click', function(){
            var _self = $(this);

            swal({
                title: "Atenção!",
                text: "Deseja realmente excluir este registro?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, quero excluir!",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false
            }, function(){
                location.href = _self.attr('href');
            });

            return false;
        });

    };

});