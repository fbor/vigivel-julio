<?php

	require_once __DIR__ . '/../../bootstrap.php';

	$_module = array(
		'menu_slug'   	=> 'ja_cliente',
		'module_slug'   => 'ja_cliente',
		'path_files'	=> sprintf('%s/ja_cliente', $_vars['path_files']),
		'url_files'		=> sprintf('%s/ja_cliente', $_vars['url_files']),
		'url_base' 		=> sprintf('%s/modules/ja_cliente', $_vars['url_base']),
		'max_images'    => 1,
	);