<?php

    require __DIR__ . '/config.php';

    check_logged();

    $menu_active = $_module['menu_slug'];

    /**
     * Actions
     */
    $actions = array('new', 'edit');
    $action  = isset($_REQUEST['action']) && in_array($_REQUEST['action'], $actions) ? $_REQUEST['action'] : 'new';

    /**
     * Edit
     */
    $id = ($action == 'edit') && isset($_REQUEST['id']) && is_numeric($_REQUEST['id']) ? $_REQUEST['id'] : null;

    $stmt = $pdo->prepare('select id, label from tbl_servicos where ativo = 1');
    $stmt->execute();

    $servicos = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if( $id ){
        $stmt = $pdo->prepare('select * from tbl_ja_cliente where id = :id');
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $data = $stmt->fetch(PDO::FETCH_ASSOC);

        if( $data )
        {
            $_edit_data              = strftime('%d/%m/%Y %H:%M:%S', strtotime($data['data']));
            $_edit_titulo            = $data['titulo'];
            $_edit_id_servico        = $data['id_servico'];
            $_edit_tag               = $data['tag'];
            $_edit_link              = $data['link'];
            $_edit_target            = $data['target'];
            $_edit_label_botao       = $data['label_botao'];
            $_edit_ordem             = $data['ordem'];
            $_edit_texto_opcional_1  = $data['texto_opcional_1'];
            $_edit_texto_opcional_2  = $data['texto_opcional_2'];
            $_edit_ativo             = $data['ativo'];


            // Fotos
            $stmt_fotos = $pdo->prepare('select id, title, filename from tbl_files where type = "photos" and module = "banners" and relationship = :id');
            $stmt_fotos->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt_fotos->execute();

            $_edit_imagens = $stmt_fotos->fetchAll(PDO::FETCH_ASSOC);
        }
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <base href="<?php echo $_vars['url_base']; ?>/" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo $_vars['title']; ?></title>

    <!-- Reset -->
    <link rel="stylesheet" type="text/css" href="style/reset.css" />
    <!-- Main Style File -->
    <link rel="stylesheet" type="text/css" href="style/root.css" />
    <!-- Grid Styles -->
    <link rel="stylesheet" type="text/css" href="style/grid.css" />
    <!-- Typography Elements -->
    <link rel="stylesheet" type="text/css" href="style/typography.css" />
    <!-- Jquery UI -->
    <link rel="stylesheet" type="text/css" href="style/jquery-ui.css" />
    <!-- Jquery Plugin Css Files Base -->
    <link rel="stylesheet" type="text/css" href="style/jquery-plugin-base.css" />
    <!-- sweetalert -->
    <link rel="stylesheet" type="text/css" href="plugins/sweetalert/dist/sweetalert.css" />
    <!-- jQuery Filer -->
    <link rel="stylesheet" type="text/css" href="plugins/jQuery.filer/css/jquery.filer.css" />
    <link rel="stylesheet" type="text/css" href="plugins/jQuery.filer/css/themes/jquery.filer-dragdropbox-theme.css" />

    <!--[if IE 7]>	  <link rel="stylesheet" type="text/css" href="style/ie7-style.css" />	<![endif]-->

    <!-- jquery base -->
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.11.custom.min.js"></script>
    <!-- jquery plugins settings -->
	<script type="text/javascript" src="js/jquery-settings.js"></script>
    <!-- toggle -->
	<script type="text/javascript" src="js/toogle.js"></script>
    <!-- tipsy -->
	<script type="text/javascript" src="js/jquery.tipsy.js"></script>
    <!-- uniform -->
	<script type="text/javascript" src="js/jquery.uniform.min.js"></script>
    <!-- Jwysiwyg editor -->
	<script type="text/javascript" src="js/jquery.wysiwyg.js"></script>
    <!-- table shorter -->
	<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>

    <!-- fullcalendar -->
	<script type="text/javascript" src="js/fullcalendar.min.js"></script>
    <!-- prettyPhoto -->
	<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
    <!-- Jquery.UI Base -->
	<script type="text/javascript" src="js/jquery.ui.core.js"></script>
	<script type="text/javascript" src="js/jquery.ui.mouse.js"></script>
	<script type="text/javascript" src="js/jquery.ui.widget.js"></script>
    <!-- Slider -->
	<script type="text/javascript" src="js/jquery.ui.slider.js"></script>
    <!-- Date Picker -->
    <script type="text/javascript" src="js/jquery.ui.datepicker.js"></script>
    <script type="text/javascript" src="js/jquery.datetimepicker.js"></script>
    <script type="text/javascript" src="js/jquery.ui.datepicker-pt-BR.js"></script>
	<script type="text/javascript" src="js/jquery.datetimepicker-pt-BR.js"></script>
    <!-- Tabs -->
	<script type="text/javascript" src="js/jquery.ui.tabs.js"></script>
    <!-- Accordion -->
	<script type="text/javascript" src="js/jquery.ui.accordion.js"></script>
    <!-- Google Js Api / Chart and others -->
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <!-- Date Tables -->
	<script type="text/javascript" src="js/jquery.dataTables.js"></script>

    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/jquery.maskMoney.js"></script>

    <!-- sweetalert -->
    <script type="text/javascript" src="plugins/sweetalert/dist/sweetalert.min.js"></script>

    <!-- jQuery Filer -->
    <script type="text/javascript" src="plugins/jQuery.filer/js/jquery.filer.min.js?v=1.0.5"></script>

    <!-- Module -->
    <script type="text/javascript" src="<?php echo $_module['url_base'] ?>/js/module.js"></script>
    <script type="text/javascript" src="plugins/ckeditor/ckeditor.js"></script>

</head>
<body>
<div class="wrapper">

	<?php include sprintf('%s/includes/header.php', $_vars['path_admin']); ?>

    <!-- START MAIN -->
    <div id="main">

        <?php include sprintf('%s/includes/sidebar.php', $_vars['path_admin']); ?>

        <!-- START PAGE -->
        <div id="page">

            <?php include __DIR__ . '/includes/header.php'; ?>

            <!-- START CONTENT -->
            <div class="content">

                <!-- START SIMPLE FORM -->
                <div class="simplebox grid740">
                    <div class="titleh">
                        <h3>Insira um novo banner</h3>
                        <div class="shortcuts-icons">
                            <a href="javascript:history.back();" style="padding:5px;"><img src="img/icons/sidemenu/arrow_left.png" alt=""></a>
                        </div>
                    </div>
                    <div class="body">

                      <form id="form" name="form" method="post" action="<?php echo $_module['url_base'] ?>/action.php" enctype="multipart/form-data">

                        <div class="st-form-line">
                            <span class="st-labeltext">Título</span>
                            <input name="titulo" type="text" class="st-forminput" id="textfield1" style="width:510px" value="<?php echo isset($_edit_titulo) ? $_edit_titulo : ''; ?>" />
                            <div class="clear"></div>
                        </div>

                        <div class="st-form-line">  
                            <span class="st-labeltext"><span class="required-red">*</span> servicos</span>
                            <select name="servico" class="uniform select" id="selectCategories" style="width:510px"  >
                                <?php
                                
                                foreach ($servicos as $servico) {
                                             
                                if (isset($_edit_id_servico)){
                                    printf('<option selected value="%s" >%s</option>',$servico['id'],$servico['label']);

                                }else{

                                printf('<option value="%s" >%s</option>',$servico['id'],$servico['label']);
                                }


                                }
                                ?>
                            </select>
                            <div class="clear"></div> 
                        </div>

                        <?php /* ?><div class="st-form-line">  
                            <span class="st-labeltext">Tag</span>
                            <input name="tag" type="text" class="st-forminput" id="textfield1" style="width:510px" value="<?php echo isset($_edit_tag) ? $_edit_tag : ''; ?>" />
                            <div class="clear"></div>
                        </div><?php */ ?>

                        <div class="st-form-line">
                            <span class="st-labeltext">Link</span>
                            <input name="link" type="text" class="st-forminput" id="textfield1" style="width:510px" value="<?php echo isset($_edit_link) ? $_edit_link : ''; ?>" />
                            <div class="clear"></div>
                        </div>

                        <div class="st-form-line">
                            <span class="st-labeltext">Abrir link</span>
                            <select name="target" class="uniform" style="width:523px">
                                <option value="">Selecione</option>
                                <option value="_self" <?php echo isset($_edit_target) && $_edit_target == '_self' ? 'selected' : ''; ?>>Mesma página</option>
                                <option value="_blank" <?php echo isset($_edit_target) && $_edit_target == '_blank' ? 'selected' : ''; ?>>Nova página</option>
                            </select>
                            <div class="clear"></div>
                        </div>

                        <div class="st-form-line">
                            <span class="st-labeltext">Texto do botão</span>
                            <input name="label_botao" type="text" class="st-forminput" id="textfield1" style="width:510px" value="<?php echo isset($_edit_label_botao) ? $_edit_label_botao : ''; ?>" />
                            <div class="clear"></div>
                        </div>

                        <div class="st-form-line">
                            <span class="st-labeltext">Mostrar até</span>
                            <input type="text" name="data" class="datepicker-input" style="width:180px;" value="<?php echo isset($_edit_data) ? $_edit_data : date('d/m/Y H:i'); ?>" />
                            <div class="clear"></div>
                        </div>

                        <div class="st-form-line">
                            <div class="titleh">
                                <h3>Descrição</h3>
                            </div>
                            <div class="body">
                                <textarea name="texto_opcional_1" id="texto_opcional_1" class="ckeditor" style="width:98%; height: 100px" ><?php echo isset($_edit_texto_opcional_1) ? $_edit_texto_opcional_1 : ''; ?></textarea>
                            </div>
                        </div>

                        <?php /* ?><div class="st-form-line">
                            <div class="titleh">
                                <h3>Texto opcional <em>(2º parágrafo)</em></h3>
                            </div>
                            <div class="body">
                                <textarea name="texto_opcional_2" id="texto_opcional_2" class="st-forminput" rows="10"  style="width:682px;"><?php echo isset($_edit_texto_opcional_2) ? $_edit_texto_opcional_2 : ''; ?></textarea>
                            </div>
                        </div><?php */ ?>

                        

                        <div class="st-form-line">
                            <span class="st-labeltext">Ordem</span>
                            <input name="ordem" type="text" class="st-forminput" id="textfield1" style="width:50px" value="<?php echo isset($_edit_ordem) ? $_edit_ordem : ''; ?>" />
                            <div class="clear"></div>
                        </div>

                        <div class="st-form-line">
                            <span class="st-labeltext">Ativo</span>
                            <label class="margin-right10"><input type="radio" name="ativo" class="uniform" value="1" <?php echo ($action == 'new' || (isset($_edit_ativo) && $_edit_ativo == 1)) ? 'checked="checked"' : ''; ?> /> Sim</label>
                            <label class="margin-right10"><input type="radio" name="ativo" class="uniform" value="0" <?php echo (isset($_edit_ativo) && $_edit_ativo == 0) ? 'checked="checked"' : ''; ?> /> Não</label>
                            <div class="clear"></div>
                        </div>

                        <div class="button-box">
                            <input type="submit" name="button" id="button" value="Salvar" class="st-button"/>
                        </div>

                        <input type="hidden" name="action" value="<?php echo $action; ?>" />

                        <?php if($action == 'edit'): ?>
                            <input type="hidden" name="id" value="<?php echo $id; ?>" />
                        <?php endif; ?>

                      </form>

                    </div>
                </div>
                <!-- END SIMPLE FORM -->

                <div class="clear"></div>

            </div>
            <!-- END CONTENT -->

        </div>
        <!-- END PAGE -->

        <div class="clear"></div>

    </div>
    <!-- END MAIN -->

	<?php include sprintf('%s/includes/footer.php', $_vars['path_admin']); ?>

</div>
</body>
</html>
