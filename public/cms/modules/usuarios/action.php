<?php

	require_once __DIR__ . '/config.php';
	require_once $_vars['path_admin'] . '/plugins/jQuery.filer/php/class.uploader.php';

	$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : null;

	switch ($action) {
		case 'new':
		case 'edit':

			usleep(600000);

			/**
			 * Values
			 */
			$id 				= isset($_REQUEST['id']) && is_numeric($_REQUEST['id']) ? $_REQUEST['id'] : null;
			$nome 				= isset($_REQUEST['nome']) ? trim($_REQUEST['nome']) : null;
			$email 				= isset($_REQUEST['email']) && filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL) ? trim($_REQUEST['email']) : null;
			$username 			= isset($_REQUEST['username']) ? trim($_REQUEST['username']) : null;
			$senha 				= isset($_REQUEST['senha']) && ! empty($_REQUEST['senha']) ? trim(md5($_REQUEST['senha'])) : null;
			$senha 				= isset($_REQUEST['senha_edit']) && ! empty($_REQUEST['senha_edit']) ? trim(md5($_REQUEST['senha_edit'])) : $senha;
			$nivel_permissao 	= isset($_REQUEST['nivel_permissao']) ? trim($_REQUEST['nivel_permissao']) : 'admin';
			$ativo 				= isset($_REQUEST['ativo']) ? trim($_REQUEST['ativo']) : null;

		
			/**
			 * Save data
			 */
			$senha_sql = ! is_null($senha) ? 'password = :senha,' : '';
			$sql 	   = ! is_null($id)
				? sprintf('update tbl_users set nome = :nome, email = :email, username = :username, %s nivel_permissao = :nivel_permissao, ativo = :ativo where id = :id', $senha_sql)
				: 'insert into tbl_users (nome, email, username, password, nivel_permissao, ativo, created_at) values(:nome, :email, :username, :senha, :nivel_permissao, :ativo, :created_at)';

			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(':nome', $nome, PDO::PARAM_STR);
			$stmt->bindValue(':email', $email, PDO::PARAM_STR);
			$stmt->bindValue(':username', $username, PDO::PARAM_STR);
			
			if( ! is_null($senha) ){
				$stmt->bindValue(':senha', $senha, PDO::PARAM_STR);
			}

			$stmt->bindValue(':nivel_permissao', $nivel_permissao, PDO::PARAM_STR);
			$stmt->bindValue(':ativo', $ativo, PDO::PARAM_STR);

			if( $id ){
				$stmt->bindValue(':id', $id, PDO::PARAM_INT);
			} else {
				$stmt->bindValue(':created_at', date('Y-m-d H:i:s'), PDO::PARAM_STR);
			}
            
            $stmt->execute();

            if( ! $id ){
				$id = $pdo->lastInsertId();
			}

			/**
			 * Verifica e cria diretório
			 */
			$path = sprintf('%s/%d', $_module['path_files'], $id);
			
			if( ! is_dir($path) )
			{
				mkdir($path, 0755, true);
			}

			
			if(isset($_FILES['imagem']) && $_FILES['imagem']['error'] == 0 && $_FILES['imagem']['size'] > 0)
			{
				$imagem = $_FILES['imagem'];
				
				$name     = pathinfo($imagem['name'], PATHINFO_FILENAME);
				$filename = get_slug_filename($imagem['name']);
				$thumb    = sprintf('thumb_%s', $filename);
				$large    = sprintf('large_%s', $filename);

				$img = Canvas\Canvas::Instance();
				
				$img->carrega($imagem['tmp_name'])
					->hexa('#ffffff')
					->redimensiona( 35, 35, 'crop' )
					->grava(sprintf('%s/%s', $path, $thumb));

				$img->carrega($imagem['tmp_name'])
					->hexa('#ffffff')
					->redimensiona( 500, '100%', 'proporcional' )
					->grava(sprintf('%s/%s', $path, $large));

				$stmt = $pdo->prepare('insert into tbl_files (filename, title, type, module, relationship, created_at) values (:filename, :title, :type, :module, :relationship, :created_at)');
				$stmt->bindValue(':filename', $filename, PDO::PARAM_STR);
				$stmt->bindValue(':title', $name, PDO::PARAM_STR);
				$stmt->bindValue(':type', 'photos', PDO::PARAM_STR);
				$stmt->bindValue(':module', $_module['module_slug'], PDO::PARAM_STR);
				$stmt->bindValue(':relationship', $id, PDO::PARAM_INT);
				$stmt->bindValue(':created_at', date('Y-m-d H:i:s'), PDO::PARAM_STR);
				$stmt->execute();
			}

			set_flash_message('message', ($action == 'new') ? 'Novo registro adicionado.' : 'Registro alterado.');
            redirect($_module['url_base']);

		break;

		case 'destroy':

			/**
			 * Values
			 */
			$id = isset($_REQUEST['id']) && is_numeric($_REQUEST['id']) ? $_REQUEST['id'] : null;

			/**
			 * Destroy register
			 */
			$stmt = $pdo->prepare('delete from tbl_users where id = :id');
			$stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();

            set_flash_message('message', 'Registro removido.');
            redirect($_module['url_base']);

		break;

		case 'check-username-exists':

			/**
			 * Values
			 */
			$id 		= isset($_REQUEST['id']) && is_numeric($_REQUEST['id']) ? $_REQUEST['id'] : 0;
			$username 	= isset($_REQUEST['username']) ? trim($_REQUEST['username']) : null;

			/**
			 * Check username exists
			 */
			$stmt = $pdo->prepare('select count(*) from tbl_users where username = :username and id <> :id');
			$stmt->bindValue(':username', $username, PDO::PARAM_STR);
			$stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();

            $num_rows = $stmt->fetchColumn();

            if( (bool)$num_rows ){
            	echo 'false';
            	exit;
            }

            echo 'true';

		break;

		case 'destroy-image':

			/**
			 * Values
			 */
			$id = isset($_REQUEST['id']) && is_numeric($_REQUEST['id']) ? $_REQUEST['id'] : null;

			/**
			 * Destroy images
			 */
			$stmt = $pdo->prepare('select relationship, filename from tbl_files where id = :id');
			$stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();

            $fetch = $stmt->fetch(PDO::FETCH_ASSOC);

            if($fetch)
            {
				$path  = sprintf('%s/%d', $_module['path_files'], $fetch['relationship']);
				$thumb = sprintf('%s/thumb_%s', $path, $fetch['filename']);
				$large = sprintf('%s/large_%s', $path, $fetch['filename']);

				if( is_file($thumb) ){
					unlink($thumb);
				}

				if( is_file($large) ){
					unlink($large);
				}

				/**
				 * Destroy register
				 */
				$stmt = $pdo->prepare('delete from tbl_files where id = :id');
				$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	            $stmt->execute();
            }

            redirect($_SERVER['HTTP_REFERER']);

		break;
		
		default:

			exit;

		break;
	}