<?php

	require_once __DIR__ . '/../../bootstrap.php';

	$_module = array(
		'menu_slug'     => 'usuarios',
		'module_slug'   => 'usuarios',
		'url_base' 		=> sprintf('%s/modules/usuarios', $_vars['url_base']),
		'path_files'	=> sprintf('%s/usuarios', $_vars['path_files']),
		'url_files'		=> sprintf('%s/usuarios', $_vars['url_files']),
		'max_images'    => 1,
		'permissoes'    => [
			'geral' => [
				'admin',
			],
		],
	);