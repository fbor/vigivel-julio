<?php

    require __DIR__ . '/config.php';

    check_logged();
    check_permissions($_SESSION['user']['nivel_permissao'], $_module['permissoes']['geral']);

    $menu_active = $_module['menu_slug'];

    /**
     * Actions
     */
    $actions = array('new', 'edit');
    $action  = isset($_REQUEST['action']) && in_array($_REQUEST['action'], $actions) ? $_REQUEST['action'] : 'new';

    /**
     * Edit
     */
    $id = isset($_REQUEST['id']) && is_numeric($_REQUEST['id']) ? $_REQUEST['id'] : null;

    if( $action == 'edit' && is_null($id) ){
        redirect($_module['url_base']);
    }

    if( ! is_null($id) ){
        $stmt = $pdo->prepare('select * from tbl_users where id = :id and username <> "goutnix"');
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $data = $stmt->fetch(PDO::FETCH_ASSOC);

        if( ! $data )
        {
            redirect($_module['url_base']);
        }

        $_edit_nome             = $data['nome'];
        $_edit_email            = $data['email'];
        $_edit_username         = $data['username'];
        $_edit_nivel_permissao  = $data['nivel_permissao'];
        $_edit_ativo            = $data['ativo'];

        // Imagem do perfil
        $stmt_imagens = $pdo->prepare('select id, title, filename from tbl_files where type = "photos" and module = :module and relationship = :id');
        $stmt_imagens->bindValue(':module', $_module['module_slug'], PDO::PARAM_STR);
        $stmt_imagens->bindValue(':id', $id, PDO::PARAM_INT);
        $stmt_imagens->execute();

        $_edit_imagens = $stmt_imagens->fetchAll(PDO::FETCH_ASSOC);

    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <base href="<?php echo $_vars['url_base']; ?>/" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo $_vars['title']; ?></title>
	
    <link rel="stylesheet" type="text/css" href="style/reset.css" /> 
    <link rel="stylesheet" type="text/css" href="style/root.css" /> 
    <link rel="stylesheet" type="text/css" href="style/grid.css" /> 
    <link rel="stylesheet" type="text/css" href="style/typography.css" /> 
    <link rel="stylesheet" type="text/css" href="style/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="style/jquery-plugin-base.css" />
    <link rel="stylesheet" type="text/css" href="plugins/sweetalert/dist/sweetalert.css" />
    <link rel="stylesheet" type="text/css" href="plugins/jQuery.filer/css/jquery.filer.css" />
    <link rel="stylesheet" type="text/css" href="plugins/jQuery.filer/css/themes/jquery.filer-dragdropbox-theme.css" />
    
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.tipsy.js"></script>
	<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/jquery.maskMoney.js"></script>
    <script type="text/javascript" src="plugins/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="plugins/jQuery.filer/js/jquery.filer.min.js?v=1.0.5"></script>
    <script type="text/javascript" src="js/jquery-settings.js"></script>
    <script type="text/javascript" src="<?php echo $_module['url_base'] ?>/js/module.js"></script>
    
</head>
<body>
<div class="wrapper">

	<?php include sprintf('%s/includes/header.php', $_vars['path_admin']); ?>
    
    <!-- START MAIN -->
    <div id="main">

        <?php include sprintf('%s/includes/sidebar.php', $_vars['path_admin']); ?>
                
        <!-- START PAGE -->
        <div id="page">
            	
            <?php include __DIR__ . '/includes/header.php'; ?>
                
            <!-- START CONTENT -->
            <div class="content">

                <!-- START SIMPLE FORM -->
                <div class="simplebox grid740">
                    <div class="titleh">
                        <h3>Cadastro/Edição de usuário</h3>
                        <div class="shortcuts-icons description-text">
                            Campos com <span class="required-red">*</span> são obrigatórios
                        </div>
                    </div>
                    <div class="body">
                    
                      <form id="form" name="form" method="post" action="<?php echo $_module['url_base'] ?>/action.php" enctype="multipart/form-data">

                        <div class="st-form-line">  
                            <span class="st-labeltext"><span class="required-red">*</span> Nome</span> 
                            <input name="nome" id="nome" type="text" class="st-forminput" style="width:510px" value="<?php echo isset($_edit_nome) ? $_edit_nome : ''; ?>" /> 
                            <div class="clear"></div>
                        </div>

                        <div class="st-form-line">  
                            <span class="st-labeltext"><span class="required-red">*</span> E-mail</span> 
                            <input name="email" id="email" type="text" class="st-forminput" style="width:510px" value="<?php echo isset($_edit_email) ? $_edit_email : ''; ?>" /> 
                            <div class="clear"></div>
                        </div>

                        <div class="st-form-line">  
                            <span class="st-labeltext"><span class="required-red">*</span> Username</span> 
                            <input name="username" id="username" type="text" class="st-forminput" style="width:510px" value="<?php echo isset($_edit_username) ? $_edit_username : ''; ?>" /> 
                            <div class="clear"></div>
                        </div>

                        <div class="st-form-line">  
                            <span class="st-labeltext"><?php if($action == 'new'): ?><span class="required-red">*</span> <?php endif; ?>Senha</span> 
                            <input name="<?php echo $action == 'new' ? 'senha' : 'senha_edit'; ?>" id="<?php echo $action == 'new' ? 'senha' : 'senha_edit'; ?>" type="password" class="st-forminput" style="width:510px" />
                            <div class="clear"></div>
                        </div>

                        <div class="st-form-line">  
                            <span class="st-labeltext"><?php if($action == 'new'): ?><span class="required-red">*</span> <?php endif; ?>Confirme a senha</span> 
                            <input name="<?php echo $action == 'new' ? 'csenha' : 'csenha_edit'; ?>" id="<?php echo $action == 'new' ? 'csenha' : 'csenha_edit'; ?>" type="password" class="st-forminput" style="width:510px" /> 
                            <div class="clear"></div>
                        </div>

                        <div class="st-form-line">  
                            <span class="st-labeltext"><span class="required-red">*</span> Nível de permissão</span>
                            <select name="nivel_permissao" class="uniform" style="width:523px">
                                <?php
                                
                                    foreach ($_niveis_usuario as $key => $value) {
                                        $selected = isset($_edit_nivel_permissao) && $_edit_nivel_permissao == $key ? 'selected' : '';
                                        printf('<option value="%s" %s>%s</option>', $key, $selected, $value);
                                    }
                                
                                ?>
                            </select>
                            <div class="clear"></div> 
                        </div>

                        <div class="st-form-line">  
                            <span class="st-labeltext">Foto do perfil</span>

                            <?php

                                if( 
                                    $action == 'new' || 
                                    ( isset($_edit_imagens) && is_array($_edit_imagens) && count($_edit_imagens) < $_module['max_images'] )
                                ):

                            ?>
                                <span class="ml0 st-form-error">(Formatos aceitos: jpg/jpge, png, gif)</span>
                                <input type="file" name="imagem" id="imagem">
                            <?php endif; ?>

                            <div class="clear"></div>

                            <?php 

                                if( isset($_edit_imagens) && is_array($_edit_imagens) && count($_edit_imagens) > 0 ):
                                    foreach($_edit_imagens as $imagem):

                                        $large = sprintf('%s/%d/large_%s', $_module['url_files'], $id, $imagem['filename']);

                            ?>
                            <div class="get-photo">
                                <div class="buttons">   
                                    <a href="<?php echo $_module['url_base'] ?>/action.php?action=destroy-image&id=<?php echo $imagem['id']; ?>" class="mini-delete">Delete</a>  
                                    <div class="clear"></div>
                                </div>
                                <a href="<?php echo $large; ?>" rel="prettyPhoto"><img src="<?php echo $large; ?>" width="100%" alt="" /></a>
                            </div>
                            <?php

                                    endforeach;
                                endif;

                            ?>
                            <div class="clear"></div>
                        </div>
                        
                        <div class="st-form-line">  
                            <span class="st-labeltext"><span class="required-red">*</span> Ativo</span>    
                            <label class="margin-right10"><input type="radio" name="ativo" class="uniform" value="1" <?php echo ($action == 'new' || (isset($_edit_ativo) && $_edit_ativo == 1)) ? 'checked="checked"' : ''; ?> /> Sim</label> 
                            <label class="margin-right10"><input type="radio" name="ativo" class="uniform" value="0" <?php echo (isset($_edit_ativo) && $_edit_ativo == 0) ? 'checked="checked"' : ''; ?> /> Não</label>
                            <div class="clear"></div> 
                        </div>

                        <div class="simple-tips" id="errors-box" style="display:none;">
                            <h2><img src="img/icons/error/error.png" width="13" alt="" /> Atenção!</h2>
                            <ul></ul>
                        </div>

                        <div class="simple-tips loading">
                            <img src="img/loading/5.gif" alt="icon">
                        </div>
                        
                        <div class="button-box">
                            <input type="submit" name="button" value="Salvar" class="st-button"/>
                            <a class="button-gray" href="<?php echo $_module['url_base']; ?>">Cancelar</a>
                        </div>

                        <input type="hidden" name="action" value="<?php echo $action; ?>" />

                        <?php if($action == 'edit'): ?>
                            <input type="hidden" name="id" value="<?php echo $id; ?>" />
                        <?php endif; ?>
                        
                      </form>
                      
                    </div>
                </div>
                <!-- END SIMPLE FORM -->
                
                <div class="clear"></div>

            </div>
            <!-- END CONTENT -->
            
        </div>
        <!-- END PAGE -->

        <div class="clear"></div>

    </div>
    <!-- END MAIN -->
   
	<?php include sprintf('%s/includes/footer.php', $_vars['path_admin']); ?>

</div>
</body>
</html>