<?php

    require __DIR__ . '/config.php';

    check_logged();
    check_permissions($_SESSION['user']['nivel_permissao'], $_module['permissoes']['geral']);

    $menu_active = $_module['menu_slug'];

    /**
     * Edit
     */
    $id = isset($_REQUEST['id']) && is_numeric($_REQUEST['id']) ? $_REQUEST['id'] : null;

    $stmt = $pdo->prepare('select * from tbl_franquias where id = :id');
    $stmt->bindValue(':id', $id, PDO::PARAM_INT);
    $stmt->execute();

    $data = $stmt->fetch(PDO::FETCH_ASSOC);

    if( ! $data ){
        redirect($_module['url_base']);
    }

    // Marcar como visualizado
    $stmt = $pdo->prepare('update tbl_franquias set visualizado = 1 where id = :id');
    $stmt->bindValue(':id', $id, PDO::PARAM_INT);
    $stmt->execute();

    // Dados
    $_detail_nome           = $data['nome'];
    $_detail_email          = $data['email'];
    $_detail_telefone       = $data['telefone'];
    $_detail_servico       = $data['servico'];
    $_detail_cidade        = $data['cidade'];
    $_detail_mensagem       = nl2br($data['mensagem']);
    $_detail_ip             = $data['ip'];
    $_detail_created_at     = strftime('%d/%m/%Y às %H:%M', strtotime($data['created_at']));

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <base href="<?php echo $_vars['url_base']; ?>/" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo $_vars['title']; ?></title>
	
    <link rel="stylesheet" type="text/css" href="style/reset.css" /> 
    <link rel="stylesheet" type="text/css" href="style/root.css" /> 
    <link rel="stylesheet" type="text/css" href="style/grid.css" /> 
    <link rel="stylesheet" type="text/css" href="style/typography.css" /> 
    <link rel="stylesheet" type="text/css" href="style/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="style/jquery-plugin-base.css" />
    
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.tipsy.js"></script>
    <script type="text/javascript" src="js/jquery-settings.js"></script>
    <script type="text/javascript" src="<?php echo $_module['url_base'] ?>/js/module.js"></script>
    
</head>
<body>
<div class="wrapper">

	<?php include sprintf('%s/includes/header.php', $_vars['path_admin']); ?>
    
    <!-- START MAIN -->
    <div id="main">

        <?php include sprintf('%s/includes/sidebar.php', $_vars['path_admin']); ?>
                
        <!-- START PAGE -->
        <div id="page">
            	
            <?php include __DIR__ . '/includes/header.php'; ?>
                
            <!-- START CONTENT -->
            <div class="content">

                <!-- START SIMPLE FORM -->
                <div class="simplebox grid740">
                    <div class="titleh">
                        <h3>Detalhes do contato</h3>
                        <div class="shortcuts-icons">
                            <a href="javascript:history.back();" style="padding:5px;"><img src="img/icons/sidemenu/arrow_left.png" alt=""></a>
                        </div>
                    </div>
                    <div class="body">
                    
                      <form id="form" name="form" method="post" action="<?php echo $_module['url_base'] ?>/action.php" enctype="multipart/form-data">
                      
                        <div class="st-form-line">  
                            <strong class="st-labeltext">Nome</strong> 
                            <span class="st-labeltext st-full"><?php echo $_detail_nome; ?></span> 
                            <div class="clear"></div>
                        </div>

                        <div class="st-form-line">  
                            <strong class="st-labeltext">E-mail</strong> 
                            <span class="st-labeltext st-full"><?php echo $_detail_email; ?></span> 
                            <div class="clear"></div>
                        </div>

                        <div class="st-form-line">  
                            <strong class="st-labeltext">Telefone</strong> 
                            <span class="st-labeltext st-full"><?php echo $_detail_telefone; ?></span> 
                            <div class="clear"></div>
                        </div>
                        <div class="st-form-line">  
                            <strong class="st-labeltext">Serviço</strong> 
                            <span class="st-labeltext st-full"><?php echo $_detail_servico; ?></span> 
                            <div class="clear"></div>
                        </div>

                        <div class="st-form-line">  
                            <strong class="st-labeltext">Cidade</strong> 
                            <span class="st-labeltext st-full"><?php echo $_detail_cidade; ?></span> 
                            <div class="clear"></div>
                        </div>

                        <?php if($_detail_mensagem): ?>
                            <div class="st-form-line">  
                                <strong class="st-labeltext">Mensagem</strong> 
                                <span class="st-labeltext st-full"><?php echo $_detail_mensagem; ?></span> 
                                <div class="clear"></div>
                            </div>
                        <?php endif; ?>

                        <div class="st-form-line">  
                            <strong class="st-labeltext">IP</strong> 
                            <span class="st-labeltext st-full"><?php echo $_detail_ip; ?></span> 
                            <div class="clear"></div>
                        </div>

                        <div class="st-form-line">  
                            <strong class="st-labeltext">Enviado em</strong> 
                            <span class="st-labeltext st-full"><?php echo $_detail_created_at; ?></span> 
                            <div class="clear"></div>
                        </div>

                        <div class="button-box">
                            <a class="button-gray" href="<?php echo $_module['url_base']; ?>">Voltar</a>
                        </div>
                        
                      </form>
                      
                    </div>
                </div>
                <!-- END SIMPLE FORM -->
                
                <div class="clear"></div>

            </div>
            <!-- END CONTENT -->
            
        </div>
        <!-- END PAGE -->

        <div class="clear"></div>

    </div>
    <!-- END MAIN -->
   
	<?php include sprintf('%s/includes/footer.php', $_vars['path_admin']); ?>

</div>
</body>
</html>