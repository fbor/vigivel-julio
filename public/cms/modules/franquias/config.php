<?php

	require_once __DIR__ . '/../../bootstrap.php';

	$_module = array(
		'menu_slug'     => 'franquias',
		'module_slug'   => 'franquias',
		'url_base' 		=> sprintf('%s/modules/franquias', $_vars['url_base']),
		'path_files'	=> sprintf('%s/franquias', $_vars['path_files']),
		'url_files'		=> sprintf('%s/franquias', $_vars['url_files']),
		'permissoes'    => [
			'geral' => [
				'admin',
			],
		],
	);