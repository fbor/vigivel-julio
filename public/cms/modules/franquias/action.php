<?php

	require_once __DIR__ . '/config.php';

	$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : null;

	switch ($action) {
		case 'destroy':

			/**
			 * Values
			 */
			$id = isset($_REQUEST['id']) && is_numeric($_REQUEST['id']) ? $_REQUEST['id'] : null;

			/**
			 * Destroy register
			 */
			$stmt = $pdo->prepare('delete from tbl_franquias where id = :id');
			$stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();

            set_flash_message('message', 'Registro removido.');
            redirect($_module['url_base']);

		break;
		
		default:

			exit;

		break;
	}