<?php

    require __DIR__ . '/config.php';

    check_logged();
    check_permissions($_SESSION['user']['nivel_permissao'], $_module['permissoes']['geral']);

    $menu_active = $_module['menu_slug'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <base href="<?php echo $_vars['url_base']; ?>/" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo $_vars['title']; ?></title>
	
    <link rel="stylesheet" type="text/css" href="style/reset.css" /> 
    <link rel="stylesheet" type="text/css" href="style/root.css" /> 
    <link rel="stylesheet" type="text/css" href="style/grid.css" /> 
    <link rel="stylesheet" type="text/css" href="style/typography.css" /> 
    <link rel="stylesheet" type="text/css" href="style/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="style/jquery-plugin-base.css" />
    <link rel="stylesheet" type="text/css" href="plugins/sweetalert/dist/sweetalert.css" />

	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.tipsy.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="plugins/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="js/jquery-settings.js"></script>
    <script type="text/javascript" src="<?php echo $_module['url_base'] ?>/js/module.js"></script>

</head>
<body>
<div class="wrapper">

	<?php include sprintf('%s/includes/header.php', $_vars['path_admin']); ?>
    
    <!-- START MAIN -->
    <div id="main">

        <?php include sprintf('%s/includes/sidebar.php', $_vars['path_admin']); ?>
                
        <!-- START PAGE -->
        <div id="page">
            	
            <?php include __DIR__ . '/includes/header.php'; ?>
                
            <!-- START CONTENT -->
            <div class="content">

                <!-- START TABLE -->
                <div class="simplebox grid740">

                    <?php 
                        $flash_message = get_flash_message('message');
                        if( $flash_message ):
                    ?>
                        <div class="albox succesbox">
                            <b>Sucesso!</b> <?php echo $flash_message; ?>
                        </div>
                    <?php endif; ?>

                    <div class="titleh">
                        <h3>Lista de franquias</h3>
                    </div>

                    <table cellpadding="0" cellspacing="0" border="0" class="display data-table" id="list-rows">
                    
                        <thead>
                            <tr>
                                <th width="1">ID</th>
                                <th>Nome</th>
                                <th>E-mail</th>
                                <th width="1">Horário</th>
                                <th width="1">Visualizado</th>
                                <th width="1">Ações</th>
                            </tr>
                        </thead>
                        
                        <tbody>

                            <?php 

                                $stmt = $pdo->prepare('select id, nome, email, visualizado, created_at from tbl_franquias order by id desc');
                                $stmt->execute();

                                foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $data):

                            ?>
                        
                                <tr class="gradeA">
                                    <td class="center"><?php echo $data['id']; ?></td>
                                    <td><?php echo $data['nome']; ?></td>
                                    <td><?php echo $data['email']; ?></td>
                                    <td align="center" nowrap><?php echo strftime('%d/%m/%Y às %H:%M', strtotime($data['created_at'])); ?></td>
                                    <td align="center">
                                        <?php echo (bool)$data['visualizado'] ? '<strong class="hg-green">Sim</strong>' : '<strong class="hg-red">Não</strong>'; ?>
                                    </td>
                                    <td class="center"> 
                                        <a href="<?php echo $_module['url_base']; ?>/details.php?id=<?php echo $data['id']; ?>" class="tips" title="Visualizar"><img src="img/icons/sidemenu/magnify.png"></a>
                                        <a href="<?php echo $_module['url_base']; ?>/action.php?action=destroy&id=<?php echo $data['id']; ?>" class="delete-row tips" title="Excluir"><img src="img/icons/sidemenu/trash.png"></a>
                                    </td>
                                </tr>

                            <?php endforeach; ?>

                        </tbody>
                    </table>
                    
                </div>
                <!-- END TABLE -->
                
                <div class="clear"></div>

            </div>
            <!-- END CONTENT -->
            
        </div>
        <!-- END PAGE -->

        <div class="clear"></div>

    </div>
    <!-- END MAIN -->
   
	<?php include sprintf('%s/includes/footer.php', $_vars['path_admin']); ?>

</div>
</body>
</html>