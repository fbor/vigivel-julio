<?php

	require_once __DIR__ . '/../../bootstrap.php';

	$_module = array(
		'menu_slug'     => 'contatos',
		'module_slug'   => 'contatos',
		'url_base' 		=> sprintf('%s/modules/contatos', $_vars['url_base']),
		'path_files'	=> sprintf('%s/contatos', $_vars['path_files']),
		'url_files'		=> sprintf('%s/contatos', $_vars['url_files']),
		'permissoes'    => [
			'geral' => [
				'admin',
			],
		],
	);