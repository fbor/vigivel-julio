<?php

	require_once __DIR__ . '/../../bootstrap.php';

	$_module = array(
		'menu_slug'   	=> 'produtos',
		'module_slug'   => 'produtos',
		'path_files'	=> sprintf('%s/produtos', $_vars['path_files']),
		'url_files'		=> sprintf('%s/produtos', $_vars['url_files']),
		'url_base' 		=> sprintf('%s/modules/produtos', $_vars['url_base']),
		'max_images'    => 1,
	);
