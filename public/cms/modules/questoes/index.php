<?php

    require __DIR__ . '/config.php';

    check_logged();

    $menu_active = $_module['menu_slug'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <base href="<?php echo $_vars['url_base']; ?>/" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo $_vars['title']; ?></title>

    <!-- Reset -->
    <link rel="stylesheet" type="text/css" href="style/reset.css" />
    <!-- Main Style File -->
    <link rel="stylesheet" type="text/css" href="style/root.css" />
    <!-- Grid Styles -->
    <link rel="stylesheet" type="text/css" href="style/grid.css" />
    <!-- Typography Elements -->
    <link rel="stylesheet" type="text/css" href="style/typography.css" />
    <!-- Jquery UI -->
    <link rel="stylesheet" type="text/css" href="style/jquery-ui.css" />
    <!-- Jquery Plugin Css Files Base -->
    <link rel="stylesheet" type="text/css" href="style/jquery-plugin-base.css" />

    <link rel="stylesheet" type="text/css" href="plugins/sweetalert/dist/sweetalert.css" />

    <!--[if IE 7]>	  <link rel="stylesheet" type="text/css" href="style/ie7-style.css" />	<![endif]-->

    <!-- jquery base -->
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.11.custom.min.js"></script>
    <!-- jquery plugins settings -->
	<script type="text/javascript" src="js/jquery-settings.js"></script>
    <!-- toggle -->
	<script type="text/javascript" src="js/toogle.js"></script>
    <!-- tipsy -->
	<script type="text/javascript" src="js/jquery.tipsy.js"></script>
    <!-- uniform -->
	<script type="text/javascript" src="js/jquery.uniform.min.js"></script>
    <!-- Jwysiwyg editor -->
	<script type="text/javascript" src="js/jquery.wysiwyg.js"></script>
    <!-- table shorter -->
	<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
    <!-- raphael base and raphael charts -->
	<script type="text/javascript" src="js/raphael.js"></script>
	<script type="text/javascript" src="js/analytics.js"></script>
	<script type="text/javascript" src="js/popup.js"></script>
    <!-- fullcalendar -->
	<script type="text/javascript" src="js/fullcalendar.min.js"></script>
    <!-- prettyPhoto -->
	<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
    <!-- Jquery.UI Base -->
	<script type="text/javascript" src="js/jquery.ui.core.js"></script>
	<script type="text/javascript" src="js/jquery.ui.mouse.js"></script>
	<script type="text/javascript" src="js/jquery.ui.widget.js"></script>
    <!-- Slider -->
	<script type="text/javascript" src="js/jquery.ui.slider.js"></script>
    <!-- Date Picker -->
	<script type="text/javascript" src="js/jquery.ui.datepicker.js"></script>
    <!-- Tabs -->
	<script type="text/javascript" src="js/jquery.ui.tabs.js"></script>
    <!-- Accordion -->
	<script type="text/javascript" src="js/jquery.ui.accordion.js"></script>
    <!-- Google Js Api / Chart and others -->
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <!-- Date Tables -->
    <script type="text/javascript" src="js/jquery.dataTables.js"></script>

    <script type="text/javascript" src="plugins/sweetalert/dist/sweetalert.min.js"></script>

    <script type="text/javascript" src="<?php echo $_module['url_base'] ?>/js/module.js"></script>


</head>
<body>
<div class="wrapper">

	<?php include sprintf('%s/includes/header.php', $_vars['path_admin']); ?>

    <!-- START MAIN -->
    <div id="main">

        <?php include sprintf('%s/includes/sidebar.php', $_vars['path_admin']); ?>

        <!-- START PAGE -->
        <div id="page">

            <?php include __DIR__ . '/includes/header.php'; ?>

            <!-- START CONTENT -->
            <div class="content">

                <!-- START TABLE -->
                <div class="simplebox grid740">

                    <div class="titleh">
                        <h3>Lista de Questões</h3>
                        <div class="shortcuts-icons">
                            <a class="shortcut tips" href="<?php echo $_module['url_base']; ?>/form.php" title="Adicionar novo"><img src="img/icons/shortcut/plus.png" width="25" height="25" alt="icon" /></a>
                        </div>
                    </div>

                    <!-- Start Data Tables Initialisation code -->
                    <script type="text/javascript" charset="utf-8">
                        $(document).ready(function() {
                                                    oTable = $('#example').dataTable({
                                                    "bJQueryUI": true,
                                                    "sPaginationType": "full_numbers",
                                    "aaSorting": [[ 0, "desc" ]]
                                                    });
                                                } );
                                            </script>
                    <!-- End Data Tables Initialisation code -->


                    <table cellpadding="0" cellspacing="0" border="0" class="display data-table" id="example">

                        <thead>
                            <tr>
                                <th width="1">ID</th>
                                <th>Pergunta</th>
                                <th width="1">Status</th>
                                <th width="1">Ações</th>
                            </tr>
                        </thead>

                        <tbody>

                            <?php

                                $stmt = $pdo->prepare('select id, titulo, ativo from tbl_questions order by id desc');
                                $stmt->execute();

                                foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $data):

                            ?>

                                <tr class="gradeA">
                                    <td class="center"><?php echo $data['id']; ?></td>
                                    <td><?php echo $data['titulo']; ?></td>
                                    <td align="center">
                                        <?php echo (bool)$data['ativo'] ? '<strong class="hg-green">ATIVO</strong>' : '<strong class="hg-red">INATIVO</strong>'; ?>
                                    </td>
                                    <td class="center">
                                        <a href="<?php echo $_module['url_base']; ?>/form.php?action=edit&id=<?php echo $data['id']; ?>" class="tips" title="Editar"><img src="img/icons/sidemenu/file_edit.png"></a>
                                        <a href="<?php echo $_module['url_base']; ?>/action.php?action=destroy&id=<?php echo $data['id']; ?>" class="delete-row tips" title="Excluir"><img src="img/icons/sidemenu/trash.png"></a>
                                    </td>
                                </tr>

                            <?php endforeach; ?>

                        </tbody>
                    </table>

                </div>
                <!-- END TABLE -->

                <div class="clear"></div>

            </div>
            <!-- END CONTENT -->

        </div>
        <!-- END PAGE -->

        <div class="clear"></div>

    </div>
    <!-- END MAIN -->

	<?php include sprintf('%s/includes/footer.php', $_vars['path_admin']); ?>

</div>
</body>
</html>