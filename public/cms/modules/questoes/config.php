<?php

	require_once __DIR__ . '/../../bootstrap.php';

	$_module = array(
		'menu_slug'   	=> 'questoes',
		'module_slug'   => 'questoes',
		'path_files'	=> sprintf('%s/questoes', $_vars['path_files']),
		'url_files'		=> sprintf('%s/questoes', $_vars['url_files']),
		'url_base' 		=> sprintf('%s/modules/questoes', $_vars['url_base']),
		'max_images'    => 1,
	);

	$imageManager = new Intervention\Image\ImageManager(['driver' => 'imagick']);