<?php

	require_once __DIR__ . '/../../bootstrap.php';

	$_module = array(
		'menu_slug'   	=> 'portal_clientes',
		'module_slug'   => 'portal_clientes',
		'path_files'	=> sprintf('%s/portal_clientes', $_vars['path_files']),
		'url_files'		=> sprintf('%s/portal_clientes', $_vars['url_files']),
		'url_base' 		=> sprintf('%s/modules/portal_clientes', $_vars['url_base']),
		'max_images'    => 1,
	);

	$imageManager = new Intervention\Image\ImageManager(['driver' => 'imagick']);