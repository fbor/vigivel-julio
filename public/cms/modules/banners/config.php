<?php

	require_once __DIR__ . '/../../bootstrap.php';

	$_module = array(
		'menu_slug'   	=> 'banners',
		'module_slug'   => 'banners',
		'path_files'	=> sprintf('%s/banners', $_vars['path_files']),
		'url_files'		=> sprintf('%s/banners', $_vars['url_files']),
		'url_base' 		=> sprintf('%s/modules/banners', $_vars['url_base']),
		'max_images'    => 1,
	);