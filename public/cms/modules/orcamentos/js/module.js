jQuery(function($){

	var list_rows  = $('#list-rows');

    if( list_rows.length ){

        /**
         * List rows
         */
        list_rows.dataTable({
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "aaSorting": [[ 0, "desc" ]]
        });

        /**
         * Trash
         */
        $('.delete-row').on('click', function(){
            var _self = $(this);

            swal({   
                title: "Atenção!",   
                text: "Deseja realmente excluir este registro?",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Sim, quero excluir!",   
                cancelButtonText: "Cancelar",   
                closeOnConfirm: false
            }, function(){   
                location.href = _self.attr('href');
            });

            return false;
        });

    };

});