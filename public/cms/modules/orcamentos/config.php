<?php

	require_once __DIR__ . '/../../bootstrap.php';

	$_module = array(
		'menu_slug'     => 'orcamentos',
		'module_slug'   => 'orcamentos',
		'url_base' 		=> sprintf('%s/modules/orcamentos', $_vars['url_base']),
		'path_files'	=> sprintf('%s/orcamentos', $_vars['path_files']),
		'url_files'		=> sprintf('%s/orcamentos', $_vars['url_files']),
		'permissoes'    => [
			'geral' => [
				'admin',
			],
		],
	);