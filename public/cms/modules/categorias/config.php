<?php

	require_once __DIR__ . '/../../bootstrap.php';

	$_module = array(
		'menu_slug'   	=> 'categorias',
		'module_slug'   => 'categorias',
		'path_files'	=> sprintf('%s/categorias', $_vars['path_files']),
		'url_files'		=> sprintf('%s/categorias', $_vars['url_files']),
		'url_base' 		=> sprintf('%s/modules/categorias', $_vars['url_base']),
		'max_images'    => 1,
	);