<?php

	require_once __DIR__ . '/../../bootstrap.php';

	$_module = array(
		'menu_slug'   	=> 'empresas',
		'module_slug'   => 'empresas',
		'path_files'	=> sprintf('%s/empresas', $_vars['path_files']),
		'url_files'		=> sprintf('%s/empresas', $_vars['url_files']),
		'url_base' 		=> sprintf('%s/modules/empresas', $_vars['url_base']),
		'max_images'    => 1,
	);

	$imageManager = new Intervention\Image\ImageManager(['driver' => 'imagick']);