<?php

return [
    [
    	'permissions' 	=> null,
        'label' 		=> 'Dashboard',
        'icon'  		=> [
            'src'    => 'img/icons/button/home.png',
            'width'  => 18,
            'height' => 18,
        ],
        'menu_slug'   => 'dashboard',
        'module_link' => $_pages['dashboard'],
    ],
    [
        'permissions'   => ['admin'],
        'label'         => 'Clientes',
        'icon'          => [
            'src'    => 'img/icons/button/group.png',
            'width'  => 18,
            'height' => 18,
        ],
        'menu_slug'   => 'clientes',
        'module_link' => $_pages['clientes'],
    ],
    [
        'permissions'   => ['admin'],
        'label'         => 'Produtos',
        'icon'          => [
            'src'    => 'img/icons/button/group.png',
            'width'  => 18,
            'height' => 18,
        ],
        'menu_slug'   => 'produtos',
        'module_link' => $_pages['produtos'],
    ],
    [
        'permissions'   => ['admin'],
        'label'         => 'Empresas',
        'icon'          => [
            'src'    => 'img/icons/button/suitcase.png',
            'width'  => 18,
            'height' => 18,
        ],
        'menu_slug'   => 'empresas',
        'module_link' => $_pages['empresas'],
    ],
    [
        'permissions'   => ['admin'],
        'label'         => 'Cadastro Franquias',
        'icon'          => [
            'src'    => 'img/icons/button/suitcase.png',
            'width'  => 18,
            'height' => 18,
        ],
        'menu_slug'   => 'cadastro_franquias',
        'module_link' => $_pages['cadastro_franquias'],
    ],
    [
        'permissions'   => ['admin', 'rh'],
        'label'         => 'Curriculos',
        'icon'          => [
            'src'    => 'img/icons/button/folder.png',
            'width'  => 18,
            'height' => 18,
        ],
        'menu_slug'   => 'curriculos',
        'module_link' => $_pages['curriculos'],
    ],
    [
        'permissions'   => ['admin'],
        'label'         => 'Questões',
        'icon'          => [
            'src'    => 'img/icons/button/folder.png',
            'width'  => 18,
            'height' => 18,
        ],
        'menu_slug'   => 'questoes',
        'module_link' => $_pages['questoes'],
    ],
    [
        'permissions'   => ['admin'],
        'label'         => 'Respostas',
        'icon'          => [
            'src'    => 'img/icons/button/folder.png',
            'width'  => 18,
            'height' => 18,
        ],
        'menu_slug'   => 'respostas',
        'module_link' => $_pages['respostas'],
    ],
    [
        'permissions'   => ['admin'],
        'label'         => 'Serviços p/ Clientes',
        'icon'          => [
            'src'    => 'img/icons/button/folder.png',
            'width'  => 18,
            'height' => 18,
        ],
        'menu_slug'   => 'ja_cliente',
        'module_link' => $_pages['ja_cliente'],
    ],
    [
        'permissions'   => ['admin'],
        'label'         => 'Banners',
        'icon'          => [
            'src'    => 'img/icons/button/slide.png',
            'width'  => 18,
            'height' => 18,
        ],
        'menu_slug'   => 'banners',
        'module_link' => $_pages['banners'],
    ],
    [
        'permissions'   => ['admin'],
        'label'         => 'Banners Laterais',
        'icon'          => [
            'src'    => 'img/icons/button/slide.png',
            'width'  => 18,
            'height' => 18,
        ],
        'menu_slug'   => 'side_banners',
        'module_link' => $_pages['side_banners'],
    ],
    [
        'permissions'   => ['admin'],
        'label'         => 'Categorias',
        'icon'          => [
            'src'    => 'img/icons/button/folder.png',
            'width'  => 18,
            'height' => 18,
        ],
        'menu_slug'   => 'categorias',
        'module_link' => $_pages['categorias'],
    ],
    [
        'permissions'   => ['admin'],
        'label'         => 'Serviços',
        'icon'          => [
            'src'    => 'img/icons/button/finish-flag.png',
            'width'  => 18,
            'height' => 18,
        ],
        'menu_slug'   => 'servicos',
        'module_link' => $_pages['servicos'],
    ],
    [
        'permissions'   => ['admin'],
        'label'         => 'Videos',
        'icon'          => [
            'src'    => 'img/icons/button/vimeo.png',
            'width'  => 18,
            'height' => 18,
        ],
        'menu_slug'   => 'videos',
        'module_link' => $_pages['videos'],
    ],
    [
        'permissions'   => ['admin'],
        'label'         => 'Orçamentos',
        'icon'          => [
            'src'    => 'img/icons/button/mail.png',
            'width'  => 18,
            'height' => 18,
        ],
        'menu_slug'   => 'orcamentos',
        'module_link' => $_pages['orcamentos'],
    ],
    [
        'permissions'   => ['admin'],
        'label'         => 'Franquias',
        'icon'          => [
            'src'    => 'img/icons/button/mail.png',
            'width'  => 18,
            'height' => 18,
        ],
        'menu_slug'   => 'franquias',
        'module_link' => $_pages['franquias'],
    ],
    [
    	'permissions' 	=> ['admin'],
        'label' 		=> 'Contatos',
        'icon'  		=> [
            'src'    => 'img/icons/button/mail.png',
            'width'  => 18,
            'height' => 18,
        ],
        'menu_slug'   => 'contatos',
        'module_link' => $_pages['contatos'],
    ],
    [
    	'permissions' 	=> ['admin'],
        'label' 		=> 'Usuários',
        'icon'  		=> [
            'src'    => 'img/icons/button/users.png',
            'width'  => 18,
            'height' => 18,
        ],
        'menu_slug'   => 'usuarios',
        'module_link' => $_pages['usuarios'],
    ],
];
