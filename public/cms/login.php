<?php

    require 'bootstrap.php';
    require 'connection.php';

    if( isset($_SESSION['logged']) && $_SESSION['logged'] === true ){
        header('Location: index.php');
        exit;
    }

    if( $_SERVER['REQUEST_METHOD'] == 'POST' ){
        $username = isset($_POST['username']) ? trim(addslashes($_POST['username'])) : null;
        $password = isset($_POST['password']) ? md5($_POST['password']) : null;

        $stmt = $pdo->prepare('select id, nome, username, nivel_permissao from tbl_users where username = :username and password = :password and ativo = 1');
        $stmt->bindParam(':username', $username, PDO::PARAM_STR);
        $stmt->bindParam(':password', $password, PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if( $user ){
            // Imagem do perfil
            $stmt_imagem = $pdo->prepare('select filename from tbl_files where type = "photos" and module = "usuarios" and relationship = :id limit 1');
            $stmt_imagem->bindValue(':id', $user['id'], PDO::PARAM_INT);
            $stmt_imagem->execute();

            $imagem = $stmt_imagem->fetch(PDO::FETCH_ASSOC);

            if( $imagem ){
                $thumb = sprintf('%s/usuarios/%d/thumb_%s', $_vars['url_files'], $user['id'], $imagem['filename']);
            } else {
                $thumb = sprintf('%s/img/user.png', $_vars['url_base']);
            }

            $_SESSION['logged'] = true;
            $_SESSION['user']   = array_merge($user, array('thumb' => $thumb));

            header('Location: index.php');
            exit;
        }

        $error_message = 'Usuário e/ou senha inválido(s)';
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $_vars['title']; ?></title>
	
    <!-- Reset -->
    <link rel="stylesheet" type="text/css" href="style/reset.css" /> 
    <!-- Main Style File -->
    <link rel="stylesheet" type="text/css" href="style/root.css" /> 
    <!-- Grid Styles -->
    <link rel="stylesheet" type="text/css" href="style/grid.css" /> 
    <!-- Typography Elements -->
    <link rel="stylesheet" type="text/css" href="style/typography.css" /> 
    <!-- Jquery UI -->
    <link rel="stylesheet" type="text/css" href="style/jquery-ui.css" />
    <!-- Jquery Plugin Css Files Base -->
    <link rel="stylesheet" type="text/css" href="style/jquery-plugin-base.css" />
    
    <!--[if IE 7]><link rel="stylesheet" type="text/css" href="style/ie7-style.css" />	<![endif]-->

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript">
    $(function(){
        $(".login-input-user")
            .focus(function(){
                $(this).attr('class','login-input-user-active ');
            })
            .blur(function(){
                $(this).attr('class','login-input-user');
            });

        $(".login-input-pass")
            .focus(function(){
                $(this).attr('class','login-input-pass-active ');
            })
            .blur(function(){
                $(this).attr('class','login-input-pass');
            });
    });
    </script>
	
</head>
<body>
	
    <div class="loginform">
    	<div class="title"> <img src="img/goutnix.png" width="130" /></div>
        <div class="body">

            <?php if(isset($error_message)): ?>
                <div class="albox errorbox">
                    <b>Erro:</b> <?php echo $error_message; ?>
                    <a href="#" class="close tips" title="close">close</a>
                </div>
            <?php endif; ?>

       	  <form id="form1" name="form1" method="post">
          	<label class="log-lab">Usuário</label>
            <input name="username" type="text" class="login-input-user" id="textfield" />
          	<label class="log-lab">Senha</label>
            <input name="password" type="password" class="login-input-pass" id="textfield" />
            <input type="submit" name="button" id="button" value="Login" class="button"/>
       	  </form>
        </div>
    </div>

</div>
</body>
</html>