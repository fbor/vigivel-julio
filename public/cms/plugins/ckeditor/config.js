/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */
CKEDITOR.editorConfig = function( config ) {
	config.toolbar = [
        ['Bold','Italic','Underline', 'Strike', '-', 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-', 'NumberedList','BulletedList', 'Outdent','Indent', 'Blockquote', '-', 'Link', 'Unlink', '-', 'TextColor','BGColor', 'Print', '-', 'Image','Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'Iframe','-','Cut','Copy','Paste', 'PasteText', 'PasteFromWord','Find','Replace', 'Scayt', '-', 'Undo','Redo', '-', 'Source'],
        ['Styles','Format','Font','FontSize', 'Maximize']
    ];

    config.language = 'pt-br';
	config.enterMode = CKEDITOR.ENTER_BR;

	config.filebrowserBrowseUrl 	 = 'plugins/ckfinder/ckfinder.html';
    config.filebrowserImageBrowseUrl = 'plugins/ckfinder/ckfinder.html?type=Images';
    config.filebrowserUploadUrl 	 = '../../plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
    config.filebrowserImageUploadUrl = '../../plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';

    /*
    filebrowserBrowseUrl : '<?= APP_ROOT; ?>/apps/ckfinder/ckfinder.html',
+            filebrowserImageBrowseUrl : '<?= APP_ROOT; ?>/apps/ckfinder/ckfinder.html?type=Images',
+            filebrowserFlashBrowseUrl : '<?= APP_ROOT; ?>/apps/ckfinder/ckfinder.html?type=Flash',
+            filebrowserUploadUrl : '<?= APP_ROOT; ?>/apps/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&currentFolder=/arquivos/diversos/',
+            filebrowserImageUploadUrl : '<?= APP_ROOT; ?>/apps/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images&currentFolder=/cars/',
+            filebrowserFlashUploadUrl : '<?= APP_ROOT; ?>/apps/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
     */
};