<?php

ini_set('display_errors', true);
error_reporting(-1);

session_start();

date_default_timezone_set('America/Sao_Paulo');

header('Content-type: text/html; charset=UTF-8');

/* Include path */
set_include_path(implode(PATH_SEPARATOR, array(
    __DIR__ . '/src',
    get_include_path()
)));

/* PEAR autoloader */
spl_autoload_register(function ($className)
{
    $filename = strtr($className, '\\', DIRECTORY_SEPARATOR) . '.php';
    foreach (explode(PATH_SEPARATOR, get_include_path()) as $path) {
        $path .= DIRECTORY_SEPARATOR . $filename;
        if (is_file($path)) {
            require_once $path;
            return true;
        }
    }
    return false;
});

$_environment = file_exists(__DIR__ . '/../../.env') ? 'local' : 'production';

switch ($_environment) {
    case 'local':
        // Banco de dados
        // $dbhost = 'localhost';
        // $dbname = 'vigivel';
        // $dbuser = 'root';
        // $dbpass = 'root';


        $dbhost = 'mysql427.umbler.com';
        $dbname = 'vigivel';
        $dbuser = 'vigivel';
        $dbpass = 'xkV7x}lf_5O';

        // Paths
        $url_base         = 'https://' . $_SERVER['HTTP_HOST'] . '/cms';
        $url_files        = 'https://' . $_SERVER['HTTP_HOST'] . '/files';
        $ckfinder_baseUrl = '/cms/plugins/ckfinder/userfiles/';
        $ckfinder_baseDir = dirname($_SERVER['SCRIPT_FILENAME']) . '/plugins/ckfinder/userfiles/';
    break;

    case 'production':
        // Banco de dados
        $dbhost = 'mysql427.umbler.com';
        $dbname = 'vigivel';
        $dbuser = 'vigivel';
        $dbpass = 'xkV7x}lf_5O';

        // Paths
        $url_base         = 'http://' . $_SERVER['HTTP_HOST'] . '/cms';
        $url_files        = 'http://' . $_SERVER['HTTP_HOST'] . '/files';
        $ckfinder_baseUrl = '/cms/plugins/ckfinder/userfiles/';
        $ckfinder_baseDir = dirname($_SERVER['SCRIPT_FILENAME']) . '/cms/plugins/ckfinder/userfiles/';
    break;
}

$_vars = array(
	'title' 		    => 'Goutnix - Sistema de Gerenciamento',
	'url_base' 		    => $url_base,
    'url_files'         => $url_files,
    'path_admin'        => __DIR__,
    'path_files'        => __DIR__ . '/../files',
    'ckfinder_baseUrl'  => $ckfinder_baseUrl,
    'ckfinder_baseDir'  => $ckfinder_baseDir,
	'pagina_atual' 	    => 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
);

$_pages = array(
    'dashboard'          => sprintf('%s/', $_vars['url_base']),
    'clientes'           => sprintf('%s/modules/clientes', $_vars['url_base']),
    'produtos'           => sprintf('%s/modules/produtos', $_vars['url_base']),
    'empresas'           => sprintf('%s/modules/empresas', $_vars['url_base']),
    'cadastro_franquias' => sprintf('%s/modules/cadastro_franquias', $_vars['url_base']),
    'banners'            => sprintf('%s/modules/banners', $_vars['url_base']),
    'side_banners'       => sprintf('%s/modules/side_banners', $_vars['url_base']),
    'ja_cliente'         => sprintf('%s/modules/ja_cliente', $_vars['url_base']),
    'curriculos'         => sprintf('%s/modules/curriculos', $_vars['url_base']),
    'questoes'           => sprintf('%s/modules/questoes', $_vars['url_base']),
    'respostas'          => sprintf('%s/modules/respostas', $_vars['url_base']),
    'categorias'         => sprintf('%s/modules/categorias', $_vars['url_base']),
    'servicos'           => sprintf('%s/modules/servicos', $_vars['url_base']),
    'videos'             => sprintf('%s/modules/videos', $_vars['url_base']),
    'orcamentos'         => sprintf('%s/modules/orcamentos', $_vars['url_base']),
    'franquias'          => sprintf('%s/modules/franquias', $_vars['url_base']),
    'contatos'           => sprintf('%s/modules/contatos', $_vars['url_base']),
    'usuarios'           => sprintf('%s/modules/usuarios', $_vars['url_base']),
    'profile'            => sprintf('%s/modules/perfil', $_vars['url_base']),
    'logout'             => sprintf('%s/logout.php', $_vars['url_base']),
);

$_menu_sidebar = include __DIR__ . '/menu_sidebar.php';

$_niveis_usuario = [
    'admin' => 'Administrador',
    'rh' => 'Recursos Humanos',
];

require_once __DIR__ . '/functions.php';
require_once __DIR__ . '/connection.php';
