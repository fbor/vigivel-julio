<?php

namespace Paginator;

use Paginator\Zebra_Pagination\Zebra_Pagination;

class Paginator extends Zebra_Pagination
{
	public function __construct($url, $preserve_query_string = false)
	{
		parent::__construct();

		$this->base_url($url, $preserve_query_string);
	}

	public function friendlyUrl($friendlyUrl = true)
	{
		if( false === is_bool($friendlyUrl) )
			throw new \InvalidArgumentException("The value must be boolean");

		if( $friendlyUrl )
			$this->method('url');
	}

	public function alwaysShowNavigation($show = true)
    {
        $this->always_show_navigation($show);
    }
}