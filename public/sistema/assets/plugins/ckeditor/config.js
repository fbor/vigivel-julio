/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */
CKEDITOR.editorConfig = function( config ) {
	config.toolbar = [
        ['Bold','Italic','Underline', 'Strike', '-', 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-', 'NumberedList','BulletedList', 'Outdent','Indent', 'Blockquote', '-', 'Link', 'Unlink', '-', 'TextColor','BGColor', 'Print', '-', 'Image','Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'Iframe','-','Cut','Copy','Paste', 'PasteText', 'PasteFromWord','Find','Replace', 'Scayt', '-', 'Undo','Redo', '-', 'Source'],
        ['Styles','Format','Font','FontSize', 'Maximize']
    ];

    config.language = 'pt-br';
	config.enterMode = CKEDITOR.ENTER_BR;

	config.filebrowserBrowseUrl 	 = '/assets/plugins/ckfinder/ckfinder.html';
    config.filebrowserImageBrowseUrl = '/assets/plugins/ckfinder/ckfinder.html?type=Images';
    config.filebrowserUploadUrl 	 = '/assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
    config.filebrowserImageUploadUrl = '/assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
};