;(function ($, window, document, undefined){

	var _document 	= $( document ),
		_header 	= $( '.header-lib' ),
		_navbar 	= _header.find( 'select#menu-main' ),
		baseUrl     = $( 'meta[name="base_url"]' ).attr( 'content' ) || '';



	_navbar.on('change', function (event) {
		
		window.location = $(this).val();

	});





    /*
     * Mobile toggle item heading
     */
    ;(function ($, window, document, undefined) {

        _header.find( '.ui-action-bar' ).on('click', '.toggle', function (event) {
            event.preventDefault();
            
            var el = $(this),
                target = _header.find( '.' + el.data( 'target' ) );


            if ( !target.length )
                return;

            
            el.toggleClass( 'is-active' );
            target.toggleClass( 'show' );

            if ( el.data( 'target' ) === 'navigation' )
                target.find( 'selec#menu-main' ).trigger( 'click' ).focus();
            

        });

    } (jQuery, window, document));





	/*
	 * Crop image
	 */
	;(function (){

		var imageCrop = $( '.image-editor' ),
			imageSrc;


		if ( !imageCrop.length )
			return;

		imageSrc = imageCrop.data( 'src' ) || '';

		imageCrop.cropit({
         	imageState: {
        		src: imageSrc
        	},
        });


        imageCrop.on('change', 'input.cropit-image-input', function(event) {
        	
        	imageCrop.addClass( 'has-change' );

        });


        imageCrop.on('click', '.image-save', function (event) {

        	var imageData = $('.image-editor').cropit( 'export' );

        	$.ajax({
        		url: 'perfil/thumbnail', // baseUrl + 'perfil/thumbnail',
        		type: 'PUT',
        		dataType: 'json',
        		data: {
        			image: imageData
        		},
        	})
        	.done(function (response, status, xhr) {
        		
        		if ( xhr.status === 200 ){
        			imageCrop.removeClass( 'has-change' );

        			var message = response.message || "Sua foto de perfil alterada!";
        			toastr.success('', message, { 'closeButton': true });
        		}

        	})
        	.fail(function() {
        	
        		// console.log("error");
        	
        	});
        	

        });


	} ());

} (jQuery, window, document));

//# sourceMappingURL=basic.js.map
