$(document).ready(function(){
    $("#file").filer({
        limit: 1,
        changeInput: '<button type="button" class="btn btn-success waves-effect waves-light"><i class="zmdi zmdi-cloud-upload m-r-5"></i> <small>SELECIONE UMA FOTO</small> </button>',
        showThumbs: true,
        addMore: false,
        captions: {
            button: "Selecionar",
            feedback: "Selecione alguns arquivos para upload",
            feedback2: "arquivos selecionados",
            drop: "Arraste alguns arquivos aqui",
            removeConfirmation: "Tem certeza que deseja remover este arquivo?",
            errors: {
                filesLimit: "Somente {{fi-limit}} arquivos são aceitos para o upload.",
                filesType: "Tipo de arquivo não aceito.",
                filesSize: "{{fi-name}} é muito grande! Tamanho máximo é de {{fi-maxSize}} MB.",
                filesSizeAll: "Arquivos selecionados são muito grandes! Tamanho máximo é de {{fi-maxSize}} MB."
            }
        }
    });
});