# Guia de instalação

Esse é um guia para instalar o projeto padrão para sites da Goutnix.

## Pré-requisitos

- Git
- Docker
- Docker Compose

## Instalação do docker e docker-compose

Instalando o docker:

```shell
$ wget -qO- https://get.docker.com | sh
$ sudo usermod -aG docker $(whoami)
```

Instalando o docker-compose:

```shell
$ curl -L "https://github.com/docker/compose/releases/download/1.10.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
$ chmod +x /usr/local/bin/docker-compose
```

Depois das etapas acima, é necessário reiniciar a máquina, e assim que reiniciar testar com os seguintes comandos:

```shell
$ docker --version
$ docker-compose --version
```

## Instalação do projeto padrão 

> Deve-se tomar conta das portas que estão sendo usadas na máquina antes de continuar, como exemplo: 3306 do mysql, 80 do nginx/apache2, outros..., se esse for o caso, é só rodar o comando ```sudo service NOME_DO_SERVIÇO stop``` para parar os serviços necessários.

> Certifique-se que não existe nenhum container ativo de outros projetos rodando (```docker ps```), pois as portas podem estar sendo usadas, o que impossibilitará o funcionamento das etapas seguintes.

Clonando o projeto default: _(não esquecer de alterar onde diz NOMEDOPROJETO)_

```shell
$ git clone git@bitbucket.org:rozainski/goutnix-site-standard-l5.3.git NOMEDOPROJETO
$ cd NOMEDOPROJETO
$ git remote rm origin
$ sudo chmod -R 777 storage bootstrap/cache
```
## Configurando o projeto

Se o projeto atual está sendo ativo pela primeira vez, rode o seguinte comando: _(é necessário nesse momento alterar o arquivo ```docker-compose.yml``` antes de continuar, conforme explicação no início do próprio arquivo)_

```shell
$ docker-compose up -d --build
```

Se o projeto já foi rodado anteriormente, rode o seguinte comando: _(sem necessidade de alterar o arquivo ```docker-compose.yml```)_

```shell
$ docker-compose start
```

> Caso tenha alterações no arquivo ```docker-compose.yml``` no decorrer do projeto, é necessário tomar cuidado para não rodar o build novamente, pois isso irá sobrescrever alguns containers e volumes. Se isso ocorrer, deve-se tomar providências de como rodar o build individual de cada container que achar necessário, seguindo a documentação do docker-compose.

Para rodar os comandos necessários de pacotes instalados que fazem parte dos containers, vamos entrar no container ```workspace``` que contempla a maioria dos acessos a outros containers e pacotes:

```shell
$ docker-compose exec workspace /bin/bash
```

Instalando as dependências do composer:

```shell
$ composer install
```

Duplicar o arquivo ```.env.example``` para ```.env```, e configurar o arquivo conforme explicação no início do próprio arquivo. Depois de configurar o arquivo ```.env```, gerar uma chave:

```shell
$ php artisan key:generate
```

Uma mensagem similar a esta irá aparecer no console: **Application key [base64:gurUYZRvvXzskrMSw5Pp1hCdtjddbKkneoNSQNPRI+Q=] set successfully.**

Copie o conteúdo que está dentro dos colchetes, que é a nossa chave. Em seguida, abra o arquivo ```config/app.php``` e como segundo parâmetro do ```env('APP_KEY')```, adicione a chave em que foi copiada, ficando assim: ```env('APP_KEY', 'CHAVECOPIADA')``` entre áspas.

Nesse momento, acesse ```http://localhost``` para ver se o projeto está rodando sem problemas. Tudo funcionando, vamos rodar agora o comando para as migrations e seeds:

```shell
$ php artisan migrator --seed
```

Você também pode acessar ```http://localhost:8080``` que é o nosso phpmyadmin, com os dados:

> Servidor: mysql

> Usuário: (definido no arquivo .env ou docker-compose.yml) 

> Senha: (definido no arquivo .env ou docker-compose.yml)

Irá aparecer na esquerda os bancos de dados que já vem por padrão, mais o nosso banco do projeto.

Ao terminar de trabalhar no projeto, rodar o seguinte comando fora do container ```workspace```: _(que irá parar os containers)_

```shell
$ docker-compose stop
```

## Recursos disponíveis

Existem diversas outras configurações disponíveis no container ```workspace``` que podem ser habilitadas no arquivo ```docker-compose.yml``` como, por exemplo: ```xdebug```, ```ssh```, ```nodejs``` _(que já vem incluso o ```npm``` e ```gulp```)_, entre outros.

No ```php-fpm```, também temos a facilidade de estar ativando alguns recursos como, por exemplo: ```soap```, ```memcached```, ```opcache``` entre outros.