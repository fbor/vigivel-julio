<?php

namespace Goutnix\Support\Database;

use Illuminate\Database\Eloquent\Factory;
use Faker\Generator;

abstract class ModelFactory
{
	/**
	 * @var string
	 */
	protected $model;

	/**
	 * @var Factory
	 */
	protected $factory;

	/**
	 * @var Generator
	 */
	protected $faker;

	/**
	 * @return void
	 */
	public function __construct()
	{
		$this->factory = app()->make(Factory::class);
		$this->faker   = app()->make(Generator::class);
	}

	/**
	 * @return void
	 */
	public function define()
	{
		$this->factory->define($this->model, function(){
			return $this->fields();
		});
	}

	/**
	 * @return array
	 */
	abstract protected function fields();
}