<?php

namespace Goutnix\Domains\Curriculos;

use Illuminate\Database\Eloquent\Model;
use Goutnix\Domains\Files\File;

class Curriculo extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbl_curriculos';

    /**
     * Document
     */
    public function document()
    {
        return $this->hasOne('Goutnix\Domains\Files\File', 'relationship')
                    ->where('module', 'curriculos')
                    ->where('type', 'files');
    }
}
