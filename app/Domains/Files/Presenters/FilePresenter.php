<?php

namespace Goutnix\Domains\Files\Presenters;

use Laracasts\Presenter\Presenter;

class FilePresenter extends Presenter 
{
    public function pathToFile($size)
    {
        return sprintf('files/%s/%d/%s_%s', $this->module, $this->relationship, $size, $this->filename);
    }

    public function render($size)
    {
        $pathToFile = $this->pathToFile($size);

		return asset($pathToFile);
    }
}