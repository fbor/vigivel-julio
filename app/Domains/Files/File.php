<?php

namespace Goutnix\Domains\Files;

use Illuminate\Database\Eloquent\Model;
use Laracasts\Presenter\PresentableTrait;

class File extends Model
{
    use PresentableTrait;

    protected $presenter = \Goutnix\Domains\Files\Presenters\FilePresenter::class;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbl_files';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'filename',
        'type',
        'module',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'featured' => 'boolean',
    ];

    /**
     * Module
     */
    public function scopeModule($query, $module)
    {
        return $query->where('module', $module);
    }

    /**
     * Type files
     */
    public function scopeType($query, $type)
    {
        return $query->where('type', $type);
    }
}
