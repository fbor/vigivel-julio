<?php

namespace Goutnix\Domains\Franquias;

use Illuminate\Database\Eloquent\Model;

class Cities extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'gv_cidade';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
}
