<?php

namespace Goutnix\Domains\Franquias;

use Illuminate\Database\Eloquent\Model;

class States extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'gv_estado';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
}
