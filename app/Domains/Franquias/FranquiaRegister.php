<?php

namespace Goutnix\Domains\Franquias;

use Illuminate\Database\Eloquent\Model;


class FranquiaRegister extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbl_cad_franquias';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

}
