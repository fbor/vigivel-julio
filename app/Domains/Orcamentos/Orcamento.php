<?php

namespace Goutnix\Domains\Orcamentos;

use Illuminate\Database\Eloquent\Model;

class Orcamento extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbl_orcamentos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome',
        'email',
        'telefone',
        'cidade',
        'estado',
        'servicos',
        'mensagem',
        'ip',
        'visualizado',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'servicos' => 'array',
        'visualizado' => 'boolean',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        //
    ];
}
