<?php

namespace Goutnix\Domains\SideBanners;

use Illuminate\Database\Eloquent\Model;
use Goutnix\Domains\Files\File;

class SideBanner extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbl_side_banners';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'ativo' => 'boolean',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        //
    ];

    /**
     * Registros ativos
     */
    public function scopeActive($query)
    {
        return $query->where('ativo', true);
    }

    /**
     * Imagem capa
     */
    public function image()
    {
        return $this->hasOne(File::class, 'relationship')
            ->module('side_banners')
            ->type('photos');
    }
}
