<?php

namespace Goutnix\Domains\Users\Providers;

use Illuminate\Support\ServiceProvider;
use Migrator\MigratorTrait as HasMigrations;
use Goutnix\Domains\Users\Database\Migrations\CreateUsersTable;
use Goutnix\Domains\Users\Database\Migrations\CreatePasswordResetsTable;
use Goutnix\Domains\Users\Database\Factories\UserFactory;
use Goutnix\Domains\Users\Database\Seeders\UserSeeder;

class DomainServiceProvider extends ServiceProvider
{
	use HasMigrations;

	public function register()
	{
		$this->registerMigrations();
		$this->registerFactories();
		$this->registerSeeders();
	}

	public function registerMigrations()
	{
		$this->migrations([
			CreateUsersTable::class,
			CreatePasswordResetsTable::class,
		]);
	}

	public function registerFactories()
	{
		(new UserFactory)->define();
	}

	public function registerSeeders()
	{
		$this->seeders([
			UserSeeder::class,
		]);
	}
}