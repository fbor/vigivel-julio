<?php

namespace Goutnix\Domains\Services;

use Illuminate\Database\Eloquent\Model;
use Goutnix\Domains\Files\File;

class Service extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbl_servicos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'ativo' => 'boolean',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        //
    ];

    /**
     * Dynamic title slug
     */
    public function getSlugAttribute()
    {
        return str_slug($this->attributes['label']);
    }

    /**
     * Registros ativos
     */
    public function scopeActive($query)
    {
        return $query->where('ativo', true);
    }

    /**
     * Icon
     */
    public function icon()
    {
        return $this->hasOne(File::class, 'relationship')
            ->module('servicos')
            ->type('icon');
    }

    /**
     * Image
     */
    public function image()
    {
        return $this->hasOne(File::class, 'relationship')
            ->module('servicos')
            ->type('photo_fixed');
    }
}
