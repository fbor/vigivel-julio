<?php

namespace Goutnix\Applications\Web\Http\Controllers;

use Illuminate\Http\Request;
use Goutnix\Support\Http\Controllers\Controller;
use Goutnix\Domains\Answer\Answer;
use Goutnix\Domains\Questions\Question;
use DB;
use Mail;

class AnswerController extends Controller
{
    public function index()
    {

		$_items = [];

		$values = DB::table('tbl_questions')
			->join('tbl_respostas', 'tbl_respostas.id_pergunta', '=', 'tbl_questions.id')->where('tbl_questions.ordem', 1)->where('tbl_respostas.ativo', 1)->get();
			$i = 0;


			foreach ($values as $val) {

				if($val->tipo == 'open') {
					$_items = [
					'id' => $val->id,
					'text' => $val->titulo,
					'last' => ($val->last == 1) ? true : false,
					'answer' => [
						'placeholder' => $val->value,
            'type_input' => $val->tipo_text,
						'type' => $val->tipo,
						'related' => [
							'id_pergunta_relacionada' =>unserialize($val->id_pergunta_relacionada)
						],
            'product_related' => unserialize($val->id_produto)
					],
					'about' => $val->descricao,
					];
				} else {
				$_items = [
					'id' => $val->id,
					'text' => $val->titulo,
					'last' => ($val->last == 1) ? true : false ,
					'answer' => [
						'type' => $val->tipo,
            'type_input' => $val->tipo_text,
						'options' => [
							'value' => unserialize($val->respostas)
						],
						'related' => unserialize($val->id_pergunta_relacionada),
            'product_related' => unserialize($val->id_produto)
					],
					'about' => $val->descricao,
				];
			}
			}


    	return response()->json($_items);
	}

	public function getNext($id)
	{
		$_items = [];

		$values = DB::table('tbl_questions')
			->join('tbl_respostas', 'tbl_respostas.id_pergunta', '=', 'tbl_questions.id')->where('tbl_questions.id', $id)->where('tbl_respostas.ativo', 1)->get();

			foreach ($values as $val) {

				if($val->tipo == 'open') {
					$_items = [
					'id' => $val->id,
					'text' => $val->titulo,
					'last' => ($val->last == 1) ? true : false,
					'answer' => [
						'placeholder' => $val->value,
            'type_input' => $val->tipo_text,
						'type' => $val->tipo,
						'related' => unserialize($val->id_pergunta_relacionada),
            'product_related' => unserialize($val->id_produto)
					],
					'about' => $val->descricao,
					];
				}else {

				$_items = [
					'id' => $val->id,
					'text' => $val->titulo,
					'last' => ($val->last == 1) ? true : false,
					'answer' => [
            'type_input' => $val->tipo_text,
						'type' => $val->tipo,
						'options' => [
							'value' => unserialize($val->respostas)
						],
						'related' => unserialize($val->id_pergunta_relacionada),
            'product_related' => unserialize($val->id_produto)

					],
					'about' => $val->descricao,
				];
				}
			}

    	return response()->json($_items);
	}

	public function getProducts(Request $request)
	{

		$reqProdutos = json_decode($request->getContent(), true);
    //
    $comma_separated = implode(", ",$reqProdutos['products']);
    //
    $values = DB::select('select name, description from tbl_produtos where id in('.$comma_separated.')');

    return response()->json($values);

	}

  public function submitMail(Request $request) {
    $reqProdutos = json_decode($request->getContent(), true);

      Mail::send('emails.default', $reqProdutos, function ($mail) use ($reqProdutos) {
          $mail
              ->to(
                  config('app.emailContato'),
                  config('app.name')
              )
              ->replyTo($reqProdutos['email'], $reqProdutos['email'])
              ->subject('Orçamento vindo do site');


      });

      return response()->json(['success' => true, 'message' => 'Orçamento enviado com sucesso!']);
      }


}
