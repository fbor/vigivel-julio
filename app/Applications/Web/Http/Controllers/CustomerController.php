<?php

namespace Goutnix\Applications\Web\Http\Controllers;

use Illuminate\Http\Request;
use Goutnix\Support\Http\Controllers\Controller;
use Goutnix\Domains\Customer\Customer;

class CustomerController extends Controller
{
    public function index()
    {

    	$software = Customer::active()
			->orderByRaw('rand()')
			->where('segmentos', 1)
			->get();

		$gerencial = Customer::active()
			->orderByRaw('rand()')
			->where('segmentos', 2)
    		->get();


    	return view('dashboard-cliente')
    		->with('software', $software)
    		->with('gerencial', $gerencial);
    }
}