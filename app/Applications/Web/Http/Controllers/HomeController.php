<?php

namespace Goutnix\Applications\Web\Http\Controllers;

use Illuminate\Http\Request;
use Goutnix\Support\Http\Controllers\Controller;
use Goutnix\Domains\Services\Service;
use Goutnix\Domains\Clientes\Cliente;
use Goutnix\Domains\Banners\Banner;

class HomeController extends Controller
{
    public function index()
    {
        $servicos = Service::active()
            ->has('icon')
            ->with('icon')
            ->take(9)
            ->limit(9)
            ->inRandomOrder()
            ->get();

    	$clientes = Cliente::active()
    		->has('image')
    		->with('image')
    		->orderByRaw('rand()')
    		->get();

        $banners = Banner::active()
            ->has('image')
            ->with('image')
            ->get();

    	return view('home')
            ->with('servicos', $servicos)
            ->with('banners', $banners)
    		->with('clientes', $clientes);
    }
}