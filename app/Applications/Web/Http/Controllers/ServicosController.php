<?php

namespace Goutnix\Applications\Web\Http\Controllers;

use Illuminate\Http\Request;
use Goutnix\Support\Http\Controllers\Controller;
use Goutnix\Domains\Services\Service;
use Goutnix\Domains\Categorias\Categoria;
use Goutnix\Domains\SideBanners\SideBanner;
use Goutnix\Domains\JaCliente\JaCliente;
use Goutnix\Domains\Videos\Video;

class ServicosController extends Controller
{
    public function index()
    {
        $servicos = Service::active()
            ->has('icon')
            ->with('icon')
            ->get();


        $categorias = Categoria::active()
            ->has('image')
            ->with('image')
            ->orderBy('ordem')
            ->get();

    	return view('service')
            ->with('servicos', $servicos)
            ->with('categorias', $categorias);
    }

    public function show($id, $slug)
    {
        $servico = Service::where('id', $id)
            ->active()
            ->has('icon')
            ->with('icon')
            ->first();


        $sideBanners = SideBanner::active()
            ->where('id_servico', $id)
            ->has('image')
            ->with('image')
            ->orderBy('ordem')
            ->get();

        $jaCliente = JaCliente::where('id_servico', $id)->active()
            ->first();


        if(!$servico || $servico->slug != $slug)
            return back();

        $servicos = Service::where('id', '<>', $id)
            ->active()
            ->has('icon')
            ->with('icon')
            ->orderBy('ordem')
            ->get();

        $videos = Video::active()
        ->get();


        // $videos = [];

        // foreach( $videosAll as $key => $video) {

        //     if ( in_array($servico->id, unserialize($video->servicos)) ) {
        //         array_push($videos, $video);
        //     }
        // }

        return view('service-show')
            ->with('servico', $servico)
            ->with('sideBanners', $sideBanners)
            ->with('servicos', $servicos)
            ->with('videos', $videos)
            ->with('jaClientes', $jaCliente);
    }
}
