<?php

namespace Goutnix\Applications\Web\Http\Controllers;

use Illuminate\Http\Request;
use Goutnix\Support\Http\Controllers\Controller;
use Goutnix\Domains\Franquias\Franquia;
use Mail;
use DB;

class FranquiasController extends Controller
{
    public function index()
    {
    	return view('franquia');
    }

    public function show($slug)
    {
        return view('franquia-show', ['slug'=> $slug]);
    }

    public function sender()
    {
    	if( ! request()->has('name') || ! request()->has('email') ){
    		return response([
	    		'success' => false,
	    		'message' => 'Nome e e-mail são obrigatórios!',
	    	], 400);
    	}

    	$data = [
    		'assunto'  => 'Solicitação de franquia vinda do site ' . config('app.name'),
            'nome'     => request('name'),
            'servico'     => request('servico'),
            'email'    => request('email'),
    		'telefone' => request('phone_number'),
    		'cidade'   => request('city'),
    		'ip'       => request()->ip(),
    	];


    	Franquia::create($data);

    	$data = array_merge($data, [
            'greeting' => $data['assunto'],
            'lines'    => [
                '<b>Nome: </b>' . $data['nome'],
                '<b>E-mail: </b>' . $data['email'],
                '<b>Telefone: </b>' . $data['telefone'],
                '<b>Serviço: </b>' . $data['servico'],
                '<b>Cidade: </b>' . $data['cidade'],
                '<b>IP: </b>' . $data['ip'],
            ],
        ]);

    	Mail::send('emails.default', $data, function ($mail) use ($data) {
            $mail
                ->to(
                    config('app.emailOrcamento'),
                    config('app.name')
                )
                ->replyTo($data['email'], $data['nome'])
            	->subject($data['assunto']);
        });

    	return response([
    		'success' => true,
    		'message' => 'Enviado com sucesso!.',
    	], 200);
    }

    public function showByState()
    {
        $id = request('id');


        if(is_null($id))
            return '';


         $ds = DB::table('tbl_cad_franquias')
            ->join('gv_estado', 'tbl_cad_franquias.estado', '=', 'gv_estado.id')
            ->join('gv_cidade', 'tbl_cad_franquias.cidade', '=', 'gv_cidade.id')
            ->where('tbl_cad_franquias.estado', $id)
            ->select('tbl_cad_franquias.*', 'gv_estado.uf', 'gv_cidade.nome')
            ->get();

         

        // $state = State::where('sigla', $uf)->active()->first();
        // $representantes = $state->representantes()->with('city')->get();

        // return view('partials.representante', compact('representantes', 'state'));

        return $ds;
    }
}