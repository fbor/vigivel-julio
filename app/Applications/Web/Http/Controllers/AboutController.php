<?php

namespace Goutnix\Applications\Web\Http\Controllers;

use Illuminate\Http\Request;
use Goutnix\Support\Http\Controllers\Controller;
use Goutnix\Domains\Empresas\Empresa;

class AboutController extends Controller
{
    public function index()
    {

    	$empresas = Empresa::active()
    		->has('image')
    		->with('image')
    		->orderByRaw('rand()')
    		->get();

    	return view('about')
    		->with('empresas', $empresas);
    }
}