<?php

namespace Goutnix\Applications\Web\Http\Controllers;

use Illuminate\Http\Request;
use Goutnix\Support\Http\Controllers\Controller;
use Goutnix\Domains\Contacts\Contact;
use Mail;

class ContactController extends Controller
{
    public function index()
    {
    	return view('contact');
    }

    public function sender()
    {
    	if( ! request()->has('name') || ! request()->has('email') || ! request('g-recaptcha-response') ){
    		return response([
	    		'success' => false,
	    		'message' => 'Nome e e-mail são obrigatórios!',
	    	], 400);
    	}

    	$data = [
    		'assunto'  => 'Contato vindo do site ' . config('app.name'),
            'nome'     => request('name'),
            'email'    => request('email'),
    		'telefone' => request('phone_number'),
    		'mensagem' => request('observacao'),
    		'ip'       => request()->ip(),
    	];

    	Contact::create($data);

    	$data = array_merge($data, [
            'greeting' => $data['assunto'],
            'lines'    => [
                '<b>Nome: </b>' . $data['nome'],
                '<b>E-mail: </b>' . $data['email'],
                '<b>Telefone: </b>' . $data['telefone'],
                '<b>Mensagem: </b>' . nl2br($data['mensagem']),
                '<b>IP: </b>' . $data['ip'],
            ],
        ]);

    	Mail::send('emails.default', $data, function ($mail) use ($data) {
            $mail
                ->to(
                    config('app.emailContato'),
                    config('app.name')
                )
                ->replyTo($data['email'], $data['nome'])
            	->subject($data['assunto']);
        });

    	return response([
    		'success' => true,
    		'message' => 'Enviado com sucesso!.',
    	], 200);
    }
}