<?php

namespace Goutnix\Applications\Web\Http\Controllers;

use Illuminate\Http\Request;
use Goutnix\Support\Http\Controllers\Controller;
use Goutnix\Domains\Curriculos\Curriculo;
use Goutnix\Domains\Files\File;
use Mail;
use Storage;
use Carbon\Carbon;
use Configuration;


class WorkController extends Controller
{
    public function index()
    {
    	return view('work');
    }

    public function sender(Request $request)
    {
        $curriculo = new Curriculo;
        $curriculo->nome = request('nome');
        $curriculo->cpf = request('cpf');
        $curriculo->rg = request('rg');
        $curriculo->sexo = request('sexo');
        $curriculo->escolaridade = request('escolaridade');
        $curriculo->cargo = request('cargo');
        $curriculo->telefone = request('telefone');
        $curriculo->email = request('email');
        $curriculo->cep = request('cep');
        $curriculo->endereco = request('endereco');
        $curriculo->bairro = request('bairro');
        $curriculo->cidade = request('cidade');
        $curriculo->estado = request('estado');
        $curriculo->ip = request()->ip();
        $curriculo->save();

        if (request()->file('curriculo')):
        $file = request()->file('curriculo');


        $filename = sprintf(
            '%s.%s',
            str_slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)),
            $file->getClientOriginalExtension()
        );

        $pathWithFilename = sprintf(
            'curriculos/%s/%s',
            $curriculo->id,
            $filename
        );

        Storage::put(
            $pathWithFilename,
            file_get_contents($file->getRealPath())
        );

        $curriculo->document()->save(new File([
            'filename'   => $filename,
            'type'       => 'files',
            'module'     => 'curriculos',
            'created_at' => Carbon::now(),
            ]));
        endif;

        Mail::send('emails.default', $curriculo->toArray(), function ($mail) use ($curriculo /** , $pathWithFilename **/) {
            $mail
                ->to(
                    config('app.emailContato'),
                    config('app.name')
                )
                ->replyTo($curriculo->email, $curriculo->nome)
                ->subject('Currículo vindo do site');
                // ->attach(public_path('files/' . $pathWithFilename));

        });

        return response()->json(['success' => true, 'message' => 'Curr�culo enviado com sucesso!']);
    }
}