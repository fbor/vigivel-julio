<?php

namespace Goutnix\Applications\Web\Http\Controllers;

use Illuminate\Http\Request;
use Goutnix\Support\Http\Controllers\Controller;
use Goutnix\Domains\Orcamentos\Orcamento;
use Goutnix\Domains\Services\Service;
use Mail;

class OrcamentosController extends Controller
{
    public function index($id = null)
    {
        $servicos = Service::active()
            ->has('icon')
            ->with('icon')
            ->orderBy('ordem')
            ->get();

        return view('orcamento', ['id'=>$id])
            ->with('servicos', $servicos);
    }

    public function sender()
    {
    	if( ! request()->has('name') || ! request()->has('email') || ! request('g-recaptcha-response')){
    		return response([
	    		'success' => false,
	    		'message' => 'Nome e e-mail são obrigatórios!',
	    	], 400);
    	}

    	$data = [
            'servicos'      => request('servicos'),
            'nome'          => request('name'),
            'email'         => request('email'),
            'telefone'      => request('phone_number'),
            'cidade'      => request('cidade'),
            'estado'      => request('estado'),
            'mensagem'      => request('observacao'),
            'ip'            => request()->ip(),
        ];

        Orcamento::create($data);

    	$data = array_merge($data, [
            'greeting' => 'Solicitação de orçamento vinda do site ' . config('app.name'),
            'lines'    => [
                '<b>Serviços: </b>' . (is_array($data['servicos']) ? implode(', ', $data['servicos']) : '<i>Nenhum selecionado</i>'),
                '<b>Nome: </b>' . $data['nome'],
                '<b>E-mail: </b>' . $data['email'],
                '<b>Telefone: </b>' . $data['telefone'],
                '<b>Cidade: </b>' . $data['cidade'],
                '<b>Estado: </b>' . $data['estado'],
                '<b>Mensagem: </b>' . nl2br($data['mensagem']),
                '<b>IP: </b>' . $data['ip'],
            ],
        ]);

    	Mail::send('emails.default', $data, function ($mail) use ($data) {
            $mail
                ->to(
                    config('app.emailOrcamento'),
                    config('app.name')
                )
                ->replyTo($data['email'], $data['nome'])
            	->subject($data['greeting']);
        });

    	return response([
    		'success' => true,
    		'message' => 'Enviado com sucesso!.',
    	], 200);
    }
}