<?php

namespace Goutnix\Applications\Web\Providers;

use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

class ViewServiceProvider extends LaravelServiceProvider
{
	/**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    	/*view()->composer(
            ['view1', 'view2'], 
            function ($view) {
                $view->with([
                    'foo' => 'bar',
                ]);
            }
        );*/
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}