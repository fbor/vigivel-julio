<?php

namespace Goutnix\Applications\Core\Providers;

use Illuminate\Support\ServiceProvider;
use Carbon\Carbon;

class CarbonServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        setlocale (LC_TIME, $this->app->getLocale() . '.utf-8');
        Carbon::setLocale($this->app->getLocale());
    }
}
