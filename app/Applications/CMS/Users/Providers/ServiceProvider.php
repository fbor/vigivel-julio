<?php

namespace Goutnix\Applications\CMS\Users\Providers;

use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

class ServiceProvider extends LaravelServiceProvider
{
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}