<?php

namespace Goutnix\Applications\CMS\Authentication\Providers;

use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

class ServiceProvider extends LaravelServiceProvider
{
    public function register()
    {
        $this->app->register(AuthServiceProvider::class);
    }
}